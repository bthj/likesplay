(function (angular) {
  "use strict";

  var app = angular.module('likesplayApp.corral', [
    'ngRoute', 'famous.angular', 'firebase.utils', 'firebase']);

  app.controller( 'Corral',
      ['$scope',
      '$location',
      '$routeParams',
      '$famous',
      'dataService',
      'profileData',
      'geocoding',
      'messaging',
      'randomness',
      'cubification',
      'cubeRotation',
      'cubeInside',
      'colorFlow',
      'fading',
      'networkUserHandles',
      'Auth',
      'authService',
      '$analytics',
      function(
        $scope,
        $location,
        $routeParams,
        $famous,
        dataService,
        profileData,
        geocoding,
        messaging,
        randomness,
        cubification,
        cubeRotation,
        cubeInside,
        colorFlow,
        fading,
        networkUserHandles,
        Auth,
        authService,
        $analytics ) {

    var Timer = $famous['famous/utilities/Timer'];
    var Transitionable = $famous['famous/transitions/Transitionable'];

    var EventHandler = $famous['famous/core/EventHandler'];
    var GenericSync = $famous['famous/inputs/GenericSync'];
    var MouseSync = $famous["famous/inputs/MouseSync"];
    var TouchSync = $famous["famous/inputs/TouchSync"];
    var ScrollSync = $famous["famous/inputs/ScrollSync"];
    var RotateSync = $famous['famous/inputs/RotateSync'];
    var PinchSync = $famous['famous/inputs/PinchSync'];

    var Easing = $famous['famous/transitions/Easing'];

    var TextareaSurface = $famous['famous/surfaces/TextareaSurface'];


    var MINIMUM_OF_CUBES_IN_QUEUE = 5;
    var COUNT_OF_NEWEST_TO_FETCH_INITIALLY = 10;

    var _delayFunctionsAddCubeToCorral = [];

    var _allCubesQueue = {};
    var _newestCubesKeys = [];
    var _userCubesKeys = [];

    $scope.visibleCubes = {};  // cube objects, from storage, visible in the scene

    $scope.mediaSurfaceSize = cubification.getMediaSurfaceSize();

    var INITIAL_SCALE = .3;

    var lastTranslationTargets = [];
    var hasCubeStartedSpaceTravel = {};

    $scope.currentForegroundKey = null;
    $scope.currentForegroundProfile = {};



    ////////////////////////////////////
    // cube selection mode

    var _cubeKeyFromUrlOrSave;
    var _userIdRequested;

    $scope.usercubes = {
      oneusercubesVisible: false,
      mycubesBackgroundColor: '#e4f3fb',
      allcubesBackgroundColor: '#ff8900'
    }

    $scope.showMyCubes = function() {

      if( $scope.loggedIn ) {

        _userIdRequested = authService.getUserId();

        dataService.getUserCubes( _userIdRequested, addUserCubesToCorral );

        setActiveButtonMyCubes();

        $scope.usercubes.oneusercubesVisible = false;
      } else {
        $location.path( "/howdy" );
      }
    }

    $scope.showAllCubes = function() {

      setActiveButtonAllCubes();

      _userIdRequested = null;

      clearUserCubesFromQueue();

      $scope.usercubes.oneusercubesVisible = false;
    }

    function switchFromUserSpecificButtonToAllCubesButton() {

      $scope.showAllCubes();
      $scope.usercubes.oneusercubesVisible = false;
    }

    function disableButtonAllCubes() {
      $scope.usercubes.allcubesBackgroundColor = '#e4f3fb';
    }
    function setActiveButtonAllCubes() {
      $scope.usercubes.mycubesBackgroundColor = '#e4f3fb';
      $scope.usercubes.allcubesBackgroundColor  = '#ff8900';
    }
    function setActiveButtonMyCubes() {
      $scope.usercubes.mycubesBackgroundColor = '#ff8900';
      $scope.usercubes.allcubesBackgroundColor  = '#e4f3fb';
    }

    function clearUserCubesFromQueue() {
      _userCubesKeys.forEach( function(oneKey) {
        delete _allCubesQueue[oneKey];
      });
      _userCubesKeys = [];
    }

    function addNewCubesToCorral( snapshotValue ) {

      Object.keys(snapshotValue).forEach(function(oneKey) {

        _newestCubesKeys.push( oneKey );
      });

      fillCubeQueue( snapshotValue );

      addCubeToCorral();
    }

    function addUserCubesToCorral( snapshotValue ) {

      Object.keys(snapshotValue).forEach(function(oneKey) {

        _userCubesKeys.push( oneKey );
      });

      addCubeToCorral();
    }


    ///// cube selection by URL

    $scope.isExhibition = $location.search().exhibition;

    if( $routeParams.cubeKey ) {
      if( $routeParams.cubeKey.indexOf("facebook:") == 0
          || $routeParams.cubeKey.indexOf("google:") == 0
          || $routeParams.cubeKey.indexOf("twitter:") == 0 ) {

        _userIdRequested = $routeParams.cubeKey;

      } else {
        _cubeKeyFromUrlOrSave = $routeParams.cubeKey;
      }
    }

    if( _userIdRequested ) {

      dataService.getUserCubes( _userIdRequested, addUserCubesToCorral );

      $scope.usercubes.oneusercubesVisible = true;
      disableButtonAllCubes();

    } else if( _cubeKeyFromUrlOrSave ) {

      getSpecifiedCubeAndPlaceItInCubeQueueThenAddItToCorral( _cubeKeyFromUrlOrSave );

    } else {
      // cube persistence
      Auth.$onAuth( function(authData) {

        if( authData ) {

          dataService.persistOnlineCubesFromLocalStorage(
            addSavedCubesToQueueIfAnyAndAddToCorral );
        } else {
          // not logged in
          getNewBatchOfRandomCubesAndFillCubeQueueThenStartAddingCubesToCorral();
        }
      });
    }

    function addSavedCubesToQueueIfAnyAndAddToCorral( savedCubes ) {
      var savedCubesKeys = Object.keys(savedCubes);
      if( savedCubesKeys.length ) {

        console.log("cubes Saved On Corral Entry");
        console.log( savedCubes );

        if( savedCubesKeys.length == 1 ) {
          var cubeKey = savedCubesKeys[0];
          var cube = savedCubes[cubeKey];
          _cubeKeyFromUrlOrSave = cubeKey;
          addSpecifiedCubeToQueueAndAddItToCorral( cubeKey, cube );
        } else {
          fillCubeQueueAndStartAddingCubesToCorral( savedCubes );
        }

      } else {
        getNewBatchOfRandomCubesAndFillCubeQueueThenStartAddingCubesToCorral();
      }
    }



    ////////////////////////////////////
    // cube corral entry

    function addCubeToCorral( cubeKey ) {

      // console.log( "addCubeToCorral, key: " + cubeKey );

      // select a cube from the Queue (dequeue) and place it on .visibleCubes

      var _cubeKey;

      if( ! cubeKey ) {

        if( _userCubesKeys.length ) {

          // console.log( "_userCubesKeys.length" );
          // console.log( _userCubesKeys.length );

          fetchOneUserCubeAndAddItToCorral();
          return;

        } else if( _newestCubesKeys.length ) {

          // console.log( "_newestCubesKeys.length" );
          // console.log( _newestCubesKeys.length );

          // let's add one of the newest cubes

          _cubeKey = _newestCubesKeys.pop();

        } else {
          // let's add a random cube
          // console.log( "let's add a random cube" );

          var queueKeys = Object.keys(_allCubesQueue);
          // ensure that the queue has its minimum of cubes
          if( queueKeys.length - 1 < MINIMUM_OF_CUBES_IN_QUEUE ) {

            getNewBatchOfRandomCubesAndFillCubeQueue(); // ends in a clall back to addCubeToCorral()
            return;

          } else {

            var randomQueueKey;
            var isChosenCubeCurrentlyVisible;
            var searchCount = 0;
            do {
              randomQueueKey = queueKeys[randomness.getRandomInt(0, queueKeys.length)];
              isChosenCubeCurrentlyVisible = isCubeCurrentlyVisible( randomQueueKey );

              if( isChosenCubeCurrentlyVisible ) {
                if( ++searchCount >= 10 ) {
                  // let's wait, in the hope that then some cubes have been removed from .visibleCubes
                  Timer.setTimeout( function() {
                    addCubeToCorral();
                  }, 10000 );
                  return;
                }
              }
            } while( isChosenCubeCurrentlyVisible && searchCount < 10 );

            _cubeKey = randomQueueKey;
          }
        }

      } else {

        _cubeKey = cubeKey;
      }


      var cubeToAdd = _allCubesQueue[_cubeKey];

      if( cubeToAdd ) {

        cubeToAdd.key = _cubeKey;

        // console.log( cubeToAdd );

        // remove it from the incoming queue

        delete _allCubesQueue[_cubeKey];


        addCubeTransforms( cubeToAdd, _cubeKey );


        $scope.visibleCubes[_cubeKey] = cubeToAdd;

        if(!$scope.$$phase)
            $scope.$apply();

        startCubeAnimationWhenAllMediaItemsHaveLoaded( cubeToAdd, _cubeKey );

        if( ! $scope.isExhibition ) {
          $analytics.eventTrack('feed', {
            category: 'Corral', label: 'addCubeToCorral', noninteraction: true
          });
        }

      } else {
        // sometimes it happens that cubeToAdd is undefined (why?), so for now we'll just try again
        addCubeToCorral();
      }

    }



    function getNewBatchOfRandomCubesAndFillCubeQueue() {

      dataService.getTotallyRandomCubes(
        MINIMUM_OF_CUBES_IN_QUEUE, fillCubeQueueAndStartAddingCubesToCorral );
    }
    function getNewBatchOfRandomCubesAndFillCubeQueueThenStartAddingCubesToCorral() {

      // dataService.getTotallyRandomCubes(
      //   MINIMUM_OF_CUBES_IN_QUEUE, fillCubeQueueAndStartAddingCubesToCorral );

      dataService.getLatestCubes(
        COUNT_OF_NEWEST_TO_FETCH_INITIALLY, addNewCubesToCorral );
    }
    function getSpecifiedCubeAndPlaceItInCubeQueueThenAddItToCorral( cubeKey ) {

      dataService.getCubeByKey( cubeKey, addSpecifiedCubeToQueueAndAddItToCorral );

      $analytics.eventTrack('feed', {
        category: 'Corral', label: 'getSpecifiedCube', noninteraction: true
      });
    }

    function fetchOneUserCubeAndAddItToCorral() {

      var oneUserCubeKey = _userCubesKeys.pop();

      dataService.getCubeByKey(
        oneUserCubeKey, addSpecifiedCubeToQueueAndAddItToCorral );


      if( !_userCubesKeys.length ) {
        switchFromUserSpecificButtonToAllCubesButton();
      }

      $analytics.eventTrack('feed', {
        category: 'Corral', label: 'fetchUserCubeAndAddItToCorral',
        noninteraction: true
      });
    }

    function fillCubeQueue( newBatchOfCubes ) {

      // TODO: ensure that the cubes we add are not already visible

      $.extend( true, _allCubesQueue, newBatchOfCubes );
    }

    // function addSpecifiedCubetoQueue( cubeKey, cube ) {
    //   var specificCube = {};
    //   specificCube[cubeKey] = cube;
    //   fillCubeQueue( specificCube );
    // }
    function addSpecifiedCubeToQueueAndAddItToCorral( cubeKey, cube ) {

      var specificCube = {};
      specificCube[cubeKey] = cube;
      fillCubeQueue( specificCube );

      clearDelayFunctionsThatAddToCorral();

      addCubeToCorral( cubeKey );
    }
    function fillCubeQueueAndStartAddingCubesToCorral( newBatchOfCubes ) {

      fillCubeQueue( newBatchOfCubes );
      addCubeToCorral();
    }

    function isCubeCurrentlyVisible( key ) {

      return key in $scope.visibleCubes;
    }



    function addCubeToCorralAfterDelay() {
      // after a random delay, pick another cube from the queue to appear on the scene
      var oneDelayFunction = Timer.setTimeout( function() {

        if( ! _isInsideCube ) {

          addCubeToCorral();
        }
      }, randomness.getRandomInt(2000, 6000) );

      _delayFunctionsAddCubeToCorral.push( oneDelayFunction );
    }

    function clearDelayFunctionsThatAddToCorral() {
      if( _delayFunctionsAddCubeToCorral ) {

        _delayFunctionsAddCubeToCorral.forEach( function(oneDelayFunction) {

          Timer.clear( oneDelayFunction );
        });
        _delayFunctionsAddCubeToCorral = [];
      }
    }

    // let's clear all pending requests to add to the corral,
    // when we navigate from this page
    $scope.$on("$destroy", function(){

      clearDelayFunctionsThatAddToCorral();
    });



    ////////////////////////////////////
    // cube media surfaces configuration

    // $scope.keyholeOpacity = new Transitionable( 0 );

    function addCubeImageSurfaces( cube, key ) {

      var cubeImageSurfaces = cubification.getInitializedImageSurfaces(6, false);

      cube.mediaItems.forEach( function( oneItem, itemIndex, array ) {

        cubeImageSurfaces[itemIndex].setContent( oneItem.mediaUrl );

        // TODO: get image surface directly, instead of this intermediate array above
        oneItem.imageSurface = cubeImageSurfaces[itemIndex];

        // oneItem.imageSurface["properties"] = {zIndex: -99};

        // oneItem.imageSurface.pipe( inputMovementSync );
      });
    }

    function startCubeAnimationWhenAllMediaItemsHaveLoaded( cube, key ) {
      // TODO: we'll have to modify this method when handling media types other than images

      var imgLoaders = [];
      cube.mediaItems.forEach( function( oneItem, itemIndex, array ) {

        imgLoaders.push( $('<img>').attr('src', oneItem.mediaUrl) );
      });
      // TODO: does this simple thing work, or is something elaborate as this needed?
      //  https://forum.jquery.com/topic/wait-until-all-images-are-loaded#14737000001440239
      // $( imgLoaders ).load(function(){

        // start the first step of animation
        // Use callbacks to signal when one cube has finished its animation segment,
        // with a dispatcher reacting to and allocating another cube
        // an opportunity to perform an animation segment.
        hasCubeStartedSpaceTravel[key] = false;
        // animateCube( key );
        animateCube( cube, key );

        addCubeToCorralAfterDelay();
      // });
    }



    ////////////////////////////////////
    // cube transitionables configuration

    // initialize the positions of the media surfaces
    // and the surrounding transforms - add Transitionables to the cube meta object
    function addCubeTransforms( cube, key ) {

      var edgeHalfLength = $scope.mediaSurfaceSize[0] / 2;
      var cubeCenter = cubification.getBoxCenterPoint();

      // initializeCubeTransformObject( key );

      // add transforms to cube media items
      cube.mediaItems.forEach( function( oneItem, itemIndex, array ) {

        var itemTransformValues = // translate and rotate arrays
          cubification.getBoxSideTransform( itemIndex, cubeCenter, edgeHalfLength );
        var itemTranslator = new Transitionable( itemTransformValues.translate );
        var itemRotator = new Transitionable( itemTransformValues.rotate );

        // visibleCubesTransforms[key].mediaItems.push({
        //   'translator': itemTranslator,
        //   'rotator': itemRotator,
        //   'origin': [.5, .5]
        // });

        oneItem.translator = itemTranslator;
        oneItem.rotator = itemRotator;
        oneItem.origin = [.5, .5];
      });

      // add transforms to the cube as a whole; translation and rotation
      // visibleCubesTransforms[key].translator = new Transitionable(
      //   [cubeCenter.x, cubeCenter.y, cubeCenter.z - 20000] );
      // visibleCubesTransforms[key].rotator = new Transitionable( [0, 0, 0] );
      // visibleCubesTransforms[key].opacitator = new Transitionable( 1 );
      // visibleCubesTransforms[key].scalator = new Transitionable( [1, 1, 1] );

      cube.translator = new Transitionable(
        [cubeCenter.x, cubeCenter.y-window.innerHeight*5, cubeCenter.z - 20000] );
      cube.rotator = new Transitionable( [0, 0, 0] );
      cube.opacitator = new Transitionable( 0 );
      // cube.scalator = new Transitionable( [INITIAL_SCALE, INITIAL_SCALE, INITIAL_SCALE] );

      // visibleCubesTransforms[key].visible = false;
    }
  /*
    function initializeCubeTransformObject( key ) {
      if( visibleCubesTransforms[key] ) {
        removeVisibleCube( key );  // TODO: we may want to do this in another place
      }
      // if( ! visibleCubesTransforms[key] ) {
        visibleCubesTransforms[key] = {};
        visibleCubesTransforms[key].mediaItems = [];
      // }
    }
  */

    function removeVisibleCube( key ) {

      console.log( "------BLÚBBS-------" );

      $scope.$apply(function () {
        delete $scope.visibleCubes[key];
      });

      delete hasCubeStartedSpaceTravel[ key ];

      if( $scope.currentForegroundKey == key ) {
        $scope.currentForegroundKey = null;
      }
    }



    ////////////////////////////////////
    // animation management

    var hasAnimationDispatchStarted = false;

    function dispatchAnimations() {
      var animationTimeout = // less waiting time as there are more cubes in the scene
        randomness.getRandomInt(1000, 2000) - Object.keys($scope.visibleCubes).length * 100;
      // console.log( "animationTimeout: " + animationTimeout );
      Timer.setTimeout( function() {

        // choose a random cube from the visible ones
        var visibleKeys = Object.keys($scope.visibleCubes);
        var key;
        var cube = null;
        var searchCount = 0;
        do {
          key = visibleKeys[randomness.getRandomInt(0, visibleKeys.length)];
          cube = $scope.visibleCubes[key];
          // cube = $scope.visibleCubes[ randomness.getRandomInt(0, $scope.visibleCubes.length) ];
          searchCount++;
        } while( (!cube || cube.translator.isActive() || key == $scope.currentForegroundKey)
                && searchCount < 66 );

        if( ! cube.translator.isActive() && key != $scope.currentForegroundKey ) {
          leakCubeDownOneSegment( cube, key );
        } // else we didn't find any non-animating cube,
        // so let's not animate the one we have again, resulting in strange behaviour

        if( ! _isInsideCube ) {

          dispatchAnimations();
        }
      }, animationTimeout );
    }

    function animateCube( cube, key ) {

      if( hasCubeStartedSpaceTravel[key] ) {
        // let's continue leaking the cube down the screen
        leakCubeDownOneSegment( cube, key );
      } else {
        // let's do initial animation, with the cube shooting in from space
        shootCubeInFromDeepSpace( cube, key );
      }
    }

    function getRandomRollRotation( currentRotation ) {
      return [
        currentRotation[0] + Math.PI * randomness.getRandomArbitrary(.05, .5) * randomness.getNegativeOrPositiveMultiplier(),
        currentRotation[1] + Math.PI * randomness.getRandomArbitrary(.05, .5) * randomness.getNegativeOrPositiveMultiplier(),
        currentRotation[2] // cubeRotation.updateRotation only works with x,y: + Math.PI * randomness.getRandomArbitrary(.05, .5) * randomness.getNegativeOrPositiveMultiplier()
      ];
    }

    function shootCubeInFromDeepSpace( cube, key ) {
      var shootingCubeHasBeenSelectedByURL = key === _cubeKeyFromUrlOrSave;
      if( shootingCubeHasBeenSelectedByURL ) {
        // this cube was selected from URL, let's bring it to the foreground
        var translationTarget = getCubeForegroundTargetPoint();

        // cube.scalator.set(
        //   [1, 1, 1],
        //   {duration:500, curve:'easeOut'} );

      } else {
        var translationTarget = getCubeTargetPoint();
      }

      cube.opacitator.set( 1, {duration: 500, curve: 'easeOut'} );

      cube.translator.set( translationTarget, {duration:1000}, function() {

        if( shootingCubeHasBeenSelectedByURL ) {
          // $scope.$apply(function () {

            setCurrentForegroundKey( key );

            if(!$scope.$$phase) $scope.$apply();
          // });
          cube.rotator.set(
            getRandomRollRotation( cube.rotator.get() ),
            {duration: 1000, curve: 'easeOut'} );
        } else {
          leakCubeDownOneSegment( cube, key );
        }
      });

      hasCubeStartedSpaceTravel[key] = true;
    }

    function leakCubeDownOneSegment( cube, key ) {
      var currentRotation = cube.rotator.get();
      cube.rotator.set(
        getRandomRollRotation( currentRotation ),
        {duration: 1000, curve: 'easeOut'} );

      var currentTranslation = cube.translator.get();
      var leakDestination
      var searchCount = 0;
      // do {
        leakDestination = [
          currentTranslation[0] + getCubeLeakXOffset(),
          currentTranslation[1] + getCubeLeakYOffset(),
          currentTranslation[2] + getCubeLeakZOffset()
        ];
      //   searchCount++;
      //   // console.log( "searchCount: " + searchCount );
      // } while( destinationWillCollideWithAnotherCube(leakDestination, key) && searchCount < 66 );

      cube.translator.set( leakDestination, {duration: 1000, curve: 'easeOut'}, function() {

        if( isCubeOffScreen( cube ) ) {
          // Remove cubes from .visibleCubes when they animate out of view
          removeVisibleCube( key );
        }
        // give some other visible cube the chance to animate
        if( ! hasAnimationDispatchStarted ) {
          dispatchAnimations();
          hasAnimationDispatchStarted = true;
        }
      } );
    }
    function destinationWillCollideWithAnotherCube( dest, travelerKey ) {
      var willCollide = false;
      var visibleKeys = Object.keys($scope.visibleCubes);
      var requiredOffset = $scope.mediaSurfaceSize[0] / 2;
      for( var i=0; i < visibleKeys.length; i++ ) {
        var oneVisibleKey = visibleKeys[i];
        if( travelerKey !== oneVisibleKey ) {
          var oneCubePos = $scope.visibleCubes[oneVisibleKey].translator.get();
          if( Math.abs(oneCubePos[0] - dest[0]) < requiredOffset
              || Math.abs(oneCubePos[1] - dest[1]) < requiredOffset
              || Math.abs(oneCubePos[2] - dest[2]) < requiredOffset ) {

            willCollide = true;
            break;
          }
        }
      }
      return willCollide;
    }

    function getCubeTargetPoint() {
      // calculate some good, random position
      var target;
      var searchCount = 0;
      do {
        target = [
          randomness.getRandomInt(-window.innerWidth*1.3, window.innerWidth*1.3),
          randomness.getRandomInt(-(window.innerHeight)*1.4, -(window.innerHeight)*1.2),
          randomness.getRandomInt(-4000,-2000)
          // randomness.getRandomInt(-(window.innerWidth/2)*.8, (window.innerWidth/2)*.8),
          // randomness.getRandomInt(-(window.innerHeight)*.4, -(window.innerHeight)*.2),
          // 0 //$scope.mediaSurfaceSize[0]
        ];
        searchCount++; // to remove the possibility of an infinite loop :P
      } while( ! isTargetProperlyOffsetFromLastTargetPoints(target) && searchCount < 66 );

      lastTranslationTargets.push(target);
      return target;
    }
    var NUMBER_OF_LAST_TARGET_REFERENCE_POINTS = 3;
    function isTargetProperlyOffsetFromLastTargetPoints( target ) {
      var isProperOffset = true;

      if( lastTranslationTargets.length > NUMBER_OF_LAST_TARGET_REFERENCE_POINTS ) {
        lastTranslationTargets.shift();
      }
      var requiredOffset = $scope.mediaSurfaceSize[0] * INITIAL_SCALE;
      for( var i=0; i < lastTranslationTargets.length; i++ ) {
        var oneLastTarget = lastTranslationTargets[i];
        if( Math.abs(oneLastTarget[0] - target[0]) < requiredOffset
            // || Math.abs(oneLastTarget[1] - target[1]) < requiredOffset
            || Math.abs(oneLastTarget[2] - target[2]) < requiredOffset ) {

          isProperOffset = false;
          break;
        }
      }
      return isProperOffset;
    }

    function getCubeForegroundTargetPoint() {
      return [0, 0, 200 /*$scope.mediaSurfaceSize[0]*/];
    }

    function getCubeLeakXOffset() {
      // return randomness.getRandomInt(-window.innerWidth*.2, window.innerWidth*.2);
      return randomness.getRandomInt(-window.innerWidth*.02, window.innerWidth*.02);
    }
    function getCubeLeakYOffset() {
      // get some good random portion of the screen Y height
      return window.innerHeight * randomness.getRandomArbitrary(.2, .9);
      // return window.innerHeight * randomness.getRandomArbitrary(.08, .2);
    }
    function getCubeLeakZOffset() {
      // return randomness.getRandomInt(-200, 200);
      return 0;
    }

    function isCubeOffScreen( cube ) {
      // check if cube translation coordinates are outside the screen coordiantes
      // TODO: maybe do proper calculation of whether the point is inside the frustum?
      // http://gamedev.stackexchange.com/a/60622
      // http://www.lighthouse3d.com/tutorials/view-frustum-culling/
      // hack for now:
      var currentTranslation = cube.translator.get();
      return currentTranslation[1] - Math.abs(currentTranslation[2]) / 2 > window.innerHeight;
      // return false;
    }





    ////////////////////////////////////
    // touch handlers

    var didPinch = false;  // to prevent jumps in rotation after a pinch gesture.


    GenericSync.register({
      "mouse": MouseSync,
      "touch": TouchSync,
      "scroll": ScrollSync
    });

    $scope.gripTouchHandler = new EventHandler();
    var cubeInsideMovementSync = new GenericSync(["mouse", "touch"]);
    $scope.gripTouchHandler.pipe( cubeInsideMovementSync );

    $scope.cubeTouchHandler = new EventHandler();
    var raycastSync = new GenericSync(["mouse", "touch"]);
    $scope.cubeTouchHandler.pipe( raycastSync );

    raycastSync.on( 'start', function(data) {
      // console.log( "raycastSync start" );
      // console.log( data );
      if( $scope.currentForegroundKey ) {

        $scope.visibleCubes[$scope.currentForegroundKey].translator.halt();
      }
    });

    raycastSync.on( 'update', function(data) {
      // console.log( "raycastSync update" );
      if( didPinch ) {
        didPinch = false;
      } else {

        if( $scope.currentForegroundKey ) {

          cubeRotation.updateRotation(
            $scope.visibleCubes[$scope.currentForegroundKey].rotator, data );

          bringCubeOneStepCloser( $scope.currentForegroundKey );
        }
      }
    });

    raycastSync.on( 'end', function(event) {
      // console.log( "raycastSync end" );
      if( didPinch ) {
        didPinch = false;
      } else {

        if( cubeRotation.hasBeenMoving() ) {

          cubeRotation.animateRotatorWithFauxDrag(
            $scope.visibleCubes[$scope.currentForegroundKey].rotator );

          if( $scope.currentForegroundKey ) bringCubeToForeground( $scope.currentForegroundKey );

          $analytics.eventTrack('click', {
            category: 'UI Interaction', label: 'cubeRotatedOutsideCorral'
          });
        }
        // else {
        //   var closestKey = getClosestCubeKeyToClick( [event.clientX, event.clientY] );
        //   if( closestKey ) {
        //     if( closestKey != $scope.currentForegroundKey ) {
        //       bringCurrentForegroundCubeToBackground();
        //     }
        //     bringCubeToForeground( closestKey );
        //     $scope.currentForegroundKey = closestKey;
        //   }
        // }
      }
    });


    $scope.bringPickedCubeToForeground = function( cubeKey ) {

      if( ! _isInsideCube && !cubeRotation.hasBeenMoving() ) {

        if( cubeKey !== $scope.currentForegroundKey ) {
          bringCurrentForegroundCubeToBackground();
        }

        bringCubeToForeground( cubeKey );

        // $scope.$apply(function () {
          setCurrentForegroundKey( cubeKey );
          if(!$scope.$$phase) $scope.$apply();
        // });

        $analytics.eventTrack('click', {
          category: 'UI Interaction', label: 'bringCubeToForeground'
        });
      }
    }

    function setCurrentForegroundKey( key ) {

      $scope.currentForegroundKey = key;

      fadeCubeActionsIn();
    }


    $scope.cubeActionsOpacity = new Transitionable( 0 );
    $scope.cubeActionsEnabled = false;

    function fadeCubeActionsIn() {
      $scope.cubeActionsEnabled = true;
      $scope.cubeActionsOpacity.set( 1, {duration:3000}, function() {

        fadeCubeActionsOut();
      });
    }

    function fadeCubeActionsOut() {
      var currentForegroundZPos =
        $scope.visibleCubes[$scope.currentForegroundKey].translator.get()[2];
      if( currentForegroundZPos > -2000 ) {
        Timer.setTimeout(function() {

          var opacity = 1 - currentForegroundZPos / -2000;
          $scope.cubeActionsOpacity.set( opacity, {duration:888} );

          fadeCubeActionsOut();
        }, 1000);
      } else {
        $scope.cubeActionsEnabled = false;
      }
    }

    $scope.shareCurrentCube = function() {

      $location.path( "/corral/" + $scope.currentForegroundKey );
    }



    // $scope.cubeTouchHandler = new EventHandler();
    // var cubeTouchSync = new GenericSync(["mouse", "touch", "scroll"]);
    // $scope.cubeTouchHandler.pipe( cubeTouchSync );
    //
    // cubeTouchSync.on( 'start', function(event) {
    //   console.log( "cube touch, clientX: " +  event.clientX + ", clientY: " + event.clientY );
    // });



    function getVectorDistance( vector1, vector2 ) {
      var pointDistanceSum = 0;
      for( var i=0; i < vector1.length; i++ ) {
        pointDistanceSum += Math.pow(vector1[i]-vector2[i], 2);
      }
      return Math.sqrt( pointDistanceSum );
    }

    function getClosestCubeKeyToClick( click2DVector ) {
      var key = null;
      var minDistance = Number.MAX_VALUE;
      var visibleCubeKeys = Object.keys($scope.visibleCubes);
      var minDistanceCube;
      visibleCubeKeys.forEach( function(oneCubeKey, index, array) {
        minDistanceCube = $scope.visibleCubes[oneCubeKey];
        var cubeTranslation = minDistanceCube.translator.get();
        var cubeTranslationInScreenCoords = [
          window.innerWidth / 2 + cubeTranslation[0],
          window.innerHeight / 2 + cubeTranslation[1] ];
        var oneCubeDistance = getVectorDistance(
          click2DVector, cubeTranslationInScreenCoords );
        if( oneCubeDistance < minDistance ) {
          minDistance = oneCubeDistance;
          key = oneCubeKey;
        }
      });
      // let's check if we actually clicked within the cube bounds
      if( minDistance >
          minDistanceCube.scalator.get()[0] * $scope.mediaSurfaceSize[0] ) {
        key = null;
      }
      return key;
    }

    function bringCurrentForegroundCubeToBackground() {
      if( $scope.currentForegroundKey ) {
        // $scope.visibleCubes[$scope.currentForegroundKey].translator.halt();
        // $scope.visibleCubes[$scope.currentForegroundKey].translator.set(
        //   getCubeTargetPoint(), {duration:1000, curve: 'easeOut'}
        // );
        $scope.visibleCubes[$scope.currentForegroundKey].translator.halt();
        startCubeFadeIntoVoid( $scope.currentForegroundKey );
        // $scope.visibleCubes[$scope.currentForegroundKey].scalator.set(
        //   [INITIAL_SCALE, INITIAL_SCALE, INITIAL_SCALE],
        //   {duration:500, curve:'easeOut'}
        // );
      }
    }

    function bringCubeToForeground( key ) {

      // $scope.visibleCubes[key].mediaItems.forEach( function( oneItem, itemIndex, array ) {
      //   oneItem.imageSurface["properties"] = {zIndex: -99};
      // });

      $scope.visibleCubes[key].translator.halt();
      $scope.visibleCubes[key].translator.set(
        getCubeForegroundTargetPoint(),
        {duration: 1000, curve: 'easeOut'}, function() {

          $scope.insideSurfacesShown = true;

          startCubeFadeIntoVoid( key );
        }
      );

      // $scope.visibleCubes[key].scalator.set(
      //   [1, 1, 1],
      //   {duration:1000, curve:'easeOut'}
      // );
    }

    function bringCubeOneStepCloser( key ) {
      var currentTranslation = $scope.visibleCubes[key].translator.get();
      if( currentTranslation[2] < $scope.mediaSurfaceSize[0] ) {
        $scope.visibleCubes[key].translator.set([
          currentTranslation[0],
          currentTranslation[1],
          currentTranslation[2] + 1
        ]);
      }
    }

    function startCubeFadeIntoVoid( key ) {
      $scope.visibleCubes[key].translator.set(
        [0, 0, -19990],
        {duration: 50000, curve: Easing.inQuad},
        function() {
          removeVisibleCube( key );
        }
      );
    }





    ////////////////////////////////////
    // cube insides

    $scope.mediaSurfacesShown = true;
    $scope.insideSurfacesShown = false;
    $scope.hatchInsideShown = true;
    var _isInsideCube = false;

    // $scope.bottleMessageReply = new TextareaSurface({
    //   placeholder: 'Message in a bottle:',
    //   rows: '30',
    //   cols: '30'
    // });

    function addFormattedDateFromTimestampToObject( objectWithTimestamp ) {

      objectWithTimestamp.formatted_date =
        timestampToDateString( objectWithTimestamp.timestamp );
    }

    function getCurrentForegroundCubeProfile() {
      var currentForegroundUserId = $scope.visibleCubes[$scope.currentForegroundKey].author;
      $scope.currentForegroundProfile = {
        "profileGender": profileData.getArrayWithGenderForUser( currentForegroundUserId ),
        "profileLookingFor": profileData.getArrayWithSeekingForUser( currentForegroundUserId ),
        "profileRelationships": profileData.getArrayWithRelationshipForUser( currentForegroundUserId ),
        "profileOrientation": profileData.getArrayWithOrientationForUser( currentForegroundUserId )
      };
      console.log( "$scope.currentForegroundProfile" );
      console.log( $scope.currentForegroundProfile );
    }

    $scope.goDeepInside = function( key ) {

      if( key === $scope.currentForegroundKey ) {

        if( ! $scope.insideSurfacesShown ) {
          // double check, otherwise set in bringCubeToForeground()
          $scope.insideSurfacesShown = true;
        }

        $scope.cubeTouchHandler.unpipe( raycastSync );
        // so you can't mess up the cube entry by touching the inside grips
        $scope.gripTouchHandler.unpipe( cubeInsideMovementSync );

        // let's stop the cube fade into void translation
        $scope.visibleCubes[key].translator.halt();

        if( ! $scope.visibleCubes[key].mediaItems[0].isBottomAligned ) {

          $scope.visibleCubes[key].mediaItems[0].origin =
            cubeInside.getHatchSurfaceOriginForOpening();

          cubeInside.translateHatchSurfaceForOpening(
            $scope.visibleCubes[key].mediaItems[0].translator,
            $scope.mediaSurfaceSize[0] );

          $scope.visibleCubes[key].mediaItems[0].isBottomAligned = true;
        }

        cubeInside.openAndEnterCube(
          $scope.visibleCubes[key].mediaItems[0].rotator, postOpenCallback,
          $scope.visibleCubes[key].rotator, $scope.visibleCubes[key].translator,
          cubeEntryDuringCallback, cubeEntryDoneCallback
        );

        _isInsideCube = true;
      }
    }

    function postOpenCallback() {
      // TODO: color flow and keyhole visibility

      getCurrentForegroundCubeProfile();

      addFormattedDateFromTimestampToObject(
        $scope.visibleCubes[$scope.currentForegroundKey] );
    }

    function cubeEntryDuringCallback() {
      // show things on cube inside walls

      getForegroundCubePubliceMessages();
      checkIfUserHasLikedForegroundCube();
      getForegroundCubeLikes();
    }

    function cubeEntryDoneCallback() {
      // wire up touch handler for cube inside walls

    //   console.log( "$scope.gripTouchHandler.pipe( cubeInsideMovementSync );" );
    //  $scope.gripTouchHandler.pipe( cubeInsideMovementSync );

      $scope.$apply(function () {
        $scope.mediaSurfacesShown = false;
        $scope.hatchInsideShown = false;
      });

      $scope.gripTouchHandler.pipe( cubeInsideMovementSync );

      // drawFromQRQueue();

      $analytics.eventTrack('click', {
        category: 'UI Interaction', label: 'cubeEntryCorral'
      });
    }



    ////////////////////////////////////
    // cube inside drag event handling

    cubeInsideMovementSync.on( 'start', function(data) {

      cubeInside.startCubeInsideRotation(
        $scope.visibleCubes[$scope.currentForegroundKey].rotator );
      console.log( "cubeInsideMovementSync start" );
    });

    cubeInsideMovementSync.on( 'update', function(data) {
      // console.log( "cubeInsideMovementSync update" );
      if( didPinch ) {
        didPinch = false;
      } else {

        cubeInside.updateCubeInsideRotation(
          $scope.visibleCubes[$scope.currentForegroundKey].rotator, data );
      }
    });

    cubeInsideMovementSync.on( 'end', function(data) {
      // console.log( "cubeInsideMovementSync end" );
      if( didPinch ) {
        didPinch = false;
      } else {

        cubeInside.snapCubeRotationToQuartCircles(
          data,
          $scope.visibleCubes[$scope.currentForegroundKey].rotator,
          $scope.visibleCubes[$scope.currentForegroundKey].translator,
          $scope.visibleCubes[$scope.currentForegroundKey].mediaItems[0].rotator,
          cubeExitDuringCallback, cubeExitDoneCallback, postCloseCallback );

        $analytics.eventTrack('click', {
          category: 'UI Interaction', label: 'cubeRotatedInsideCorral'
        });
      }
    });

    function cubeExitDuringCallback() {
      $scope.$apply(function () {
        $scope.mediaSurfacesShown = true;
        $scope.hatchInsideShown = true;
      });

      // $scope.gripTouchHandler.unpipe( cubeInsideMovementSync );

      _isInsideCube = false;
      addCubeToCorralAfterDelay();
      dispatchAnimations();
    }

    function cubeExitDoneCallback() {

     $scope.cubeTouchHandler.pipe( raycastSync );

     $scope.insideSurfacesShown = false;

     addPublicMessageToForegroundCube();

     $analytics.eventTrack('click', {
       category: 'UI Interaction', label: 'cubeExitCorral'
     });
    }

    function postCloseCallback() {

      // TODO:
      // $scope.$apply(function () {
      //   $scope.showKeyhole = true;
      //   transitionableFadeIn( $scope.keyholeOpacity );
      //   startWatchingColorFlowValues();
      // });

      startCubeFadeIntoVoid( $scope.currentForegroundKey );
    }



    ////////////////////////////////////
    // messaging

    /// public messages

    $scope.messages = {
      foregroundCubePublicMessages: []
    };

    // var _qrDrawQueue = {};


    function getForegroundCubePubliceMessages() {
      messaging.getPublicMessagesForCube(
        $scope.currentForegroundKey, applyForegroundCubePublicMessagesToScope );
    }
    function applyForegroundCubePublicMessagesToScope( messagesArray ) {

      $scope.messages.foregroundCubePublicMessages = messagesArray;

      $scope.messages.foregroundCubePublicMessages.forEach( function(oneMessage) {
        addFormattedDateFromTimestampToObject( oneMessage );
      });
    }

    // watching QR code containers enter the DOM tree with the jquery.initialize plugin
    // $(".qrcode-main-message").initialize(function(){
    //   if( $(this).attr("id") && $(this).text().indexOf("http") == 0 ) {
    //     var containerId = $(this).attr("id");
    //     var signatureString = $(this).text().trim();
    //     $(this).empty();
    //
    //     _qrDrawQueue[containerId] = signatureString;
    //   }
    // });
    // function drawFromQRQueue() { // called in some cube entry callback
    //   var containerIds = Object.keys( _qrDrawQueue );
    //   containerIds.forEach( function(oneContainerId) {
    //
    //     drawQRCodeInContainer( oneContainerId, _qrDrawQueue[oneContainerId] );
    //   });
    //   _qrDrawQueue = {};
    // }
    // function drawQRCodeInContainer( containerId, signatureString ) {
    //     var qrcodesvg = new Qrcodesvg( signatureString, containerId, 75 );
    //     qrcodesvg.draw({"method":"classic", "fill-colors":["#000000","#2D2D2D"], "fill-colors-scope":"square"}, {"stroke-width":0});
    // }

    function addPublicMessageToForegroundCube() {
      var publicMessage = $("#reply" + $scope.currentForegroundKey ).val();
      if( publicMessage ) {
        var cubeId = $scope.currentForegroundKey;
        var cubeCreatorId = $scope.visibleCubes[cubeId].author; // he will be notified

        messaging.addPublicMessageToCube( cubeId, cubeCreatorId, publicMessage.trim() );

        $analytics.eventTrack('entry', {
          category: 'UI Interaction', label: 'addPublicMessageToCube'
        });
      }
    }



    /// likes
    $scope.likes = {
      hasLikedForegroundCube: false,
      foregroundCubeLikes: []
    };

    function checkIfUserHasLikedForegroundCube() {
      messaging.hasUserLiked(
        $scope.currentForegroundKey, showIfUserHasLikedForegroundCube );
    }
    function showIfUserHasLikedForegroundCube( likeKey ) {
      $scope.likes.hasLikedForegroundCube = likeKey;
    }

    function getForegroundCubeLikes() {
      messaging.getLikesForCube(
        $scope.currentForegroundKey, applyForegroundCubeLikesToScope );
    }
    function applyForegroundCubeLikesToScope( likesArray ) {

      $scope.likes.foregroundCubeLikes = likesArray;

      $scope.likes.foregroundCubeLikes.forEach( function(oneLike) {
        addFormattedDateFromTimestampToObject( oneLike );
      });
    }

    $scope.likeCube = function( cubeId ) {
      var cubeCreatorId = $scope.visibleCubes[cubeId].author; // he will be notified

      messaging.likeCube( cubeId, cubeCreatorId, showIfUserHasLikedForegroundCube );

      $analytics.eventTrack('entry', {
        category: 'UI Interaction', label: 'likeCube'
      });
    }

    $scope.unlikeCube = function( cubeId, likeId ) {

      messaging.unlikeCube( cubeId, likeId, showIfUserHasLikedForegroundCube );

      $analytics.eventTrack('entry', {
        category: 'UI Interaction', label: 'unlikeCube'
      });
    }


    $scope.showUserCubes = function( userId ) {
      $location.path( "/corral/" + userId );
    }


    /// location

    $scope.hostURL = location.protocol.concat("//").concat(window.location.hostname);




    ////////////////////////////////////
    // cube info surfaces

    $scope.scrollMessagesEventHandler = new EventHandler();
    $scope.scrollLikesEventHandler = new EventHandler();
    $scope.scrollTagsEventHandler = new EventHandler();
    $scope.scrollProfileEventHandler = new EventHandler();
    $scope.scrollLocationEventHandler = new EventHandler();

    $scope.tagFlexLayoutOptions = cubeInside.tagFlexLayoutOptions;


    // based on http://www.epochconverter.com/programming/#javascript
    function timestampToDateString( timestamp ) {
      console.log( "timestampToDateString: " +  timestamp );
      var myDate = new Date( timestamp );
      return myDate.toGMTString();
      // return myDate.toLocaleString();
    }



    ////////////////////////////////////
    // inside grip sizes and positions

    $scope.gripSize = {};
    $scope.gripIconSize = {};
    $scope.gripTranslations = [];
    $scope.gripIconTranslations = [];

    cubeInside.calculateGripSizes(
      $scope.gripSize, $scope.gripIconSize, $scope.mediaSurfaceSize[0] );
    cubeInside.calculateGripTranslations(
      $scope.gripSize, $scope.gripTranslations, $scope.gripIconTranslations,
      6, $scope.mediaSurfaceSize[0] );



    ////////////////////////////////////
    // TODO: resize event handler

    $scope.resizeEventHandler = new EventHandler();



    ////////////////////////////////////
    // colour flow

    $scope.flowRGB = [0, 0, 0];

    function updateFlowRGB( newRGB ) {
      $scope.$apply(function () {
        $scope.flowRGB = newRGB;
      });
    }

    colorFlow.registerColorUpdateCallback( updateFlowRGB );

    // for now we'll have color flowing continuously, instead of starting/stopping it
    // for performance reasins, like done in LikeBreeding
    colorFlow.startWatchingColorFlowValues();





    $scope.navigateToRoots = function() {

      $location.path( "/roots" );

      $analytics.eventTrack('click', {
        category: 'Navigation', label: 'navigateToRootsFromCorral'
      });
    }

    $scope.navigateToBreeding = function() {

      if( networkUserHandles.getAllNetworkUserHandles().length ) {
        $location.path( "/breeding" );
      } else {
        $location.path( "/roots" );
      }



      $analytics.eventTrack('click', {
        category: 'Navigation', label: 'navigateToBreedingFromCorral'
      });
    }

    $scope.navigateToFrontpage = function() {

      $location.path( "/index" );

      $analytics.eventTrack('click', {
        category: 'Navigation', label: 'navigateToFrontpageFromCorral'
      });
    }

  }]);



  app.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/corral/:cubeKey', {
      templateUrl: 'corral/corral.html',
      controller: 'Corral'
    })
    .when('/corral', {
      templateUrl: 'corral/corral.html',
      controller: 'Corral'
    });
  }]);


})(angular);
