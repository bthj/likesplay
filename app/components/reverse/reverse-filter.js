'use strict';

/* Filters */

angular.module('likesplayApp')
  .filter('reverse', function() {
    return function(items) {
      return items.slice().reverse();
    };
  });