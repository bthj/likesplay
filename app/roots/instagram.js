(function (angular) {
  "use strict";

  var app = angular.module('likesplayApp.roots.instagram', ['ngRoute', 'famous.angular']);

  app.controller("InstagramRegistrationController", [
    '$scope',
    '$routeParams',
    '$location',
    '$famous',
    'NETNAME',
    'localStorageManager',
    'networkUserHandles',
    'harvestInstagram',
    'colorFlow',
    '$analytics',
    function(
      $scope,
      $routeParams,
      $location,
      $famous,
      NETNAME,
      localStorageManager,
      networkUserHandles,
      harvestInstagram,
      colorFlow,
      $analytics ) {


    var Transitionable = $famous['famous/transitions/Transitionable'];
    var Easing = $famous['famous/transitions/Easing'];


    ////////////////////////////////////
    // user verification

    // var _username = $routeParams.username;
    // console.log( "_username" );
    // console.log( _username );
    var _afterHash = $location.hash();
    console.log( "_afterHash" );
    console.log( _afterHash );


    var _usernameVerified = undefined;
    var _userId;
    function verifyName() {

      // will send the user to Instagram Client-Side (Implicit) Authentication
      // if there is no access token present, or if it has expired:
      harvestInstagram.doesUserExist(
        $scope.networkUser.name.trim(), setVerificationStatusAndSaveUserId );

      braceLogoForVerificationCheck();
    }

    function setVerificationStatusAndSaveUserId( userId ) {

      if( userId ) {

        _userId = userId;

        _usernameVerified = true;
      } else {
        _usernameVerified = false;
      }
    }

    function saveOrObtainAccessToken() {
      if( _afterHash && _afterHash.indexOf("access_token=") == 0 ) {
        // so we have a user name and an API token to verify it
        // let's begin by saving the access token
        var accessToken = _afterHash.split('=')[1];
        localStorageManager.saveApiAccessToken( NETNAME.INSTAGRAM, accessToken );

        // and then we whould be all set to verify the user name, if it was provided
        // if( _username ) verifyName();

        // let's get rid of that pesky access token in the url
        // $location.path( '/instagram' );
        // history.pushState("", document.title, window.location.pathname);
        window.location.hash = "";
      } else {
        harvestInstagram.populateAccessToken();
      }
    }

    saveOrObtainAccessToken();



    ////////////////////////////////////
    // logo animations and reaction to user name verification:

    $scope.logoTranslator = new Transitionable( [0,0,0] );

    function braceLogoForVerificationCheck() {
      $scope.logoTranslator.set(
        [-window.innerWidth/2.5, 0, 0],
        {duration: 500, curve:'easeOut'}, function() {
          if( undefined === _usernameVerified ) {

            braceLogoForVerificationCheck();

          } else if( true === _usernameVerified ) {

            translateLogoForwardAndSaveUserHandleAndNavigateToRoots();

          } else {

            translateLogoToCenterWithFailEasing();
          }
        }
      );
    }
    function translateLogoToCenterWithFailEasing() {

      $scope.logoTranslator.set(
        [0, 0, 0],
        {
          method: 'spring',
          period: 250,
          dampingRatio: 0.2
        }
      );
    }
    function translateLogoForwardAndSaveUserHandleAndNavigateToRoots() {
      $scope.logoTranslator.set(
        [window.innerWidth/1.5, 0, 0],
        {duration: 200, curve:'easeOut'}, function() {

          var handle = networkUserHandles.addNetworkUser(
            NETNAME.INSTAGRAM,
            $scope.networkUser.name.trim(),
            _userId );

          // we got a user handle if it wasn't already saved
          if( handle ) {
            harvestInstagram.getMediaItemsForInstagramUser( handle );
          }

          _navigateToRoots();
        }
      );
    }



    ////////////////////////////////////
    // setup for view template

    $scope.headerOptions = {
      properties: {
        paddingTop: '5px',
        textAlign: 'center',
        fontSize: '34px',
        color: 'white'
      }
    }

    $scope.ui = {
      inputLegend: "Username",
      networkColor: "#517fa4",
      logoPath: "img/instagram_white.svg",
      inputPartial: "roots/addnetwork_input.html",
      inputPlaceholder: "Instagram",
      inputFontSize: '10vh'
    };

    $scope.networkUser = {
      name: /*_username ||*/ '',
      token: ''
    };


    $scope.addUserHandle = function() {

      verifyName();

      $analytics.eventTrack('click', {
        category: 'UI Interaction', label: 'addUserHandleInstagram'
      });
    }

    function _navigateToRoots() {
      $location.path("/roots");

      $analytics.eventTrack('click', {
        category: 'Navigation', label: 'navigateToRootsFromInstagram'
      });
    }

    $scope.navigateToRoots = _navigateToRoots;



    ////////////////////////////////////
    // colour flow

    $scope.flowRGB = [0, 0, 0];

    function updateFlowRGB( newRGB ) {
      $scope.$apply(function () {
        $scope.flowRGB = newRGB;
      });
    }

    colorFlow.registerColorUpdateCallback( updateFlowRGB );
    colorFlow.startWatchingColorFlowValues();

  }]);

  app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    .when('/instagram', {
      templateUrl: 'roots/addnetwork.html',
      controller: 'InstagramRegistrationController'
    });
    // .when('/instagram/:username', {
    //   templateUrl: 'roots/addnetwork.html',
    //   controller: 'InstagramRegistrationController'
    // });
  }]);

})(angular);
