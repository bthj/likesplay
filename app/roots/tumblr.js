(function (angular) {
  "use strict";

  var app = angular.module('likesplayApp.roots.tumblr', ['ngRoute', 'famous.angular']);

  app.controller("TumblrRegistrationController", [
    '$scope',
    '$famous',
    'NETNAME',
    'networkUserHandles',
    'harvestTumblr',
    '$location',
    'colorFlow',
    '$analytics',
    function(
      $scope,
      $famous,
      NETNAME,
      networkUserHandles,
      harvestTumblr,
      $location,
      colorFlow,
      $analytics ) {

    var Transitionable = $famous['famous/transitions/Transitionable'];
    var Easing = $famous['famous/transitions/Easing'];



    ////////////////////////////////////
    // blog verification

    var _usernameVerified = undefined;
    function verifyName() {

      harvestTumblr.doesBlogExist(
        $scope.networkUser.name.trim(), setVerificationStatus );

      braceLogoForVerificationCheck();
    }
    function setVerificationStatus( isVerified ) {

      _usernameVerified = isVerified;
    }


    ////////////////////////////////////
    // logo animations and reaction to user name verification:

    $scope.logoTranslator = new Transitionable( [0,0,0] );

    function braceLogoForVerificationCheck() {
      $scope.logoTranslator.set(
        [-window.innerWidth/2.5, 0, 0],
        {duration: 500, curve:'easeOut'}, function() {
          if( undefined === _usernameVerified ) {

            braceLogoForVerificationCheck();

          } else if( true === _usernameVerified ) {

            translateLogoForwardAndSaveUserHandleAndNavigateToRoots();

          } else {

            translateLogoToCenterWithFailEasing();
          }
        }
      );
    }
    function translateLogoToCenterWithFailEasing() {

      $scope.logoTranslator.set(
        [0, 0, 0],
        // {duration: 500, curve: Easing.outBounce}
        // {
        //   method: 'spring',
        //   period: 1000,
        //   dampingRatio: 0.1
        // }
        {
          method: 'spring',
          period: 250,
          dampingRatio: 0.2
        }
      );
    }
    function translateLogoForwardAndSaveUserHandleAndNavigateToRoots() {
      $scope.logoTranslator.set(
        [window.innerWidth/1.5, 0, 0],
        {duration: 200, curve:'easeOut'}, function() {

          var handle = networkUserHandles.addNetworkUser(
            NETNAME.TUMBLR,
            $scope.networkUser.name.trim() );

          // we got a user handle if it wasn't already saved
          if( handle ) {
            harvestTumblr.getMediaItemsFromTumblrAccount( handle );
          }

          _navigateToRoots();
        }
      );
    }




    ////////////////////////////////////
    // setup for view template

    $scope.headerOptions = {
      properties: {
        paddingTop: '5px',
        textAlign: 'center',
        fontSize: '34px',
        color: 'white'
      }
    }

    $scope.ui = {
      inputLegend: "Blog name",
      networkColor: "#32506d",
      logoPath: "img/tumblr_white.svg",
      inputPartial: "roots/addnetwork_input.html",
      inputPlaceholder: "Tumblr",
      inputFontSize: '10vh'
    };

    $scope.networkUser = {
      name: '',
      token: ''
    };


    $scope.addUserHandle = function() {

      verifyName();

      $analytics.eventTrack('click', {
        category: 'UI Interaction', label: 'addUserHandleTumblr'
      });
    }

    function _navigateToRoots() {
      $location.path("/roots");

      $analytics.eventTrack('click', {
        category: 'Navigation', label: 'navigateToRootsFromTumblr'
      });
    }

    $scope.navigateToRoots = _navigateToRoots;



    ////////////////////////////////////
    // colour flow

    $scope.flowRGB = [0, 0, 0];

    function updateFlowRGB( newRGB ) {
      $scope.$apply(function () {
        $scope.flowRGB = newRGB;
      });
    }

    colorFlow.registerColorUpdateCallback( updateFlowRGB );
    colorFlow.startWatchingColorFlowValues();

  }]);

  app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    .when('/tumblr', {
      templateUrl: 'roots/addnetwork.html',
      controller: 'TumblrRegistrationController'
    });
  }]);

})(angular);
