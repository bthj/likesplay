(function (angular) {
  "use strict";

  var app = angular.module('likesplayApp.roots', ['ngRoute', 'famous.angular']);

  app.controller('SocialNetworkRegCtrl',
      ['$scope',
      '$http',
      '$location',
      'networkUserHandles',
      'localStorageManager',
      'mediaItemHarvester',
      'harvestTumblr',
      'harvestInstagram',
      'harvestFacebook',
      'NETNAME',
      '$famous',
      '$timeline',
      'colorFlow',
      '$analytics',
      function(
        $scope,
        $http,
        $location,
        networkUserHandles,
        localStorageManager,
        mediaItemHarvester,
        harvestTumblr,
        harvestInstagram,
        harvestFacebook,
        NETNAME,
        $famous,
        $timeline,
        colorFlow,
        $analytics ){


    ////////////////////////////////////
    // famo.us layout configuration

    var Timer = $famous['famous/utilities/Timer'];
    var EventHandler = $famous['famous/core/EventHandler'];
    var Transitionable = $famous['famous/transitions/Transitionable'];
    var Easing = $famous['famous/transitions/Easing'];

    $scope.scrollViewEventHandler = new EventHandler();



    ////////////////////////////////////
    // UI options
    $scope.siteCellLayout = {
      ratios: [2, 1, .3]
    };

    $scope.userHandleLegendOptions = {
      properties: {
        fontSize: '1.3em',
        // paddingTop: '.2em',
        color: 'white',
        textAlign: 'left'
      }
    }
    $scope.userHandleNameOptions = {
      properties: {
        fontSize: '1.7em',
        // paddingTop: '.2em',
        paddingLeft: '.05em',
        color: 'white',
        textAlign: 'center',
        whiteSpace: 'nowrap'
      }
    }
    $scope.userHandleMetaTopOptions = {
      properties: {
        fontSize: '.8em',
        paddingTop: '1.5em',
        paddingRight: '.5em',
        color: 'white',
        textAlign: 'center'
      }
    }
    $scope.userHandleMetaBottomOptions = {
      properties: {
        fontSize: '1.3em',
        // paddingTop: '.5em',
        paddingRight: '.5em',
        color: 'white',
        textAlign: 'center'
      }
    }
    $scope.userHandleDeleteOptions = {
      properties: {
        fontSize: '2em',
        // paddingTop: '.2em',
        color: 'white',
        textAlign: 'center',
        cursor: 'pointer'
      }
    }

    // $scope.siteCellSurfaceLegendOptions = {
    //   properties: {
    //     fontSize: '18px',
    //     paddingTop: '30px',
    //     // color: 'black',
    //     textAlign: 'center'
    //   }
    // }
    // $scope.siteCellSurfaceInputOptions = {
    //   properties: {
    //     paddingTop: '18px'
    //   }
    // }


    // header options
    $scope.headerOptions = {
      properties: {
        paddingTop: '5px',
        textAlign: 'center',
        fontSize: '34px',
        color: 'white',
        zIndex: 1
      }
    }

    $scope.subHead1 = {
      properties: {
        textAlign: 'center',
        fontSize: '18px',
        paddingTop: '12px',
        fontFamily: "'Architects Daughter', cursive"
      }
    }
    $scope.subHead2 = {
      properties: {
        textAlign: 'center',
        paddingRight: '15px',
        paddingTop: '1em',
        fontSize: '1.2em',
        fontFamily: "'Architects Daughter', cursive"
      }
    }
    $scope.subHead3 = {
      properties: {
        textAlign: 'center',
        paddingLeft: '15px',
        paddingTop: '1em',
        fontSize: '1.2em',
        fontFamily: "'Architects Daughter', cursive"
      }
    }

    // footer options
    $scope.buttonGridLayoutOptions = {
      ratios: [1, true]
    };
    $scope.footerLeftOptions = {
      properties: {
        paddingTop: '14px',
        paddingLeft: '15px',
      }
    };
    $scope.buttonOptions = {
      properties: {
        fontSize: '18px',
        paddingTop: '12px',
        color: 'white',
        textAlign: 'center',
        cursor: 'pointer'
      }
    };


    ////////////////////////////////////
    // form submit animation

    $scope.scrollOffset = 0;

    $scope.config = {
      showForm: false,
      entryTransition: new Transitionable( [0,-window.innerHeight*2.4,0] ),
      scrollViewTransition: new Transitionable( 0 )
    };

    // $timeline configuration based on http://strangemilk.com/famous-and-angular-beautiful-animations/

    $scope.rootsScrollTimelines = {
      translate: function( $transition, scrollOffset ) {
        return $timeline([
          [0, [0, 0, 0], Easing.outQuad],
          [.6, [0, -100+scrollOffset/2, -2000], Easing.inBack],
          [1, [0, -2600+scrollOffset/2, -20000]]
        ])( $transition.get() );
      },
      rotateX: function( $transition ) {
        return $timeline([
          [0, 0, Easing.outQuart],
          [.6, 1],
          [1, 1.5]
        ])( $transition.get() );
      },
      opacity: function( $transition ) {
        return $timeline([
          [0, 1],
          [.6, 1, Easing.inCirc],
          [1, 0]
        ])( $transition.get() );
      }
    };


    $scope.playExitAnimationAndNavigateToBreeding = function() {
      var rootsScroll = $famous.find('#rootsScroll')[0].renderNode;
      $scope.scrollOffset = rootsScroll.getAbsolutePosition();
      $scope.config.scrollViewTransition.set( 1, {duration: 2000},
        navigateToBreeding );

    }

    function navigateToBreeding() {

      $scope.$apply(function() {
        $location.path("/breeding");
      });

      $analytics.eventTrack('click', {
        category: 'Navigation', label: 'navigateToBreedingFromRoots'
      });
    }


    angular.element(document).ready(function () {
      // entry animation
      Timer.setTimeout(function() {
        $scope.config.showForm = true;
        $scope.config.entryTransition.set(
          [0,0,0], {duration:1000, curve: Easing.outBack} );
      }, 500);
    });



    ////////////////////////////////////
    // social media sites registration navigation

    $scope.navigateToTumblrRegistration = function() {
      $location.path("/tumblr");

      $analytics.eventTrack('click', {
        category: 'Navigation', label: 'navigateToTumblrRegistration'
      });
    }

    $scope.navigateToInstagramRegistration = function() {
      $location.path("/instagram");

      $analytics.eventTrack('click', {
        category: 'Navigation', label: 'navigateToInstagramRegistration'
      });
    }

    $scope.navigateToFacebookRegistration = function() {
      $location.path("/facebook");

      $analytics.eventTrack('click', {
        category: 'Navigation', label: 'navigateToFacebookRegistration'
      });
    }



    ////////////////////////////////////
    // social media sites form data handling

    $scope.userHandles = networkUserHandles.getAllNetworkUserHandles();


    function addColourToUserHandles() {
      if( $scope.userHandles ) {
        $scope.userHandles.forEach(function( oneHandle ) {
          switch( oneHandle.network ) {
            case NETNAME.TUMBLR:
              oneHandle.colour = "#32506d";
              break;
            case NETNAME.INSTAGRAM:
              // oneHandle.colour = "#517fa4";
              oneHandle.colour = "#675144";
              break;
            case NETNAME.FACEBOOK:
              oneHandle.colour = "#3b5998";
              break;
          }
        });
      }
    }

    addColourToUserHandles();

    // var _handles = {};
    // $scope.formData = {};  // as recommended in http://stackoverflow.com/a/22768720/169858

    // populateUserHandlesFormData();

    // function getUserHandleArraysFromSets( userHandles ) {
    //   var tumblrHandles = [];
    //   var weHeartItHandles = [];
    //   var soundCloudHandles = [];
    //   userHandles.forEach(function( oneHandleEntry, index, array ){
    //     switch( oneHandleEntry.network ) {
    //       case NETNAME.TUMBLR:
    //         tumblrHandles.push( oneHandleEntry.user );
    //         break;
    //       case NETNAME.WEHEARTIT:
    //         weHeartItHandles.push( oneHandleEntry.user );
    //         break;
    //       case NETNAME.SOUNDCLOUD:
    //         soundCloudHandles.push( oneHandleEntry.user );
    //         break;
    //     }
    //   });
    //   var userHandleArrays = {};
    //   userHandleArrays[NETNAME.TUMBLR] = tumblrHandles;
    //   userHandleArrays[NETNAME.WEHEARTIT] = weHeartItHandles;
    //   userHandleArrays[NETNAME.SOUNDCLOUD] = soundCloudHandles;
    //   return userHandleArrays;
    //   // return {
    //   //   (NETNAME.TUMBLR): tumblrHandles,
    //   //   (NETNAME.WEHEARTIT): weHeartItHandles,
    //   //   (NETNAME.SOUNDCLOUD): soundCloudHandles
    //   // }
    // }

    // function populateUserHandlesFormData() {
    //   var userHandles = localStorageManager.getSavedNeworkUserHandles();
    //
    //   if( userHandles && userHandles.length ) {
    //
    //     _handles = getUserHandleArraysFromSets( userHandles );
    //
    //     // var handles = getUserHandleArraysFromSets( userHandles );
    //
    //   //   $scope.formData.tumblr = handles["tumblr"].join( ", " );
    //   //   $scope.formData.weheartit = handles["weHeartIt"].join( ", " );
    //   //   $scope.formData.soundcloud = handles["soundCloud"].join( ", " );
    //   // } else {
    //   //   $scope.formData.tumblr = "";
    //   //   $scope.formData.weheartit = "";
    //   //   $scope.formData.soundcloud = "";
    //   }
    //
    //   // populateSocialNetworksScope();
    // }

    // function areNetworkHandleSetsEqual( handleSet1, handleSet2 ) {
    //   if( handleSet1 && handleSet2 ) {
    //     var handles1 = getUserHandleArraysFromSets( handleSet1 );
    //     var handles2 = getUserHandleArraysFromSets( handleSet2 );
    //     return areScalarValueArraysEqual( handles1["tumblr"], handles2["tumblr"] )
    //             && areScalarValueArraysEqual( handles1["weHeartIt"], handles2["weHeartIt"] )
    //             && areScalarValueArraysEqual( handles1["soundCloud"], handles2["soundCloud"] );
    //   } else {
    //     // returning false here is strictly not correct, both could be undefined,
    //     // but then we want to start from scratch anyway.
    //     return false;
    //   }
    //
    // }
    //
    // function areScalarValueArraysEqual( a1, a2 ) {
    //   // based on http://stackoverflow.com/a/19746771/169858
    //   // (http://stackoverflow.com/a/22395463/169858)
    //   a1.sort();
    //   a2.sort();
    //   return a1.length==a2.length && a1.every(function(v,i) { return v === a2[i]});
    // }


    // $scope.saveUserHandles = function() {
    //
    //   networkUserHandles.clear();
    //
    //   $scope.socialNetworks.forEach( function(oneNet) {
    //     oneNet.formData.split(",").forEach(function(name, index, array){
    //       if( name.trim() ) {
    //         networkUserHandles.addNetworkUser( oneNet.network, name.trim() );
    //       }
    //     });
    //   });
    //
    //   // if( $scope.formData.tumblr ) {
    //   //   $scope.formData.tumblr.split(",").forEach(function(name, index, array){
    //   //     networkUserHandles.addNetworkUser( NETNAME.TUMBLR, name.trim() );
    //   //   });
    //   // }
    //   //
    //   // if( $scope.formData.weheartit ) {
    //   //   $scope.formData.weheartit.split(",").forEach(function(name, index, array){
    //   //     networkUserHandles.addNetworkUser( NETNAME.WEHEARTIT, name.trim() );
    //   //   });
    //   // }
    //   //
    //   // if( $scope.formData.soundcloud ) {
    //   //   $scope.formData.soundcloud.split(",").forEach(function(name, index, array){
    //   //     networkUserHandles.addNetworkUser( NETNAME.SOUNDCLOUD, name.trim() );
    //   //   });
    //   // }
    //
    //   // if( ! areNetworkHandleSetsEqual(
    //   //       networkUserHandles.getAllNetworkUserHandles(),
    //   //       localStorageManager.getSavedNeworkUserHandles() ) ) {
    //   //
    //   //   // localStorageManager.clearAllSavedMediaItems();
    //   // }
    //
    //   localStorageManager.saveNetworkUserHandles(
    //     networkUserHandles.getAllNetworkUserHandles() );
    //
    //
    //   mediaItemHarvester.fetchMediaItemsForAllUserHandles();
    //
    //
    //   playExitAnimationAndNavigateToBreeding();
    // }

    // $scope.clearSavedData = function() {
    //   localStorageManager.clearAllSavedMediaItems();
    //   localStorageManager.clearSavedNetworkUserHandles();
    //   populateUserHandlesFormData();
    // }

    $scope.removeNetworkAccountAndClearItsData = function( handle ) {

      $scope.userHandles.forEach( function(oneActiveHandle, index) {
        if( handle.network == oneActiveHandle.network
          && handle.user == oneActiveHandle.user ) {

            localStorageManager.clearSavedMediaItems(
              handle.network, handle.user );

            $scope.userHandles.splice( index, 1 );

            localStorageManager.saveNetworkUserHandles( $scope.userHandles );

            mediaItemHarvester.clearMediaItemCountForHandle( handle );
          }
      });

      $analytics.eventTrack('click', {
        category: 'UI Interaction', label: 'removeAccount', value: handle.network
      });
    }

    $scope.refreshAccount = function( handle ) {
      localStorageManager.clearSavedMediaItems(
        handle.network, handle.user );

      switch( handle.network ) {
        case NETNAME.TUMBLR:
          harvestTumblr.clearMediaItemCount();
          harvestTumblr.getMediaItemsFromTumblrAccount( handle );
          break;
        case NETNAME.INSTAGRAM:
          harvestInstagram.clearMediaItemCount();
          harvestInstagram.getMediaItemsForInstagramUser( handle );
          break;
        case NETNAME.FACEBOOK:
          harvestFacebook.clearMediaItemCount();
          harvestFacebook.getMediaItemsForFacebookUser( handle );
          break;
        default:
          break;
      }

      $analytics.eventTrack('click', {
        category: 'UI Interaction', label: 'refreshAccount', value: handle.network
      });
    }

    $scope.refreshMediaItems = function() {

      localStorageManager.clearAllSavedMediaItems();
      mediaItemHarvester.fetchMediaItemsForAllUserHandles();

      $analytics.eventTrack('click', {
        category: 'UI Interaction', label: 'refreshAllMediaItems'
      });
    }



    ////////////////////////////////////
    // colour flow

    $scope.flowRGB = [0, 0, 0];

    function updateFlowRGB( newRGB ) {
      $scope.$apply(function () {
        $scope.flowRGB = newRGB;
      });
    }


    ////////////////////////////////////
    // collected media items count update

    function updateMediaCount() {
      // TODO: maybe a service should do this switching
      if( $scope.userHandles ) {
        $scope.userHandles.forEach(function( oneHandle ) {

          oneHandle.count = mediaItemHarvester.getMediaCount( oneHandle );
        });
      }
    }



    if( $scope.userHandles.length ) {
      colorFlow.registerColorUpdateCallback( updateFlowRGB );
      colorFlow.startWatchingColorFlowValues();


      Timer.setInterval(function() {

        updateMediaCount();

      }, 1000);
    }



    /// social network handles
    // $scope.socialNetworks;
    // function populateSocialNetworksScope() {
    //   $scope.socialNetworks = [
    //    {
    //      faIndex: 0,
    //      network: NETNAME.TWITTER,
    //      backgroundColor: '#00aced',
    //      label: 'Twitter',
    //      formData: _handles[NETNAME.TWITTER] ? _handles[NETNAME.TWITTER].join( ", " ) : "",
    //      inputId: 'username-twitter',
    //      disabled: 'disabled',
    //      placeholder: 'working on it'
    //    },
    //    {
    //      faIndex: 1,
    //      network: NETNAME.TUMBLR,
    //      backgroundColor: '#32506d',
    //      label: 'Tumblr',
    //      formData: _handles[NETNAME.TUMBLR] ? _handles[NETNAME.TUMBLR].join( ", " ) : "",
    //      inputId: 'username-tumblr',
    //      disabled: '',
    //      placeholder: 'enter your username'
    //    },
    //    {
    //      faIndex: 2,
    //      network: NETNAME.PINTEREST,
    //      backgroundColor: '#cb2027',
    //      label: 'Pinterest',
    //      formData: _handles[NETNAME.PINTEREST] ? _handles[NETNAME.PINTEREST].join( ", " ) : "",
    //      inputId: 'username-pinterest',
    //      disabled: 'disabled',
    //      placeholder: 'working on it'
    //    },
    //    {
    //      faIndex: 3,
    //      network: NETNAME.WEHEARTIT,
    //      backgroundColor: '#f69',
    //      label: 'We Heart It',
    //      formData: _handles[NETNAME.WEHEARTIT] ? _handles[NETNAME.WEHEARTIT].join( ", " ) : "",
    //      inputId: 'username-weheartit',
    //      disabled: 'disabled',
    //      placeholder: 'working on it'
    //    },
    //    {
    //      faIndex: 4,
    //      network: NETNAME.ELLO,
    //      backgroundColor: '#c5c5c5',
    //      label: 'Ello',
    //      formData: _handles[NETNAME.ELLO] ? _handles[NETNAME.ELLO].join( ", " ) : "",
    //      inputId: 'username-ello',
    //      disabled: 'disabled',
    //      placeholder: 'working on it'
    //    },
    //    {
    //      faIndex: 5,
    //      network: NETNAME.FACEBOOK,
    //      backgroundColor: '#3b5998',
    //      label: 'Facebook',
    //      formData: _handles[NETNAME.FACEBOOK] ? _handles[NETNAME.FACEBOOK].join( ", " ) : "",
    //      inputId: 'username-facebook',
    //      disabled: 'disabled',
    //      placeholder: 'might wire it soon'
    //    },
    //    {
    //      faIndex: 6,
    //      network: NETNAME.GOOGLEPLUS,
    //      backgroundColor: '#dd4b39',
    //      label: 'Google+',
    //      formData: _handles[NETNAME.GOOGLEPLUS] ? _handles[NETNAME.GOOGLEPLUS].join( ", " ) : "",
    //      inputId: 'username-googleplus',
    //      disabled: 'disabled',
    //      placeholder: 'might wire it soon'
    //    },
    //    {
    //      faIndex: 7,
    //      network: NETNAME.INSTAGRAM,
    //      backgroundColor: '#517fa4',
    //      label: 'Instagram',
    //      formData: _handles[NETNAME.INSTAGRAM] ? _handles[NETNAME.INSTAGRAM].join( ", " ) : "",
    //      inputId: 'username-instagram',
    //      disabled: 'disabled',
    //      placeholder: 'might wire it soon'
    //    },
    //    {
    //      faIndex: 8,
    //      network: NETNAME.FLICKR,
    //      backgroundColor: '#ff0084',
    //      label: 'Flickr',
    //      formData: _handles[NETNAME.FLICKR] ? _handles[NETNAME.FLICKR].join( ", " ) : "",
    //      inputId: 'username-flickr',
    //      disabled: 'disabled',
    //      placeholder: 'might wire it soon'
    //    },
    //    {
    //      faIndex: 9,
    //      network: NETNAME.VINE,
    //      backgroundColor: '#00bf8f',
    //      label: 'Vine',
    //      formData: _handles[NETNAME.VINE] ? _handles[NETNAME.VINE].join( ", " ) : "",
    //      inputId: 'username-vine',
    //      disabled: 'disabled',
    //      placeholder: 'might wire it soon'
    //    },
    //    {
    //      faIndex: 10,
    //      network: NETNAME.YOUTUBE,
    //      backgroundColor: '#bb0000',
    //      label: 'YouTube',
    //      formData: _handles[NETNAME.YOUTUBE] ? _handles[NETNAME.YOUTUBE].join( ", " ) : "",
    //      inputId: 'username-youtube',
    //      disabled: 'disabled',
    //      placeholder: 'might wire it soon'
    //    },
    //    {
    //      faIndex: 11,
    //      network: NETNAME.VIMEO,
    //      backgroundColor: '#f7b42c',
    //      label: 'Vimeo',
    //      formData: _handles[NETNAME.VIMEO] ? _handles[NETNAME.VIMEO].join( ", " ) : "",
    //      inputId: 'username-vimeo',
    //      disabled: 'disabled',
    //      placeholder: 'might wire it soon'
    //    },
    //    {
    //      faIndex: 12,
    //      network: NETNAME.SOUNDCLOUD,
    //      backgroundColor: '#f50',
    //      label: 'SoundCloud',
    //      formData: _handles[NETNAME.SOUNDCLOUD] ? _handles[NETNAME.SOUNDCLOUD].join( ", " ) : "",
    //      inputId: 'username-soundcloud',
    //      disabled: 'disabled',
    //      placeholder: 'might wire it soon'
    //    },
    //    {
    //      faIndex: 13,
    //      network: NETNAME.SPOTIFY,
    //      backgroundColor: '#84bd00',
    //      label: 'Spotify',
    //      formData: _handles[NETNAME.SPOTIFY] ? _handles[NETNAME.SPOTIFY].join( ", " ) : "",
    //      inputId: 'username-spotify',
    //      disabled: 'disabled',
    //      placeholder: 'might wire it soon'
    //    },
    //    {
    //      faIndex: 14,
    //      network: NETNAME.DEVIANTART,
    //      backgroundColor: '#d4dfd0',
    //      label: 'DeviantArt',
    //      formData: _handles[NETNAME.DEVIANTART] ? _handles[NETNAME.DEVIANTART].join( ", " ) : "",
    //      inputId: 'username-deviantart',
    //      disabled: 'disabled',
    //      placeholder: 'might wire it soon'
    //    }
    //  ];
    // }


  }]);



  app.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/roots', {
      templateUrl: 'roots/socialnetworkregistration.html',
      controller: 'SocialNetworkRegCtrl'
    });
  }]);


})(angular);
