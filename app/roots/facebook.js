(function (angular) {
  "use strict";

  var app = angular.module('likesplayApp.roots.facebook', ['ngRoute', 'famous.angular']);

  app.controller("FacebookRegistrationController", [
    '$scope',
    '$location',
    '$famous',
    'colorFlow',
    'networkUserHandles',
    'harvestFacebook',
    'NETNAME',
    '$analytics',
    function(
      $scope,
      $location,
      $famous,
      colorFlow,
      networkUserHandles,
      harvestFacebook,
      NETNAME,
      $analytics ) {

    var Transitionable = $famous['famous/transitions/Transitionable'];
    var Easing = $famous['famous/transitions/Easing'];



    ////////////////////////////////////
    // user verification

    var _usernameVerified = undefined;
    var _userName;
    var _userId;

    function setVerificationStatusAndSaveUserId( userId, userName ) {

      if( userId ) {

        _userId = userId;
        _userName = userName;

        _usernameVerified = true;
      } else {
        _usernameVerified = false;
      }
    }


    $scope.addUserHandle = function() {

      braceLogoForVerificationCheck();

      FB.login(function(response) {
        if( response.authResponse ) {
          console.log('Welcome!  Fetching your information.... ');

          var userId = response.authResponse.userID;

          FB.api('/me', function(response) {
            console.log('Good to see you, ' + response.name + '.');

            setVerificationStatusAndSaveUserId( userId, response.name );

          });
        } else {
          console.log('User cancelled login or did not fully authorize.');

          setVerificationStatusAndSaveUserId( null );
        }
      }, {scope: 'user_photos, user_videos'});


      $analytics.eventTrack('click', {
        category: 'UI Interaction', label: 'addUserHandleFacebook'
      });
    }



    ////////////////////////////////////
    // logo animations and reaction to user name verification:

    $scope.logoTranslator = new Transitionable( [0,0,0] );

    function braceLogoForVerificationCheck() {
      $scope.logoTranslator.set(
        [-window.innerWidth/2.5, 0, 0],
        {duration: 500, curve:'easeOut'}, function() {
          if( undefined === _usernameVerified ) {

            braceLogoForVerificationCheck();

          } else if( true === _usernameVerified ) {

            translateLogoForwardAndSaveUserHandleAndNavigateToRoots();

          } else {

            translateLogoToCenterWithFailEasing();
          }
        }
      );
    }
    function translateLogoToCenterWithFailEasing() {

      $scope.logoTranslator.set(
        [0, 0, 0],
        {
          method: 'spring',
          period: 250,
          dampingRatio: 0.2
        }
      );
    }
    function translateLogoForwardAndSaveUserHandleAndNavigateToRoots() {
      $scope.logoTranslator.set(
        [window.innerWidth/1.5, 0, 0],
        {duration: 200, curve:'easeOut'}, function() {

          var handle = networkUserHandles.addNetworkUser(
            NETNAME.FACEBOOK,
            _userName,
            _userId );

          // we got a user handle if it wasn't already saved
          // if( handle ) {
            harvestFacebook.getMediaItemsForFacebookUser( handle );
          // }

          _navigateToRoots();
        }
      );
    }



    ////////////////////////////////////
    // setup for view template

    $scope.headerOptions = {
      properties: {
        paddingTop: '5px',
        textAlign: 'center',
        fontSize: '34px',
        color: 'white'
      }
    }

    $scope.ui = {
      inputLegend: "Facebook",
      networkColor: "#3b5998",
      logoPath: "img/facebook_white.svg",
      inputPartial: "roots/addnetwork_fbloginbutton.html",
      inputFontSize: '4vh'
    };

    $scope.networkUser = {
      name: '',
      token: ''
    };


    function _navigateToRoots() {
      $location.path("/roots");

      $analytics.eventTrack('click', {
        category: 'Navigation', label: 'navigateToRootsFromFacebook'
      });
    }

    $scope.navigateToRoots = _navigateToRoots;



    ////////////////////////////////////
    // colour flow

    $scope.flowRGB = [0, 0, 0];

    function updateFlowRGB( newRGB ) {
      $scope.$apply(function () {
        $scope.flowRGB = newRGB;
      });
    }

    colorFlow.registerColorUpdateCallback( updateFlowRGB );
    colorFlow.startWatchingColorFlowValues();

  }]);


  app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    .when('/facebook', {
      templateUrl: 'roots/addnetwork.html',
      controller: 'FacebookRegistrationController'
    });
  }]);

})(angular);
