(function (angular) {
  "use strict";

  var app = angular.module('likesplayApp.embedtest', ['ngRoute']);

  app.controller( 'EmbedTest', ['$scope','$http','$sce',function($scope, $http, $sce){

    var _data;

    $scope.surfaces = [];

    $http.get( '//iframe.ly/api/iframely?url=' + encodeURI('//ringulr.bthj.is/post/114069685141') + '&api_key=4dad98eeb233482b1564af' )
        .success(function(data) {

          $scope.surfaces.push({
            color: 'red',
            zIndex: 0,
            embed: $sce.trustAsHtml(data.html)
          });

    });

    $http.get( '//iframe.ly/api/iframely?url=' + encodeURI('//vine.co/v/O9juzaJAz6D') + '&api_key=4dad98eeb233482b1564af' )
        .success(function(data) {

          $scope.surfaces.push({
            color: 'red',
            zIndex: 0,
            embed: $sce.trustAsHtml(data.html)
          });

    });
    $http.get( '//iframe.ly/api/iframely?url=' + encodeURI('//ringulr.bthj.is/post/117075960987/likesplay-happy-earth-day-2015-from') + '&api_key=4dad98eeb233482b1564af' )
        .success(function(data) {

          $scope.surfaces.push({
            color: 'red',
            zIndex: 0,
            embed: $sce.trustAsHtml(data.html)
          });

    });
    $http.get( '//open.iframe.ly/api/oembed?url=' + encodeURI('//ringulr.bthj.is/post/116013851407/stofnar-falla') + '&origin=bthj' )
        .success(function(data) {

      // console.log( data.links.app[0].html );
      // $("#testembed").html( data.links.app[0].html );
      // $("#testembed").html( data.links.player[0].html );
      // $("#testembed").html( data.html );

      _data = data;

      console.log( _data );

      $scope.surfaces.push({
        color: 'red',
        zIndex: 0,
        embed: $sce.trustAsHtml(data.html)
      });

      // $("#testembed").html( $('<img>').attr("src", _data.thumbnail_url) );
      // $("#testembed").html( _data.html );
      // $("#testembed").html( _data.links.app[0].html );
    });

    $scope.surfaceClick = function( index ) {

      alert( "index " + index + " clicked");
    }
  }]);

  app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    .when('/embedtest', {
      templateUrl: 'embedtest/embedtest.html',
      controller: 'EmbedTest'
    });
  }]);


})(angular);
