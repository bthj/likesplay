'use strict';

// Declare app level module which depends on filters, and services
angular.module('likesplayApp', [
    'likesplayApp.config',
    'likesplayApp.security',
    'likesplayApp.home',
    'likesplayApp.account',
    'likesplayApp.chat',
    'likesplayApp.login',

    'likesplayApp.roots',
    'likesplayApp.roots.tumblr',
    'likesplayApp.roots.instagram',
    'likesplayApp.roots.facebook',
    'likesplayApp.breeding',
    'likesplayApp.corral',
    'likesplayApp.cubes',
    'likesplayApp.oauth',
    'likesplayApp.embedtest',
    'likesplayApp.index',

    'likesplayApp.services.ui',
    'likesplayApp.services.auth',
    'likesplayApp.services.data',
    'likesplayApp.services.data.local',
    'likesplayApp.services.data.harvest',
    'likesplayApp.services.breeding',
    'likesplayApp.services.randomization',
    'likesplayApp.services.location',

    'angulartics',
    'angulartics.google.analytics'
  ])

  .run(['$rootScope', 'Auth', '$famous', '$window',
  function($rootScope, Auth, $famous, $window) {
    // track status of authentication
    Auth.$onAuth(function(user) {
      $rootScope.loggedIn = !!user;
    });


    // background starfield options
    $rootScope.mediaItemsBackgroundSurfaceOptions = {
      properties: {
        // backgroundImage: "url('img/backgrounds/starnetblog_cloudy_starfield_texture5.jpg')",
        backgroundImage: "url('img/backgrounds/moon_and_earth_lroearthrise_frame.jpg')",
        // backgroundRepeat: 'repeat',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center center',
        zIndex: -100000,
      }
    };

    // central famo.us easing method registration
    var Transitionable = $famous['famous/transitions/Transitionable'];
    var SpringTransition = $famous['famous/transitions/SpringTransition'];
    var SnapTransition = $famous['famous/transitions/SnapTransition'];
    Transitionable.registerMethod('spring', SpringTransition);
    Transitionable.registerMethod('snap', SnapTransition);



    // facebook sdk initialization
    // see https://developers.facebook.com/docs/javascript/howto/angularjs
    // https://developers.facebook.com/docs/facebook-login/login-flow-for-web/v2.3

    $window.fbAsyncInit = function() {
        FB.init({
          appId: '374703969397357',
          // status: true,
          // cookie: true,
          xfbml: true,
          version: 'v2.3'
        });
    };

    // Load the SDK asynchronously
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

  }]);
