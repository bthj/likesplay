(function() {
   'use strict';

   /* User Authentication Service, using Firebase */

   angular.module('likesplayApp.services.auth', ['firebase.auth','firebase.utils'])

   .factory('authService', ['Auth', 'fbutil', function(Auth, fbutil){

     var _provider;
     var _userId;
     var _fullName;


     // find a suitable name based on the meta info given by each provider
     // from: https://www.firebase.com/docs/web/guide/user-auth.html
     function getAuthenticatedUserName( authData ) {
       switch(authData.provider) {
          case 'google':
            return authData.google.displayName;
          case 'twitter':
            return authData.twitter.displayName;
          case 'facebook':
            return authData.facebook.displayName;
       }
     }

     function getAuthenticatedUserHandle( authData ) {
       switch(authData.provider) {
          case 'google':
            return authData.google.email;
          case 'twitter':
            return authData.twitter.username;
          case 'facebook':
            return authData.facebook.email;
       }
     }

     function authenticate( provider ) {
       Auth.$authWithOAuthPopup( provider, function(error, authData) {
         if( error ) {
           if( error.code === "TRANSPORT_UNAVAILABLE" ) {
             // popup not available, so we'll redirect instead
             Auth.$authWithOAuthRedirect( provider, function(error) {
               if( error ) {
                 alert( "Login with " + provider + " failed." );
               }
             });
           } else {
             alert( "Login with " + provider + " failed." );
           }
         } else {
           console.log( authData );
         }
       });
     }

     function storeUserDataInDB( authData ) {

       fbutil.ref("people").child(authData.uid).once("value", function(data) {

         if( data.val() === null ) {
           // this user has not been stored, let's to that!
           var userData = {
             provider: authData.provider,
             name: getAuthenticatedUserName( authData )
           }
           var userHandle = getAuthenticatedUserHandle( authData );
           if( userHandle ) {
             userData.handle = userHandle;
           }
           fbutil.ref("people").child(authData.uid).set( userData );
         }
       });
     }

     function clearUserData() {

       _userId = null;
       _fullName = null;
     }

     function setUserData( authData ) {

       _provider = authData.provider;
       _userId = authData.uid;
       _fullName = getAuthenticatedUserName( authData );

       storeUserDataInDB( authData );
     }

     Auth.$onAuth( function(authData) {

       if( authData ) {
         setUserData( authData );
       } else {
         clearUserData();
       }
     });


     return {

       authenticateWithFacebook: function() {
         authenticate( 'facebook' );
       },

       authenticateWithGoogle: function() {
         authenticate( 'google' );
       },

       authenticateWithTwitter: function() {
         authenticate( 'twitter' );
       },

       clearUserData: clearUserData,

       setUserData: setUserData,


       getProvider: function() {
         return _provider;
       },
       getUserId: function() {
         return _userId;
       },

       getUserFullName: function() {
         return _fullName;
       },

       isLoggedIn: function() {
         return _userId !== null;
       }

     };

   }])

})();
