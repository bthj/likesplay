(function() {
   'use strict';

   /* Services for data harvesting from remote APIs */

   angular.module('likesplayApp.services.data.harvest', [])

     // social network name keys
     .constant('NETNAME', {
       TWITTER: 'Twitter',
       TUMBLR: 'Tumblr',
       PINTEREST: 'Pinterest',
       WEHEARTIT: 'WeHeartIt',
       ELLO: "Ello",
       FACEBOOK: "Facebook",
       GOOGLEPLUS: "GooglePlus",
       INSTAGRAM: "Instagram",
       FLICKR: "Flickr",
       VINE: "Vine",
       YOUTUBE: "YouTube",
       VIMEO: "Vimeo",
       SOUNDCLOUD: 'SoundCloud',
       SPOTIFY: "Spotify",
       DEVIANTART: "DeviantArt"
     })

     .factory('networkUserHandles', [
       'localStorageManager', function(localStorageManager) {

       var _networkHandles = [];

       function getNetworkUser( network, user ) {
         var networkUser = null;
         for( var i=0; i < _networkHandles.length; i++ ) {
           if( _networkHandles[i].network == network
              && _networkHandles[i].user == user ) {

             networkUser = _networkHandles[i];
             break;
           }
         }
         return networkUser;
       }

       function addNetworkUser( network, user, userId ) {
         // let's first refresh from localStorage
         getAllNetworkUserHandles();
         var existingHandle = getNetworkUser( network, user );
         if( ! existingHandle ) {

           var handle = {
             'network': network,
             'user': user,
             'userId': userId
           };

           _networkHandles.push( handle );

           localStorageManager.saveNetworkUserHandles( _networkHandles );

           return handle;
         } else {
           return existingHandle;
         }
       }
       function getAllNetworkUserHandles() {
         var savedHandles = localStorageManager.getSavedNeworkUserHandles();
         if( savedHandles ) _networkHandles = savedHandles;
         return _networkHandles;
       }
       function getUserHandlesForNetwork( network ) {
         var userHandles = [];
         _networkHandles.forEach(function(element, index, array){
           if( element.network == network ) userHandles.push( element.user );
         });
         return userHandles;
       }
       function clear() {
         _networkHandles = [];
       }

       return {
         addNetworkUser: addNetworkUser,
         getAllNetworkUserHandles: getAllNetworkUserHandles,
         getUserHandlesForNetwork: getUserHandlesForNetwork,
         clear: clear
       }
     }])



     .factory('mediaItemHarvester',
         ['localStorageManager', 'networkUserHandles', 'NETNAME',
         'harvestTumblr', 'harvestInstagram', 'harvestFacebook',
         function(localStorageManager, networkUserHandles, NETNAME,
           harvestTumblr, harvestInstagram, harvestFacebook ) {

        function allItems() {

          return localStorageManager.getAllMediaItems();
        }

        function fetchMediaItemsForAllUserHandles() {

          clearMediaCounters();

          networkUserHandles.getAllNetworkUserHandles().forEach(function(handle, index, array){
            // console.log( handle.network + ': ' + handle.user );

            fetchMediaItemsForOneHandle( handle );
          });
        }

        function fetchMediaItemsForOneHandle( handle ) {
          switch( handle.network ) {
            case NETNAME.TUMBLR:
              harvestTumblr.getMediaItemsFromTumblrAccount( handle );
              break;
            case NETNAME.INSTAGRAM:
              harvestInstagram.getMediaItemsForInstagramUser( handle );
              break;
            case NETNAME.FACEBOOK:
              harvestFacebook.getMediaItemsForFacebookUser( handle )
              break;
            default:
              break;
          }
        }

        function getMediaCount( handle ) {
          switch( handle.network ) {
            case NETNAME.TUMBLR:
              return harvestTumblr.getMediaItemCount( handle );
            case NETNAME.INSTAGRAM:
              return harvestInstagram.getMediaItemCount( handle );
            case NETNAME.FACEBOOK:
              return harvestFacebook.getMediaItemCount( handle );
            default:
              return null;
          }
        }
        function clearMediaItemCountForHandle( handle ) {
          switch( handle.network ) {
            case NETNAME.TUMBLR:
              harvestTumblr.clearMediaItemCount();
              break;
            case NETNAME.INSTAGRAM:
              harvestInstagram.clearMediaItemCount();
              break;
            case NETNAME.FACEBOOK:
              harvestFacebook.clearMediaItemCount();
              break;
            default:
              break;
          }
        }
        function clearMediaCounters() {
          networkUserHandles.getAllNetworkUserHandles().forEach(function(handle){
            clearMediaItemCountForHandle( handle );
          });
        }

        return {
         allItems: allItems,
         getMediaCount: getMediaCount,
         clearMediaItemCountForHandle: clearMediaItemCountForHandle,
         fetchMediaItemsForAllUserHandles: fetchMediaItemsForAllUserHandles
        };

     }])


     .factory('harvestPaging', ['localStorageManager', function(localStorageManager) {

       // TODO: queue instead of parallel? multiple simultaneous requests might be to heavy for mobile devices
       var waitTimeBetweenRequests = 1000;

       function initiateNextPageRequestOrSave(
         totalItems, currentOffset, limit, handle, mediaItems, callback ) {

         var newOffset = currentOffset + limit;

         console.log("newOffset: " + newOffset + ", totalItems: " + totalItems + ", total collected: " + Object.keys(mediaItems).length + ", for: " + callback.name);

         if( newOffset < totalItems ) {

           setTimeout( function(){

             // callback( handle, mediaItems, newOffset, reEncounterCount );

             // other arguments are prepended to newOffset configured with a
             //  .bind() call, where this callback was sent as an argument:
             callback( newOffset );

           }, waitTimeBetweenRequests);
         } else {
           console.log( "Finished paging through " + callback.name +
            " and the current total of harvested media items is: " + Object.keys(mediaItems).length );


           localStorageManager.saveMediaItems( mediaItems, handle );
         }
       }

       return {
         initiateNextPageRequestOrSave: initiateNextPageRequestOrSave
       };

     }])


     .factory('harvestTumblr',
       ['$http', 'harvestPaging', 'NETNAME', 'localStorageManager',
       function($http, harvestPaging, NETNAME, localStorageManager ) {

       var tumblrAPIkey = "Djl1CTqPhHhlCsDP9PKcvXptI4jSMmr6QbObhcDVRt4RhTTTdi"
       var tumblrBeforeUser = "http://api.tumblr.com/v2/blog/";
       var tumblrAfterUser = ".tumblr.com";
       var tumblrPosts = "/posts";
       var tumblrLikes = "/likes";
       var tumblrBeforeLimit = "?limit=";

       var tumblrBeforeOffset = "&offset=";
       var tumblrBeforeBefore = "&before=";

       var tumblrBeforeApiKey = "&api_key="
       var tumblrAfterApiKey = "&callback=JSON_CALLBACK";

       var limit = 20;

       // how many posts do we want to encounter again, compared to what's already
       // in mediaItems, before determining that we have harvested all new posts:
       var maxPostReReads = 3;  // Why 3?  I don't know.  Double Tap should do it http://youtu.be/JmA2WYyw-_A


       ///////
       // reporting number of collected media items

       var _mediaItemCounts = {};
       function clearMediaItemCount() {
         _mediaItemCounts = {};
       }
       function incrementMediaItemCount( handle ) {
         if( _mediaItemCounts[handle.user] ) {
           _mediaItemCounts[handle.user]++;
         } else{
           _mediaItemCounts[handle.user] = 1;
         }
       }
       function getMediaItemCount( handle ) {
         if( _mediaItemCounts[handle.user] === undefined ) {
           var savedItems = localStorageManager.getSavedMediaItems( handle );
           _mediaItemCounts[handle.user] = Object.keys(savedItems).length;
         }
         return _mediaItemCounts[handle.user];
       }


       ///////
       // harvest media items

       function getMediaItemsFromTumblrAccount( handle ) {

         var mediaItems = localStorageManager.getSavedMediaItems( handle );

         var startingFromEmpty = Object.keys(mediaItems).length == 0;

         var reEncounterCountStart = startingFromEmpty ? -Number.MAX_VALUE : 0;

         getPostsFromTumblrAccount( handle, mediaItems, reEncounterCountStart, 0 );

         var initialLikesUrl = getLikesUrl(
           handle, Date.now ? Date.now() : new Date().getTime() );
         getLikesFromTumblrAccount(
           initialLikesUrl, handle, mediaItems, reEncounterCountStart );
       }

       function getMediaUrl( post ) {
         var mediaUrl;
         switch( post.type ) {
           case "photo":
              if( post.photos[0].alt_sizes.length > 1 ) {
                mediaUrl = post.photos[0].alt_sizes[1].url;
              } else {
                mediaUrl = post.photos[0].alt_sizes[0].url;
              }
              break;
           case "video":
              if( post.thumbnail_url ) {
                mediaUrl = post.thumbnail_url;
              } else {
                mediaUrl = ""; // "img/video_200.png";
              }
              break;
           case "audio":
              if( post.album_art ) {
                mediaUrl = post.album_art;
              } else {
                mediaUrl = ""; // "img/audio_200.png";
              }
              break;
           default:
              mediaUrl = "img/heart_background_square.png";
         }
         return mediaUrl;
       }

       function getMediaItemPopulatedFromPostData( post ) {
         var mediaItem = {
           "type": post.type,
           "mediaUrl": getMediaUrl( post ),
           "tags": post.tags,
           "isPlayer": new Boolean(post.type == "video" || post.type == "audio"),
           "date": post.date,
           "timestamp": post.timestamp,
           "creator": post.blog_name, // or post.source_title ?
           "provider": NETNAME.TUMBLR,
           "sourceUrl": post.post_url // won't really need this here, as it's now they key, but might be handy
         };
         if( post.caption ) {
           mediaItem.caption = post.caption;
         }
         if( post.player ) {
           mediaItem.player = post.player;
         }
         if( post.photos ) {
           mediaItem.photos = post.photos;
         }
         if( post.thumbnail_height || post.thumbnail_width || post.thumbnail_url ) {
           mediaItem.thumbnail_height = post.thumbnail_height;
           mediaItem.thumbnail_width = post.thumbnail_width;
           mediaItem.thumbnail_url = post.thumbnail_url;
         }
         return mediaItem;
       }


       function doesBlogExist( blogName, callback ) {
         $http.jsonp(
           tumblrBeforeUser
           +blogName
           +tumblrAfterUser
           +"/info?api_key="
           +tumblrAPIkey
           +tumblrAfterApiKey
         ).success(function(data, status, headers, config) {

           callback( data.meta.status === 200 );

         }).error(function(data, status, headers, config) {
           callback( data.meta.status === 200 );
         });
       }

       function getLikesUrl( handle, timestamp ) {
         return tumblrBeforeUser
           +handle.user
           +tumblrAfterUser
           +tumblrLikes
           +tumblrBeforeLimit
           +limit
           +tumblrBeforeBefore
           +timestamp
           +tumblrBeforeApiKey
           +tumblrAPIkey
           +tumblrAfterApiKey;
       }
      //  var _inspectedLikes = 0;
       function getLikesFromTumblrAccount( url, handle, mediaItems, reEncounterCount ) {

         var oldestTimestamp;

        //  console.log("likes url:");
        //  console.log( url );

         $http.jsonp( url )
           .success(function(data) {

             console.log( data.response );
             data.response.liked_posts.forEach(function(post, index, array) {

              //  _inspectedLikes++;

               if( (post.type == "photo" || post.type == "video" || post.type == "audio")
                // && post.tags.length
                && !mediaItems[post.post_url]
                && getMediaUrl( post ) ) {

                 var oneMediaItem = getMediaItemPopulatedFromPostData( post );

                 mediaItems[post.post_url] = oneMediaItem;
                 incrementMediaItemCount( handle );

               } else if( mediaItems[post.post_url] ) {
                 reEncounterCount++;
               }

               if( ! oldestTimestamp || post.timestamp < oldestTimestamp ) {
                 oldestTimestamp = post.liked_timestamp;
               }
             });
            //  console.log( "_inspectedLikes:" );
            //  console.log( _inspectedLikes );
             if( oldestTimestamp && reEncounterCount < maxPostReReads ) {

               setTimeout( function() {

                 var nextLikesUrl = getLikesUrl( handle, oldestTimestamp );
                 getLikesFromTumblrAccount( nextLikesUrl, handle, mediaItems, reEncounterCount );

               }, 1000 );

              //  harvestPaging.initiateNextPageRequestOrSave(
              //    data.response.liked_count,
              //    offset,
              //    limit,
              //    handle,
              //    mediaItems,
              //    getLikesFromTumblrAccount.bind(
              //       null, handle, mediaItems, reEncounterCount )
              //   );
            } else {
              console.log( "done paging through tumblr likes" );
              localStorageManager.saveMediaItems( mediaItems, handle );
            }
         });
       }

       function getPostsFromTumblrAccount( handle, mediaItems, reEncounterCount, offset ) {

         $http.jsonp(
           tumblrBeforeUser
           +handle.user
           +tumblrAfterUser
           +tumblrPosts
           +tumblrBeforeLimit
           +limit
           +tumblrBeforeOffset
           +offset
           +tumblrBeforeApiKey
           +tumblrAPIkey
           +tumblrAfterApiKey )
           .success(function(data) {

             console.log( data.response );
             data.response.posts.forEach(function(post, index, array){

               if( (post.type == "photo" || post.type == "video" || post.type == "audio")
               //  && post.tags.length
               && !mediaItems[post.post_url]
               && getMediaUrl( post ) ) {

                 var oneMediaItem = getMediaItemPopulatedFromPostData( post );

                 mediaItems[post.post_url] = oneMediaItem;
                 incrementMediaItemCount( handle );

               } else if( mediaItems[post.post_url] ) {
                 reEncounterCount++;
               }
             });
             if( reEncounterCount < maxPostReReads ) {
               harvestPaging.initiateNextPageRequestOrSave(
                 data.response.total_posts,
                 offset,
                 limit,
                 handle,
                 mediaItems,
                 getPostsFromTumblrAccount.bind(
                   null, handle, mediaItems, reEncounterCount )
                );
             }
         });

       }

       return {
         doesBlogExist: doesBlogExist,
         getMediaItemCount: getMediaItemCount,
         clearMediaItemCount: clearMediaItemCount,
         getMediaItemsFromTumblrAccount: getMediaItemsFromTumblrAccount
       };

     }])


     .factory('harvestInstagram',
         ['$http',
         'NETNAME',
         'localStorageManager',
         '$window',
         '$location',
         function(
           $http,
           NETNAME,
           localStorageManager,
           $window,
           $location ) {

        var _instagramClientId = "6c9e91c23efc4a378e9ba6f19030eec4";


        ///////
        // reporting number of collected media items

        var _mediaItemCounts = {};
        function clearMediaItemCount() {
          _mediaItemCounts = {};
        }
        function incrementMediaItemCount( handle ) {
          if( _mediaItemCounts[handle.user] ) {
            _mediaItemCounts[handle.user]++;
          } else{
            _mediaItemCounts[handle.user] = 1;
          }
        }
        function getMediaItemCount( handle ) {
          if( _mediaItemCounts[handle.user] === undefined ) {
            var savedItems = localStorageManager.getSavedMediaItems( handle );
            _mediaItemCounts[handle.user] = Object.keys(savedItems).length;
          }
          return _mediaItemCounts[handle.user];
        }


        ///////
        // authorization

        function directToAuthorizationUrl() {
          var hostUrl = $location.protocol() + "://" + $location.host();
          var authUrl = "https://instagram.com/oauth/authorize/?client_id="
            + _instagramClientId
            + "&redirect_uri="
            + hostUrl + "/instagram" // + (userName ? "/"+userName : "")
            + "&response_type=token";

          $window.location.href = authUrl;
        }

        var _accessToken;
        function populateAccessToken() {
          if( ! _accessToken ) {
            _accessToken = localStorageManager.getApiAccessToken( NETNAME.INSTAGRAM );
            if( ! _accessToken ) {
              directToAuthorizationUrl();
            }
          }
        }

        function doesUserExist( userName, callback ) {

          populateAccessToken();
          if( _accessToken ) {

            $http.jsonp("https://api.instagram.com/v1/users/search?q="+ userName
              + "&access_token=" + _accessToken + "&callback=JSON_CALLBACK")
            .success(function(data, status, headers, config) {
              // let's see if we find an exact match in the search reults
              var userId = null;
              for( var i=0; i < data.data.length; i++ ) {
                if( data.data[i].username === userName ) {
                  userId = data.data[i].id;
                  break;
                }
              }
              callback( userId );

            }).error(function(data, status, headers, config) {
              // we assume that the access token has expired:
              directToAuthorizationUrl();
            });
          }
        }


        ///////
        // harvest media items

        function getMediaItemsForInstagramUser( handle ) {

          populateAccessToken();
          if( _accessToken ) {

            var mediaItems = localStorageManager.getSavedMediaItems( handle );

            getOriginalPosts( handle, mediaItems );

            getLikes( handle, mediaItems );
          }
        }

        function getOriginalPostsUrl( handle, nextMaxId ) {
          return "https://api.instagram.com/v1/users/"
            + handle.userId
            + "/media/recent/?access_token="
            + _accessToken
            + "&callback=JSON_CALLBACK"
            + (nextMaxId ? "&max_id="+nextMaxId : "");
        }

        function getLikesUrl( handle, nextMaxId ) {
          return "https://api.instagram.com/v1/users/self/media/liked?access_token="
            + _accessToken
            + "&callback=JSON_CALLBACK"
            + (nextMaxId ? "&max_id="+nextMaxId : "");
        }

        function getMediaUrl( post ) {
          return post.images.low_resolution.url;
        }


        function getOriginalPosts( handle, mediaItems ) {

          var recentPostsStartUrl = getOriginalPostsUrl( handle );

          callAPIAndAddToMediaItems(
            recentPostsStartUrl, handle, mediaItems, getOriginalPostsUrl );
        }

        function getLikes( handle, mediaItems ) {

          var likesUrl = getLikesUrl( handle );

          callAPIAndAddToMediaItems(
            likesUrl, handle, mediaItems, getLikesUrl );
        }


        function callAPIAndAddToMediaItems( url, handle, mediaItems, nextUrlMethod ) {

          $http.jsonp( url ).success( function( data ) {

            data.data.forEach( function( post ) {
              // console.log( post );

              // TODO: now there's no re-encounter count, like for Tumblr, and maybe just better
              //  to refresh everything while we are at it?
              var oneMediaItem = {
                "type": post.type,
                "mediaUrl": getMediaUrl( post ),
                "tags": post.tags,
                "isPlayer": new Boolean(post.type == "video"),
                "timestamp": post.created_time,
                "creator": (post.user.full_name ? post.user.full_name : post.user.username ),
                "caption": (post.caption ? post.caption.text : ""),
                "provider": NETNAME.INSTAGRAM,
                "sourceUrl": post.link,

                // instagram special:
                "images": post.images
              };
              if( post.videos ) oneMediaItem["videos"] = post.videos;

              mediaItems[post.link] = oneMediaItem;
              incrementMediaItemCount( handle );
            });

            if( data.pagination.next_max_id ) {

              setTimeout( function() {

                var postsNextUrl = nextUrlMethod(
                  handle, data.pagination.next_max_id );

                callAPIAndAddToMediaItems(
                  postsNextUrl, handle, mediaItems, nextUrlMethod );

              }, 1000 );
            } else {

              console.log( "done paging through posts" );
              localStorageManager.saveMediaItems( mediaItems, handle );

            }
          }).error( function(data) {
            // we assume that the access token has expired:
            directToAuthorizationUrl();
          });
        }


        return {
          populateAccessToken: populateAccessToken,
          doesUserExist: doesUserExist,
          getMediaItemCount: getMediaItemCount,
          clearMediaItemCount: clearMediaItemCount,
          getMediaItemsForInstagramUser: getMediaItemsForInstagramUser
        };

     }])


     .factory('harvestFacebook',
         ['localStorageManager',
         '$location',
         'NETNAME',
         function(
           localStorageManager,
           $location,
           NETNAME ) {


         ///////
         // reporting number of collected media items

         var _mediaItemCounts = {};
         function clearMediaItemCount() {
           _mediaItemCounts = {};
         }
         function incrementMediaItemCount( handle ) {
           if( _mediaItemCounts[handle.user] ) {
             _mediaItemCounts[handle.user]++;
           } else{
             _mediaItemCounts[handle.user] = 1;
           }
         }
         function getMediaItemCount( handle ) {
           if( _mediaItemCounts[handle.user] === undefined ) {
             var savedItems = localStorageManager.getSavedMediaItems( handle );
             _mediaItemCounts[handle.user] = Object.keys(savedItems).length;
           }
           return _mediaItemCounts[handle.user];
         }


         ///////
         // authorization
         // and
         // harvest media items

         function getMediaItemsForFacebookUser( handle ) {

           if( FB ) {
             FB.getLoginStatus(function(response) {
               if (response.status === 'connected') {
                 console.log('Logged in.');

                 var mediaItems = localStorageManager.getSavedMediaItems( handle );

                 getTaggedInPhotos( handle, mediaItems );
                 getUploadedPhotos( handle, mediaItems );

                 getTaggedInVideos( handle, mediaItems );
                 getUploadedVideos( handle, mediaItems );

               } else {
                 $location.path( "/facebook" );
               }
             });
           } else {
             $location.path( "/facebook" );
           }

         }

         function getTaggedInPhotos( handle, mediaItems ) {
           apiCall( "/me/photos", handle, mediaItems, addPhotoToMediaItems );
         }
         function getUploadedPhotos( handle, mediaItems ) {
           apiCall( "/me/photos?type=uploaded", handle, mediaItems, addPhotoToMediaItems );
         }

         function getTaggedInVideos( handle, mediaItems ) {
           apiCall( "/me/videos", handle, mediaItems, addVideoToMediaItems );
         }
         function getUploadedVideos( handle, mediaItems ) {
           apiCall( "/me/videos?type=uploaded", handle, mediaItems, addVideoToMediaItems );
         }


         function apiCall( next, handle, mediaItems, addToMediaItems ) {
           FB.api( next, function(response) {
             console.log( "response" );
             console.log( response );
             if( response.data ) {

               response.data.forEach( function(oneItem) {

                 addToMediaItems( oneItem, handle, mediaItems );
               });
             }

             if( response.paging && response.paging.next ) {
               setTimeout(function() {
                 apiCall( response.paging.next, handle,
                   mediaItems, addToMediaItems );
               }, 500);
             } else {
               console.log( "done paging through posts" );
               localStorageManager.saveMediaItems( mediaItems, handle );
             }
           });
         }

         function addPhotoToMediaItems( onePhotoItem, handle, mediaItems ) {
          //  console.log( "onePhotoItem" );
          //  console.log( onePhotoItem );

           var oneMediaItem = {
             "type": "photo",
             "mediaUrl": onePhotoItem.source, // is rather small
             "tags": [],
             "isPlayer": new Boolean(false),
             "timestamp": onePhotoItem.created_time, // which isn't a timestamp but a formatted date
             "creator": onePhotoItem.from.name,
             "caption": onePhotoItem.name || onePhotoItem.description || "",
             "provider": NETNAME.FACEBOOK,
             "sourceUrl": onePhotoItem.link,

             // facebook video special
             "height": onePhotoItem.height,
             "width": onePhotoItem.width,
             "images": onePhotoItem.images
           };

           mediaItems[onePhotoItem.id] = oneMediaItem;

           incrementMediaItemCount( handle );
         }
         function addVideoToMediaItems( oneVideoItem, handle, mediaItems ) {
          //  console.log( "oneVideoItem" );
          //  console.log( oneVideoItem );

           var oneMediaItem = {
             "type": "video",
             "mediaUrl": oneVideoItem.picture, // is rather small
             "tags": [],
             "isPlayer": new Boolean(true),
             "timestamp": oneVideoItem.created_time, // which isn't a timestamp but a formatted date
             "creator": oneVideoItem.from.name,
             "caption": oneVideoItem.name || oneVideoItem.description || "",
             "provider": NETNAME.FACEBOOK,
             "sourceUrl": oneVideoItem.id, // we can construct a link from this id

             // facebook video special
             "source": oneVideoItem.source,
             "embed_html": oneVideoItem.embed_html,
             "format": oneVideoItem.format
            //  ,
            //  "icon": oneVideoItem.icon
           };

           mediaItems[oneVideoItem.id] = oneMediaItem;

           incrementMediaItemCount( handle );
         }


         return {
           getMediaItemCount: getMediaItemCount,
           clearMediaItemCount: clearMediaItemCount,
           getMediaItemsForFacebookUser: getMediaItemsForFacebookUser
         };

      }]);

 })();
