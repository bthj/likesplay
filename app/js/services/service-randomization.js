(function() {
   'use strict';

   /* Randomization Services */

   angular.module('likesplayApp.services.randomization', [])

    .factory('randomness', function(){

      return {

        // from: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
        // Returns a random integer between min (included) and max (excluded)
        // Using Math.round() will give you a non-uniform distribution!
        getRandomInt: function (min, max) {
          return Math.floor(Math.random() * (max - min)) + min;
        },

        // Returns a random number between min (inclusive) and max (exclusive)
        getRandomArbitrary: function (min, max) {
          return Math.random() * (max - min) + min;
        },

        halfChance: function () {
          return ( Math.random() < 0.5 ? 0 : 1 );
        },

        getNegativeOrPositiveMultiplier: function () {
          if( this.halfChance() ) {
            return 1;
          } else {
            return -1;
          }
        }

      };

    });

})();
