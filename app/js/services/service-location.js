(function() {
   'use strict';

   /* Location Services */

   angular.module('likesplayApp.services.location', [])

    .factory('geocoding', ['fbutil', function(fbutil) {

      var _geocoder;

      function initialize() {
        _geocoder = new google.maps.Geocoder();
      }

      function getCurrentPositionAndGeocodeIt( resultsCallback, denialCallback ) {

        if( "geolocation" in navigator ) {
          navigator.geolocation.getCurrentPosition( function(position) {

            geocodeReverseCurrentPosition(
              position.coords.latitude, position.coords.longitude, resultsCallback );
          }, function( err ) {
            // from http://diveintohtml5.info/geolocation.html
            // PERMISSION_DENIED (1) if the user clicks that “Don’t Share” button or otherwise denies you access to their location.
            // POSITION_UNAVAILABLE (2) if the network is down or the positioning satellites can’t be contacted.
            // TIMEOUT (3) if the network is up but it takes too long to calculate the user’s position. How long is “too long”? I’ll show you how to define that in the next section.
            if( err.code == 1 ) {
              denialCallback();
            }
          });
        } else {
          resultsCallback( null );
        }
      }

      function geocodeReverseCurrentPosition( lat, lng, resultsCallback ) {

        if( !_geocoder ) initialize();
        var geocodingResults = [];

        var latLng = new google.maps.LatLng(lat, lng);
        _geocoder.geocode({'latLng': latLng}, function(results, status) {
          if( status == google.maps.GeocoderStatus.OK ) {
            results.forEach( function(oneResult) {
              geocodingResults.push({
                formatted_address: oneResult.formatted_address,
                latLng: [oneResult.geometry.location.lat(), oneResult.geometry.location.lng()]
              });
            });
            resultsCallback( geocodingResults );

          } else if( status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT ) {
            // TODO:  look up in our own DB
          } else {
            resultsCallback( null );
          }
        });
      }

      return {

        getAddressesCloseToUserLocation: function( resultsCallback, denialCallback ) {

          getCurrentPositionAndGeocodeIt( resultsCallback, denialCallback );
        }
      };

    }]);

 })();
