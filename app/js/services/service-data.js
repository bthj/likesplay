(function() {
   'use strict';

   /* Data Management Services */

   angular.module('likesplayApp.services.data', ['firebase.auth','firebase.utils'])

    .factory('dataService', [
      'localStorageManager', 'authService', 'randomness', 'fbutil', 'cubesList', 'tagsList',
      function(localStorageManager, authService, randomness, fbutil, cubesList, tagsList ) {

      var _userId;

      var _cubeGeoFire;
      function getCubeGeoFire() {
        if( !_cubeGeoFire ) {
          _cubeGeoFire = new GeoFire( fbutil.ref("geo").child("cubes") );
        }
        return _cubeGeoFire;
      }

      // TODO: rely solely on remote (fire)database ?
      var _localCubes = localStorageManager.getSavedCubes();


      function addCubeReferenceToUser( cubeId ) {

        fbutil.ref("users").child(authService.getUserId()).child("cubes").child(cubeId).set(true);
      }


      function isTagValidFirebaseKey( tag ) {
        var invalidKeyCharactersRE = /\.|#|\$|\/|\[|\]/;
        var invalidCharMatch = invalidKeyCharactersRE.exec( tag );
  			return ! invalidCharMatch;
      }

      function updateUserTagsCounts( tags ) {
        if( tags ) {
          Object.keys(tags).forEach( function( oneTag ) {
            // transaction not really needed here,
            // but one way of creating or incrementing a counter, from https://www.firebase.com/docs/web/guide/saving-data.html#section-transactions
            fbutil.ref("users").child(authService.getUserId()).child("tags")
                .child(oneTag).transaction(function(currentCount) {

              return (currentCount || 0) + 1;
            });
          });
        }
      }

      function addCubeToTags( cubeId, tags ) {
        if( tags ) {
          Object.keys(tags).forEach( function( oneTag ) {
            // tagsList.one( oneTag ).$add(  );
            fbutil.ref('tags').child( oneTag ).child( cubeId ).set(
              Firebase.ServerValue.TIMESTAMP );
          });
        }
      }

      function addCubeGeoMapping( cubeId, cube ) {
        var location = cube.insideInformation.location;
        if( location ) {
          console.log( "addCubeGeoMapping: cubeId: "+cubeId+", latLng: " );
          console.log( location.latLng );
          getCubeGeoFire().set( cubeId, location.latLng).then(function() {

            console.log( "added cube id " + cubeId + " to GeoFire" );
          }, function( error ) {
            console.log("Error while adding cube: "+cubeId+" to GeoFire: "+error);
          });
        }
      }


      function getMediaItemsCleandOfAngularProperties( mediaItems ) {
        var cleanedMediaItems = [];
        mediaItems.forEach( function(oneMediaItem, index) {
          // let's get all non-angular keys
          var nonAngularKeys = Object.keys(oneMediaItem).filter(function(value) {
            return value !== '$$hashKey';
          });
          var oneCleanedMediaItem = {};
          nonAngularKeys.forEach( function(oneKey) {
            oneCleanedMediaItem[oneKey] = mediaItems[index][oneKey];
          });
          cleanedMediaItems.push( oneCleanedMediaItem );
        });
        return cleanedMediaItems;
      }


      return {

        getTotallyRandomCubes: function( howMany, callback ) {

          var someRandomCubes = {};

          // TODO: might want to keep loading of all cubes at least centralized?
          fbutil.ref("cubes").once("value", function(snapshot) {

            var cubeKeys = Object.keys(snapshot.val());
            do {
              var oneCubeKeysIndex = randomness.getRandomInt( 0, cubeKeys.length );
              var oneCubeKey = cubeKeys[oneCubeKeysIndex];
              if( ! someRandomCubes[oneCubeKey] ) {

                someRandomCubes[oneCubeKey] = snapshot.val()[oneCubeKey];
              }
            } while( Object.keys(someRandomCubes).length < howMany );

            callback( someRandomCubes );
          });
        },

        getLatestCubes: function( howMany, callback ) {

          // TODO: do we want .orderByChild("timestamp") here?
          fbutil.ref("cubes").limitToLast( howMany ).on("value", function(snapshot) {

            callback( snapshot.val() );
          });
        },

        getUserCubes: function( userId, callback ) {

          fbutil.ref("users").child( userId ).child("cubes").once("value", function(snapshot) {

            callback( snapshot.val() );
          });
        },

        getCubeByKey: function( cubeKey, callback ) {

          fbutil.ref("cubes").child(cubeKey).once("value", function(snapshot) {

            callback( cubeKey, snapshot.val() );
          });
        },

        // returns those cubes that were saved
        persistOnlineCubesFromLocalStorage: function( callbackWhenAllCubesPersisted ) {
          var savedCubes = {};
          var localCubes = localStorageManager.getCubesNotSavedOnline();
          if( Object.keys(localCubes).length ) {

            localCubes.forEach( function( oneLocalCube, index ) {

              console.log("adding oneLocalCube");
              console.log(oneLocalCube);

              // var cubeRef = fbutil.ref("cubes").push();
              // console.log("cubeRef");
              // console.log(cubeRef.key());
              var cubeObject = {
                author: authService.getUserId(),
                by: authService.getUserFullName(),
                mediaItems: getMediaItemsCleandOfAngularProperties( oneLocalCube.mediaItems ),
                tags: oneLocalCube.tags || null,
                // timestamp: Firebase.ServerValue.TIMESTAMP,
                timestamp: Date.now ? Date.now() : new Date().getTime(), // polyfill-ish, see: https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Date/now#Polyfill
                insideInformation: (oneLocalCube.insideInformation || {})
                // TODO: location,

              };

              // cubeRef.set( cubeObject, function( err ) {
              //   if( err ) {
              //
              //   }
              //   console.log( "set object: " + cubeRef.key() );
              // });

              // savedCubes.push( oneLocalCube );

              cubesList.all().$add( cubeObject ).then( function(ref) {
                var cubeId = ref.key();

                console.log("added cube, id: " + cubeId);

                addCubeReferenceToUser( cubeId );

                updateUserTagsCounts( cubeObject.tags );

                addCubeToTags( cubeId, cubeObject.tags );

                addCubeGeoMapping( cubeId, cubeObject );

                savedCubes[cubeId] = cubeObject;

                if( index == localCubes.length-1 ) {
                  // time to call back
                  callbackWhenAllCubesPersisted( savedCubes );
                }
              });

              oneLocalCube.savedOnline = true;
            });
          } else {
            // there were no local unsaved cubes, but we must phone home anyways!
            callbackWhenAllCubesPersisted( {} );
          }

          // TODO: should we make sure the local cubes were successfully saved online,
          //  before clearing localStorage?
          localStorageManager.clearSavedCubes();
        },

        getTagsObjectFromMediaItems: function( mediaItems ) {
          var tagsObj = {};
          mediaItems.forEach( function( oneItem ) {
            if( oneItem.tags && oneItem.tags.length ) {
              oneItem.tags.forEach( function( oneTag ) {
                if( isTagValidFirebaseKey(oneTag) ) {
                  if( tagsObj[oneTag] ) {
                    tagsObj[oneTag]++;
                  } else {
                    tagsObj[oneTag] = 1;
                  }
                }
              });
            }
          });
          return tagsObj;
        }

      };

    }])

    .factory('cubesList', [
        '$firebaseArray', 'fbutil',
        function($firebaseArray, fbutil) {
      return {
        all: function() {
          var ref = fbutil.ref("cubes");
          return $firebaseArray( ref );
        },
        last: function( lastCount ) {
          var ref = fbutil.ref("cubes").limitToLast( lastCount );
          return $firebaseArray( ref );
        }
      };
    }])

    .factory('tagsList', [
        '$firebaseArray', 'fbutil',
        function($firebaseArray, fbutil) {

      return {
        all: function() {
          var ref = fbutil.ref('tags');
          return $firebaseArray( ref );
        },
        one: function( tag ) {
          var ref = fbutil.ref('tags').child( tag );
          return $firebaseArray( ref );
        }
      };
    }])


    .factory('profileData', [
        'fbutil', 'authService', 'Auth', '$firebaseArray',
        function(fbutil, authService, Auth, $firebaseArray) {

      // profile data queues, added to while not logged in
      // - values waiting to be added when user has logged in
      var _profileSaveQueue = {
        "gender": [],
        "seeking": [],
        "relationship": [],
        "orientation": []
      }

      var _userProfileRef; // set in Auth.$onAuth

      function getUserProfileArrayFromBucket( userId, bucketKey ) {
        var ref = fbutil.ref("users").child(userId).child("profile").child(bucketKey);
        var fbArray = $firebaseArray( ref );

        return fbArray;
      }

      function getArrayUserProfileFromBucketKey( bucketKey ) {
        if( authService.isLoggedIn() ) {
          var ref = _userProfileRef.child( bucketKey );
          return $firebaseArray( ref );
        } else {
          return [];
        }
      }

      function getArrayGlobalFromBucketKey( bucketKey ) {

        return $firebaseArray( fbutil.ref("profile/" + bucketKey ) );
      }

      // function updateProfileBuckets( bucketValuesToSet, bucketKey ) {
      //
      //
      //   userBucketRef.once("value", function(snapshot) {
      //
      //     // find saved values that are not being set and so have to be removed
      //     var currentValuesToBeRemoved = [];
      //     if( snapshot.val() ) {
      //       snapshot.val().forEach( function(oneSavedValue) {
      //         if( bucketValuesToSet.indexOf(oneSavedValue) < 0 ) {
      //           currentValuesToBeRemoved.push( oneSavedValue );
      //         }
      //       });
      //     }
      //
      //     // set the new values in the user's bucket
      //     userBucketRef.set( bucketValuesToSet );
      //
      //     // set the new values in the global bucket list
      //     bucketValuesToSet.forEach( function( oneValueToSet ) {
      //       globalBucketRef.child( oneValueToSet ).set( true );
      //       globalBucketUsersRef.child( oneValueToSet ).child( authService.getUserId() )
      //         .set( Firebase.ServerValue.TIMESTAMP );
      //     });
      //     // and remove from the global list existing values that are not being set now
      //     currentValuesToBeRemoved.forEach( function(oneValueToRemove) {
      //       globalBucketUsersRef.child( oneValueToRemove ).child(authService.getUserId()).remove();
      //     });
      //   });
      // }


      function addValueToBuckets( valueToAdd, bucketKey, removeValueFromSaveQueue ) {
        // getArrayUserProfileFromBucketKey( bucketKey ).$add( valueToAdd ).then(function(ref) {
        //   if( removeValueFromSaveQueue ) removeValueFromSaveQueue( valueToAdd, bucketKey );
        // });
        getArrayUserProfileFromBucketKey( bucketKey ).$loaded()
            .then(function( bucketArray ) {

          if( -1 === bucketArray.$indexFor(valueToAdd) ) {

            _userProfileRef.child( bucketKey ).child( valueToAdd ).set( true, function() {
              if( removeValueFromSaveQueue ) removeValueFromSaveQueue( valueToAdd, bucketKey );
            });

            fbutil.ref("profile/" + bucketKey).child( valueToAdd ).transaction( function(currentCount) {
              return (currentCount || 0) + 1;
            });

            fbutil.ref("profile/" + bucketKey + "-users").child( valueToAdd )
              .child( authService.getUserId() ).set( Firebase.ServerValue.TIMESTAMP );
          }
        });
      }

      function removeValueFromBuckets( valueToRemove, bucketKey ) {
        // getArrayUserProfileFromBucketKey( bucketKey ).$remove( valueToRemove );
        _userProfileRef.child( bucketKey ).child( valueToRemove ).remove();

        fbutil.ref("profile/" + bucketKey).child( valueToRemove ).transaction( function(currentCount) {
          return Math.max( (currentCount || 0) - 1, 0);
        });

        fbutil.ref("profile/" + bucketKey + "-users").child( valueToRemove )
          .child( authService.getUserId() ).remove();
      }


      function addValuesFromSaveQueuesToBuckets() {
        Object.keys(_profileSaveQueue).forEach( function(bucketKey) {
          _profileSaveQueue[bucketKey].forEach( function(oneValue) {
            addValueToBuckets( oneValue, bucketKey, removeValueFromSaveQueue );
          });
        });
      }

      function removeValueFromSaveQueue( valueToRemove, bucketKey ) {
        var valueIndex = _profileSaveQueue[bucketKey].indexOf(valueToRemove);
        if( valueIndex >= 0 ) {
          _profileSaveQueue[bucketKey].splice(valueIndex, 1);
        }
      }

      Auth.$onAuth( function(authData) {
        if( authData ) {
          _userProfileRef = fbutil.ref("users").child(authService.getUserId()).child("profile");
          // when we're logged, check if there are any profile values to add to online db
          addValuesFromSaveQueuesToBuckets();
        }
      });


      function addValueToBucketssOrSaveQueue( valueToAdd, bucketKey ) {
        var addedToQueue;
        if( authService.isLoggedIn() ) {
          addValueToBuckets( valueToAdd, bucketKey );
          addedToQueue = false;
        } else {
          // add this value to a save queue
          console.log( "add value to queue: valueToAdd: " + valueToAdd + ", bucketKey: " + bucketKey );
          _profileSaveQueue[bucketKey].push( valueToAdd );
          addedToQueue = true;
        }
        return addedToQueue;
      }

      function removeValueFromBucketssOrSaveQueue( valueToRemove, bucketKey ) {
        var removedFromQueue;
        if( authService.isLoggedIn() ) {
          removeValueFromBuckets( valueToRemove, bucketKey );
          removedFromQueue = false;
        } else {
          console.log( "remove value from queue: valueToRemove: " + valueToRemove + ", bucketKey: " + bucketKey );
          removeValueFromSaveQueue( valueToRemove, bucketKey );
          removedFromQueue = true;
        }
        return removedFromQueue;
      }

      return {
        getArrayWithGender: function() {
          return getArrayUserProfileFromBucketKey( "gender" );
        },
        getArrayWithSeeking: function() {
          return getArrayUserProfileFromBucketKey( "seeking" );
        },
        getArrayWithRelationship: function() {
          return getArrayUserProfileFromBucketKey( "relationship" );
        },
        getArrayWithOrientation: function() {
          return getArrayUserProfileFromBucketKey( "orientation" );
        },

        getArrayWithGenderForUser: function( userId ) {
          return getUserProfileArrayFromBucket( userId, "gender" );
        },
        getArrayWithSeekingForUser: function( userId ) {
          return getUserProfileArrayFromBucket( userId, "seeking" );
        },
        getArrayWithRelationshipForUser: function( userId ) {
          return getUserProfileArrayFromBucket( userId, "relationship" );
        },
        getArrayWithOrientationForUser: function( userId ) {
          return getUserProfileArrayFromBucket( userId, "orientation" );
        },


        // to add gender, seeking, relationship or orientation
        addValueToBucketssOrSaveQueue: addValueToBucketssOrSaveQueue,

        // to remove gender, seeking, relationship or orientation
        removeValueFromBucketssOrSaveQueue: removeValueFromBucketssOrSaveQueue,


        getOptionArrayGender: function() {
          return getArrayGlobalFromBucketKey( "gender" );
        },
        getOptionArrayLookingFor: function() {
          return getArrayGlobalFromBucketKey( "seeking" );
        },
        getOptionArrayRelationship: function() {
          return getArrayGlobalFromBucketKey( "relationship" );
        },
        getOptionArrayOrientation: function() {
          return getArrayGlobalFromBucketKey( "orientation" );
        },

        setGender: function( genderArray ) {
          updateProfileBuckets( genderArray, "gender" );
        },
        setLookingFor: function( lookingForArray ) {
          updateProfileBuckets( lookingForArray, "seeking" );
        },
        setRelationship: function( relationshipArray ) {
          updateProfileBuckets( relationshipArray, "relationship" );
        },
        setOrientation: function( orientationArray ) {
          updateProfileBuckets( orientationArray, "orientation" );
        }
      }
    }])

    .factory('messaging', [
        'fbutil', 'authService', 'Auth', '$firebaseArray',
        function(fbutil, authService, Auth, $firebaseArray) {


      /// general public messaging functions

      function messagesPublic( cubeId, bucketKey ) {
        var ref = fbutil.ref("messaging").child( bucketKey ).child(cubeId);
        return $firebaseArray( ref );
      }

      function addPublicMessageToUsersSentHistory(
          userId, sentMessagesBucketKey, cubeId ) {
        fbutil.ref("users").child(userId).child(sentMessagesBucketKey)
          .child(cubeId).set(Firebase.ServerValue.TIMESTAMP);
      }
      function removePublicMessageFromUsersSentHistory(
         userId, sentMessagesBucketKey, cubeId ) {
        fbutil.ref("users").child(userId).child(sentMessagesBucketKey)
          .child(cubeId).remove();
      }

      function addPublicMessageToOneUserFeed(
          userId, messagesFeedBucketKey, cubeId, messageId ) {

        fbutil.ref("users").child( userId ).child( messagesFeedBucketKey )
          .child(cubeId).set(messageId); // will be set to true when the message has been read
      }
      function addPublicMesssageIdToUsersFeed(
          messagesBucketKey, messagesFeedBucketKey,
          cubeId, cubeCreatorId, messageId,
          notifyAllCubeParticipants ) {
        var feedUserIds = [];
        if( cubeCreatorId !== authService.getUserId() ) {
          feedUserIds.push( cubeCreatorId );
        }
        if( notifyAllCubeParticipants ) {
          // get all users that have posted on this cube,
          // that are not the current user nor the cube creator
          messagesPublic( cubeId, messagesBucketKey ).$loaded().then( function(messagesArray) {
            messagesArray.forEach( function(oneMessage) {
              if( oneMessage.from !== cubeCreatorId
                  && oneMessage.from !== authService.getUserId() ) {

                feedUserIds.push( oneMessage.from );
              }
            });

            // now we can add notifications to the users feeds
            feedUserIds.forEach( function( oneUserId ) {

              addPublicMessageToOneUserFeed(
                  oneUserId, messagesFeedBucketKey, cubeId, messageId );
            });
          });
        } else {
          // we should only have cube creator ID here,
          // or nothing if it was tha cube creator that added the message
          feedUserIds.forEach( function( oneUserId ) {

            addPublicMessageToOneUserFeed(
                oneUserId, messagesFeedBucketKey, cubeId, messageId );
          });
        }
      }


      /// likes private functions

      function hasUserLiked( cubeId, callback ) {
        if( authService.isLoggedIn() ) {

          fbutil.ref("messaging").child("likes").child(cubeId)
            .orderByChild("from").equalTo(authService.getUserId()).on("value",
            function(snapshot) {
              if( snapshot && snapshot.val() ) {

                var likeKeys = Object.keys( snapshot.val() );
                if( likeKeys.length ) {
                  callback( likeKeys[0] );  // won't be called if user hasn't liked.
                }
              } else {
                callback( false );
              }
            }
          );
        }
      }


      return {

        /// public messages

        getPublicMessagesForCube: function( cubeId, callback ) {

          messagesPublic( cubeId, "messagesPublic" ).$loaded().then(
            function(messagesArray) {

              callback( messagesArray );
            }
          );
        },

        addPublicMessageToCube: function( cubeId, cubeCreatorId, message ) {
          if( authService.isLoggedIn() ) {

            messagesPublic( cubeId, "messagesPublic" ).$add({
              from: authService.getUserId(),
              timestamp: Firebase.ServerValue.TIMESTAMP,
              content: message
            }).then( function(ref) {
              var messageId = ref.key();

              addPublicMesssageIdToUsersFeed(
                "messagesPublic", "messagesPublicFeed",
                cubeId, cubeCreatorId, messageId );

              addPublicMessageToUsersSentHistory(
                authService.getUserId(), "messagesPublicToCubes", cubeId );
            });
          }
        },


        /// likes

        getLikesForCube: function( cubeId, callback ) {

          messagesPublic( cubeId, "likes" ).$loaded().then(
            function(likesArray) {

              callback( likesArray );
            }
          );
        },

        hasUserLiked: hasUserLiked,

        likeCube: function( cubeId, cubeCreatorId, callback ) {
          if( authService.isLoggedIn() ) {

            messagesPublic( cubeId, "likes" ).$add({
              from: authService.getUserId(),
              timestamp: Firebase.ServerValue.TIMESTAMP
            }).then( function(ref) {

              var likeId = ref.key();

              addPublicMesssageIdToUsersFeed(
                "likes", "likesFeed",
                cubeId, cubeCreatorId, likeId );

              addPublicMessageToUsersSentHistory(
                authService.getUserId(), "likesToCubes", cubeId );

              hasUserLiked( cubeId, callback );
            });
          }
        },

        unlikeCube: function( cubeId, likeId, callback ) {
          if( authService.isLoggedIn() ) {

            fbutil.ref("messaging").child("likes").child(cubeId).child(likeId)
              .remove( function(error) {
                if( !error ) {

                  removePublicMessageFromUsersSentHistory(
                    authService.getUserId(), "likesToCubes", cubeId )

                  hasUserLiked( cubeId, callback );
                }
            });

            // var rec = messagesPublic( cubeId, "likes" ).$getRecord( likeId );
            // messagesPublic( cubeId, "likes" ).$remove( rec ).then(function(ref) {
            //
            //
            // });
          }
        },


        /// private messages

        getPrivateCorrespondents: function() {

        },

        getMessagesWithCorrespondent: function( userId ) {

        }
      };

    }]);

})();
