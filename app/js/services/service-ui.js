(function() {
   'use strict';

   /* UI Services */

   angular.module('likesplayApp.services.ui', [])

    .factory('cubification', [function() {

      var _firstItemBottomAligned = false;

      return {

        setFirstItemBottomAligned: function( isBottomAligned ) {
          _firstItemBottomAligned = isBottomAligned;
        },
        isFirstItemBottomAligned: function() {
          return _firstItemBottomAligned;
        },

        getBoxSideTransform: function(
            itemIndex, center, edgeHalfLength, currentYRotation) {

          // + and - to remove flicker along adjoining surface edges
          var posAdjust = 0;

          if( undefined === currentYRotation ) currentYRotation = 0;

          // adapted from http://jsbin.com/lovuv/1/edit?js , http://stackoverflow.com/a/26593028/169858

          switch( itemIndex ) {
            case 0:  // Front
              return {
                translate: [
                  center.x,
                  // check if first media surface is aligned for door-opening:
                  center.y + (_firstItemBottomAligned ? $scope.mediaSurfaceSize[0] / 2 : 0),
                  center.z + edgeHalfLength+posAdjust
                ],
                rotate: [0, currentYRotation, 0]
              };
            case 1:  // Back
              return {
                translate: [center.x, center.y, center.z - edgeHalfLength-posAdjust],
                rotate: [Math.PI, currentYRotation, Math.PI]
              };
            case 2:  // Top
              return {
                translate: [center.x, center.y - edgeHalfLength-posAdjust, center.z],
                rotate: [Math.PI/2, currentYRotation, 0]
              };
            case 3:  // Bottom
              return {
                translate: [center.x, center.y + edgeHalfLength+posAdjust, center.z],
                rotate: [-Math.PI/2, currentYRotation, Math.PI]
              };
            case 4:  // Left
              return {
                translate: [center.x - edgeHalfLength-posAdjust, center.y, center.z],
                rotate: [0, currentYRotation-Math.PI/2, 0]
              };
            case 5:  // Right
              return {
                translate: [center.x + edgeHalfLength+posAdjust, center.y, center.z],
                rotate: [0, currentYRotation+Math.PI/2, 0]
              };
            default:
              break;
          }
        },

        getMediaSurfaceSize: function( headerFooterHeight ) {
          if( undefined === headerFooterHeight ) headerFooterHeight = 0;

          if( window.innerWidth < window.innerHeight ) {
            var edgeLength = window.innerWidth / 2;
            if( edgeLength * 3 > window.innerHeight - headerFooterHeight ) {
              edgeLength = (window.innerHeight - headerFooterHeight) / 3;
            }
          } else {
            var edgeLength = (window.innerHeight - headerFooterHeight) / 2;
            if( edgeLength * 3 > window.innerWidth ) {
              edgeLength = window.innerWidth / 3;
            }
          }
          console.log("getMediaSurfaceSize");
          console.log([edgeLength, edgeLength]);
          return [edgeLength, edgeLength];
        },

        getBoxCenterPoint: function( headerFooterHeight ) {
          if( undefined === headerFooterHeight ) headerFooterHeight = 0;

          return {
            x: window.innerWidth / 2,
            y: (window.innerHeight / 2) - headerFooterHeight / 2,
            z: 0
          };
        },


        getInitializedImageSurfaces: function( numberOfItems, setDefaultContent ) {
          var imageSurfaces = [];
          for( var i=0; i < numberOfItems; i++ ) {

            var oneImageSurface = new BkImageSurface({
              sizeMode: BkImageSurface.SizeMode.ASPECTFILL
            });
            if( setDefaultContent ) {
              oneImageSurface.setContent( 'img/heart_background_square.png' );
            }
            imageSurfaces.push( oneImageSurface );
          }
          return imageSurfaces;
        }

      };
    }])


    .factory('cubeRotation', ['$famous', function($famous) {

      var Easing = $famous['famous/transitions/Easing'];

      var _INPUT_ROTATION_SENSITIVITY = 120;
      var _lastMax5RotationVelocity = [];
      var _lastMax5Delta = [];

      return {

        updateRotation: function( rotator, data ) {

          var currentRotation = rotator.get();
          rotator.set([
            currentRotation[0] - data.delta[1] / _INPUT_ROTATION_SENSITIVITY,
            currentRotation[1] + data.delta[0] / _INPUT_ROTATION_SENSITIVITY,
            currentRotation[2]
          ]);

          _lastMax5Delta.push( data.delta );
          if( _lastMax5Delta.length > 5 ) _lastMax5Delta.shift();
          _lastMax5RotationVelocity.push( data.velocity );
          if( _lastMax5RotationVelocity.length > 5 ) _lastMax5RotationVelocity.shift();
        },

        animateRotatorWithFauxDrag: function( rotator ) {

          if( _lastMax5Delta.length ) {
            var currentRotation = rotator.get();
            var xAvgDelta = 0, yAvgDelta = 0;

            _lastMax5Delta.forEach( function( oneDelta ){
              xAvgDelta += oneDelta[0];
              yAvgDelta += oneDelta[1];
            });
            xAvgDelta = xAvgDelta / _lastMax5Delta.length;
            yAvgDelta = yAvgDelta / _lastMax5Delta.length;

            var xAvgVelocity = 0, yAvgVelocity = 0;
            _lastMax5RotationVelocity.forEach( function( oneVelocitySample ) {
              xAvgVelocity += Math.abs(oneVelocitySample[0]);
              yAvgVelocity += Math.abs(oneVelocitySample[1]);
            });
            xAvgVelocity = xAvgVelocity / _lastMax5RotationVelocity.length;
            yAvgVelocity = yAvgVelocity / _lastMax5RotationVelocity.length;

            rotator.set([
              currentRotation[0] - yAvgDelta * yAvgVelocity / 10,
              currentRotation[1] + xAvgDelta * xAvgVelocity / 10,
              currentRotation[2]
            ], {
              duration: 500 * (xAvgVelocity + yAvgVelocity),
              curve: Easing.outQuad
            }, function() {
              // clear movement history after faux drag animation completes:
              _lastMax5RotationVelocity = [];
              _lastMax5Delta = [];
            });
          }

        },

        hasBeenMoving: function() {
          return _lastMax5Delta.length;
        }

      };

    }])


    .factory('cubeInside', ['$famous', 'randomness', function($famous, randomness) {

      var Timer = $famous['famous/utilities/Timer'];
      var Easing = $famous['famous/transitions/Easing'];

      // spring and snap transition methods registred in app.js


      var DOOR_OPENING_DURATION = 987;
      var CUBE_ENTRY_DURATION = 3000;

      var CUBE_INSIDE_ROTATION_SENSITIVITY = 200;
      var VELOCITY_THRESHOLD = .2;

      var _cubeInsideStartRotation;


      ////////////////////////////////////
      // travel inside cube - private methods

      function swingHatchOpen( surfaceRotator, postOpenCallback ) {
        var surfaceCurrentRotation = surfaceRotator.get();

        surfaceRotator.set(
          [
            surfaceCurrentRotation[0] - Math.PI - Math.PI/8,
            surfaceCurrentRotation[1],
            surfaceCurrentRotation[2]
          ], {
            duration: DOOR_OPENING_DURATION,
            curve: 'easeInOut'
          }, function() {

            surfaceCurrentRotation = surfaceRotator.get();
            surfaceRotator.set(
              [
                surfaceCurrentRotation[0] + Math.PI/8,
                surfaceCurrentRotation[1],
                surfaceCurrentRotation[2]
              ], {
                method: 'spring',
                period: 1000,
                dampingRatio: 0.1
              }, function() {

                postOpenCallback();
              }
            );
          }
        );

      }

      function alignCubeForEntry(
          cubeRotator, cubeTranslator,
          cubeEntryDuringCallback, cubeEntryDoneCallback ) {

        var currentItemsContainerRotation = cubeRotator.get();

        var numberOfCirclesRotatedX = Math.round(
          currentItemsContainerRotation[0] / (Math.PI*2) );
        var numberOfCirclesRotatedY = Math.round(
          currentItemsContainerRotation[1] / (Math.PI*2) );

        var wholeCircleExcessX =
          currentItemsContainerRotation[0] - numberOfCirclesRotatedX * Math.PI * 2;
        var wholeCircleExcessY =
          currentItemsContainerRotation[1] - numberOfCirclesRotatedY * Math.PI * 2;
        if( wholeCircleExcessX > Math.PI ) {
          var rotationDeltaX = wholeCircleExcessX + (Math.PI/2 - wholeCircleExcessX);
        } else {
          var rotationDeltaX = -wholeCircleExcessX;
        }
        if( wholeCircleExcessY > Math.PI ) {
          var rotationDeltaY = wholeCircleExcessY + (Math.PI/2 - wholeCircleExcessY);
        } else {
          var rotationDeltaY = -wholeCircleExcessY;
        }

        cubeRotator.set( [
          currentItemsContainerRotation[0] + rotationDeltaX,
          currentItemsContainerRotation[1],
          currentItemsContainerRotation[2]
        ], {duration: 1000}, function() {

          currentItemsContainerRotation = cubeRotator.get();
          Timer.setTimeout(function() {

            cubeRotator.set( [
              currentItemsContainerRotation[0],
              currentItemsContainerRotation[1] + rotationDeltaY,
              currentItemsContainerRotation[2]
            ], {duration: 1000}, function() {

              Timer.setTimeout(function() {

                enterCube( cubeTranslator, cubeEntryDuringCallback, cubeEntryDoneCallback );
              }, 333);
            });

          }, 333);
        });
      }

      function enterCube(
        cubeTranslator, cubeEntryDuringCallback, cubeEntryDoneCallback ) {

        // translate cube around camera
        cubeTranslator.set(
          [0, 0, 620], {duration: CUBE_ENTRY_DURATION, curve: Easing.outBack}, function() {

            cubeEntryDoneCallback();
          });

          Timer.setTimeout( function() {

            cubeEntryDuringCallback();

          }, CUBE_ENTRY_DURATION * .66 );
      }



      ////////////////////////////////////
      // exit cube - private methods

      function translateAndRotateCubeIntoMiddleGround(
          cubeRotator, cubeTranslator, hatchRotator,
          cubeExitDuringCallback, cubeExitDoneCallback, postCloseCallback,
          snapToX, snapToY, currentZ,
          clockwiseX, clockwiseY, deltaXLarger, negOrPosX, negOrPosY ) {

        var targetX, targetY;

        if( deltaXLarger ) {
          // we'll rotate ~half circle around the X axis
          if( clockwiseX ) {
            targetX = snapToX - Math.PI * randomness.getRandomArbitrary(1.1, 1.4);
          } else {
            targetX = snapToX + Math.PI * randomness.getRandomArbitrary(1.1, 1.4);
          }
          targetY = snapToY + randomness.getRandomArbitrary(-Math.PI / 6, Math.PI / 6);
        } else {
          if( clockwiseY ) {
            targetY = snapToY - Math.PI * randomness.getRandomArbitrary(1.1, 1.4);
          } else {
            targetY = snapToY + Math.PI * randomness.getRandomArbitrary(1.1, 1.4);
          }
          targetX = snapToX + randomness.getRandomArbitrary(-Math.PI / 6, Math.PI / 6);
        }

        var exitRotationDuration = CUBE_ENTRY_DURATION; // / 2.7;
        var exitTranslationDuration = CUBE_ENTRY_DURATION;

        // rotate the cube so the opening faces the viewer, when seen from the outside
        cubeRotator.set(
          [targetX, targetY, currentZ],
          {duration: exitRotationDuration, curve: Easing.outCubic}
        );

        // translate the cube into "middle ground",
        // after some delay so the outsides don't come abrubtly into view
        Timer.setTimeout( function() {

          // $scope.$apply(function () {
          //   $scope.mediaSurfacesShown = true;
          //   $scope.hatchInsideShown = true;
          // });
          cubeExitDuringCallback();
          cubeTranslator.set(
            [0, 0, 0],
            {duration: exitTranslationDuration, curve: Easing.outBack}, function() {

              // $scope.cubeTouchHandler.pipe( inputMovementSync );
              //
              // isInsideCube = false;
              cubeExitDoneCallback();

            });

        }, exitRotationDuration * .38);


        slamDoorClosed( hatchRotator, exitTranslationDuration, postCloseCallback );
      }

      function slamDoorClosed( hatchRotator, exitTranslationDuration, postCloseCallback ) {

        // and slam the door closed!
        Timer.setTimeout( function() {

          // $scope.$apply(function () {
          //   $scope.showKeyhole = true;
          //   transitionableFadeIn( $scope.keyholeOpacity );
          //   startWatchingColorFlowValues();
          // });

          var firstItemCurrentRotation = hatchRotator.get();

          hatchRotator.set([
            firstItemCurrentRotation[0] + Math.PI,
            firstItemCurrentRotation[1],
            firstItemCurrentRotation[2]
          ], {
            duration: DOOR_OPENING_DURATION,
            curve: Easing.outBounce
          }, function() {

            // checkIfCubeIsReadyForSpace();
            postCloseCallback();
          });

        }, exitTranslationDuration * .8 );
      }



      return {

        ////////////////////////////////////
        // travel inside cube - public methods

        getHatchSurfaceOriginForOpening: function() {
          // let's set the first surface alignment along the bottom edge
          return [.5, 1];
        },

        translateHatchSurfaceForOpening: function( surfaceTranslator, surfaceEdgeLength ) {

          // and translate it down the Y axis half the surface edge length
          var surfaceCurrentTranslation = surfaceTranslator.get();
          surfaceTranslator.set([
            surfaceCurrentTranslation[0],
            surfaceCurrentTranslation[1] + surfaceEdgeLength / 2,
            surfaceCurrentTranslation[2]
          ]);
        },

        openAndEnterCube: function(
            hatchRotator, postOpenCallback,
            cubeRotator, cubeTranslator,
            cubeEntryDuringCallback, cubeEntryDoneCallback ) {

          swingHatchOpen( hatchRotator, postOpenCallback );

          Timer.setTimeout(function() {

            alignCubeForEntry(
                cubeRotator, cubeTranslator,
                cubeEntryDuringCallback, cubeEntryDoneCallback );

          }, DOOR_OPENING_DURATION * .7 );
        },


        ////////////////////////////////////
        // cube inside drag event handling - public methods

        startCubeInsideRotation: function( cubeRotator ) {

          _cubeInsideStartRotation = cubeRotator.get();
        },

        updateCubeInsideRotation: function( cubeRotator, eventData ) {

          var currentRotation = cubeRotator.get();
          cubeRotator.set([
            currentRotation[0] + eventData.delta[1] / CUBE_INSIDE_ROTATION_SENSITIVITY,
            currentRotation[1] - eventData.delta[0] / CUBE_INSIDE_ROTATION_SENSITIVITY,
            currentRotation[2]
          ]);
        },

        snapCubeRotationToQuartCircles: function(
          eventData,
          cubeRotator, cubeTranslator, hatchRotator,
          cubeExitDuringCallback, cubeExitDoneCallback, postCloseCallback ) {

          // let's snap the rotation to quart circles

          var currentItemsContainerRotation = cubeRotator.get();

          var negOrPosX = currentItemsContainerRotation[0] / Math.abs(currentItemsContainerRotation[0]) || 1;
          var negOrPosY = currentItemsContainerRotation[1] / Math.abs(currentItemsContainerRotation[1]) || 1;

          // remainder gives us how much into a new circle we have rotated:
          var amountRotatedIntoNewQuartCircleX =
            currentItemsContainerRotation[0] % (Math.PI / 2);
          var amountRotatedIntoNewQuartCircleY =
            currentItemsContainerRotation[1] % (Math.PI / 2);

          var snapToLesserX, snapToLesserY;
          // X rotation - we depennd on Y velocity
          if( Math.abs(eventData.velocity[1]) > VELOCITY_THRESHOLD ) {
            // we've surpassed the velocity threshold,
            // find the next quarter in the direction we're going
            // - check if we're decreasing or increasing the rotation value
            if( Math.abs(currentItemsContainerRotation[0]) < Math.abs(_cubeInsideStartRotation[0]) ) {
              snapToLesserX = true;
            } else {
              snapToLesserX = false;
            }
          } else {
            // let's find on which side of one-eigth of a circle we are
            if( Math.abs(amountRotatedIntoNewQuartCircleX) < Math.PI / 4 ) {
              snapToLesserX = true;
            } else {
              snapToLesserX = false;
            }
          }
          // Y rotation - we depennd on X velocity
          if( Math.abs(eventData.velocity[0]) > VELOCITY_THRESHOLD ) {
            if( Math.abs(currentItemsContainerRotation[1]) < Math.abs(_cubeInsideStartRotation[1]) ) {
              snapToLesserY = true;
            } else {
              snapToLesserY = false;
            }
          } else {
            if( Math.abs(amountRotatedIntoNewQuartCircleY) < Math.PI / 4 ) {
              snapToLesserY = true;
            } else {
              snapToLesserY = false;
            }
          }

          if( snapToLesserX ) {
            var snapToX = currentItemsContainerRotation[0] - amountRotatedIntoNewQuartCircleX;
          } else {
            var snapToX =
              currentItemsContainerRotation[0]
              + Math.PI*negOrPosX/2 - amountRotatedIntoNewQuartCircleX;
          }
          if( snapToLesserY ) {
            var snapToY = currentItemsContainerRotation[1] - amountRotatedIntoNewQuartCircleY;
          } else {
            var snapToY =
              currentItemsContainerRotation[1]
              + Math.PI*negOrPosY/2 - amountRotatedIntoNewQuartCircleY;
          }

          // console.log( "cosX: " + Math.cos(snapToX) + ", sinX: " + Math.sin(snapToX) );
          // console.log( "cosY: " + Math.cos(snapToY) + ", sinY: " + Math.sin(snapToY) );
          var rotatingTowardsDoor =
            Math.round(Math.cos(snapToX)) == 1 && Math.round(Math.cos(snapToY)) == -1
              || Math.round(Math.cos(snapToX)) == -1 && Math.round(Math.cos(snapToY)) == 1;

          if( rotatingTowardsDoor ) {
            // pigs in space!
            var deltaXLarger =
              Math.abs(currentItemsContainerRotation[0] - _cubeInsideStartRotation[0])
              > Math.abs(currentItemsContainerRotation[1] - _cubeInsideStartRotation[1]);

            var clockwiseX = currentItemsContainerRotation[0] < _cubeInsideStartRotation[0];
            var clockwiseY = currentItemsContainerRotation[1] < _cubeInsideStartRotation[1];

            translateAndRotateCubeIntoMiddleGround(
              cubeRotator, cubeTranslator, hatchRotator,
              cubeExitDuringCallback, cubeExitDoneCallback, postCloseCallback,
              snapToX, snapToY, currentItemsContainerRotation[2],
              clockwiseX, clockwiseY, deltaXLarger, negOrPosX, negOrPosY );

          } else {
            // rotate to the closest 90 degree angle
            cubeRotator.set(
              [snapToX, snapToY, currentItemsContainerRotation[2]],
              {
                method: 'snap',
                period: 400,
                dampingRatio: .25
              });
          }

        },

        calculateGripSizes: function( gripSize, gripIconSize, surfaceEdgeLength ) {

          gripSize.x = surfaceEdgeLength * .4;
          gripSize.y = surfaceEdgeLength / 6;

          gripIconSize.x = surfaceEdgeLength * .2;
          gripIconSize.y = surfaceEdgeLength / 6;
        },

        calculateGripTranslations: function(
            gripSize, gripTranslations, gripIconTranslations,
            numberOfVisibleItems, surfaceEdgeLength ) {

          for( var i=0; i < numberOfVisibleItems; i++ ) {
            if( gripTranslations[i] ) {
              gripTranslations[i] = {};
              gripIconTranslations[i] = {};
            } else {
              gripTranslations.push( {} );
              gripIconTranslations.push( {} );
            }

            var gripY;
            if( i == 2 ) { // top media surface
              gripY = -surfaceEdgeLength/2+gripSize.y/2;
            } else {
              gripY = surfaceEdgeLength/2-gripSize.y/2;
            }
            gripTranslations[i].leftX = -surfaceEdgeLength/2+gripSize.x/2;
            gripTranslations[i].leftY = gripY;
            gripTranslations[i].rightX = surfaceEdgeLength/2-gripSize.x/2;
            gripTranslations[i].rightY = gripY;
            gripTranslations[i].z = 4;

            gripIconTranslations[i].x = 0;
            gripIconTranslations[i].y = gripY;
            gripIconTranslations[i].z = 2;
          }
        },



        tagFlexLayoutOptions : {
          ratios: [8, 2]
        }

      };

    }])


    .factory('colorFlow', ['$famous', function($famous) {

      var Timer = $famous['famous/utilities/Timer'];
      var Transitionable = $famous['famous/transitions/Transitionable'];

      var _colorUpdateCallback;

      var _isWatchingColorFlow = false;

      var _flowRGB = new Transitionable( [0, 0, 0] );
      // $scope.flowRGB = [0, 0, 0];

      function getColorFlowTransitionValues() {
        var newRGB = _flowRGB.get();
        var newRGBIntegers = [
          Math.floor(newRGB[0]),
          Math.floor(newRGB[1]),
          Math.floor(newRGB[2])
        ];

        _colorUpdateCallback( newRGBIntegers );
      }


      function transitionToNewRandomColor() {
        // inspired by http://www.algosome.com/articles/drawing-javascript-graphics-svg.html
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);

        _flowRGB.set(
          [r, g, b],
          { duration: 1000 },
          function() {

            if( _isWatchingColorFlow ) transitionToNewRandomColor();
          }
        );
      }

      return {

        // we just support one callback at a time, which should be enough
        // ...we just have one active/visible controller at a time...
        registerColorUpdateCallback: function( callbackToRegister ) {

          _colorUpdateCallback = callbackToRegister;
        },

        unregisterColorUpdateCallback: function( callbackToUnregister ) {

          _colorUpdateCallback = null;
        },

        startWatchingColorFlowValues: function() {

          if( ! _isWatchingColorFlow ) {

            transitionToNewRandomColor();

            Timer.every( getColorFlowTransitionValues, 2 );

            _isWatchingColorFlow = true;
          }
        },

        stopWatchingColorFlowValues: function() {

          if( _isWatchingColorFlow ) {

            Timer.clear( getColorFlowTransitionValues );

            _isWatchingColorFlow = false;
          }
        }

      };
    }])

    .factory('fading', ['$famous', function($famous) {
      var Easing = $famous['famous/transitions/Easing'];

      function transitionableFadeIn( transitionableToFade ) {

        transitionableToFade.set(
          1,
          { duration: 2000, curve: Easing.inOutSine},
          function() {
            transitionablePartialFadeOut( transitionableToFade );
          }
        );
      }
      function transitionablePartialFadeOut( transitionableToFade ) {

        transitionableToFade.set( .7,
          {duration: 2000, curve: Easing.inOutSine},
          function() {
            transitionableFadeIn( transitionableToFade );
          }
        );
      }
      function stopTransitionableFading( fadingTransitionableToStop ) {

        fadingTransitionableToStop.halt();
      }

      return {

        transitionableFadeIn: transitionableFadeIn,
        stopTransitionableFading: stopTransitionableFading
      };
    }])


    .factory('embed', ['$http', 'NETNAME', function($http, NETNAME){

      function hasEmbedData( objectToCheck ) {
        var hasEmbed = false;
        if( "iframely" in objectToCheck
        // && objectToCheck.sourceUrl == objectToCheck.iframely.url
        ) {

          hasEmbed = true;
        }
        return hasEmbed;
      }

      function addEmbedDataToObject( objToReceiveEmbedData, data ) {

        objToReceiveEmbedData.iframely = data;
      }



      /*
       * getItemMaximized* functions will return such a structure:
       *  {
       *    width: ,
       *    height: ,
       *    image: ,  - optional
       *    embed: , - optional
       *    isPlayer: ,
       *    title: ,
       *    link:
       *  }
      */

      function availableAspectRatio( availableSpace ) {
        return availableSpace[0] / availableSpace[1];
      }
      function setMaximizedSizeToMetaObjectFromItemRatio(
          maximizedMeta, itemRatio, availableSpace ) {

        if( itemRatio < 1 ) {
          // tall item
          maximizedMeta.height = availableSpace[1];
          maximizedMeta.width = maximizedMeta.height * itemRatio;
          // check if width is within bounds
          if( maximizedMeta.width > availableSpace[0] ) {
            maximizedMeta.width = availableSpace[0];
            if( availableAspectRatio(availableSpace) < 1 ) {
              // tall screen
              maximizedMeta.height = maximizedMeta.width * itemRatio;
            } else {
              // wide screen
              maximizedMeta.height = maximizedMeta.width / itemRatio;
            }
          }
        } else {
          // wide item
          maximizedMeta.width = availableSpace[0];
          maximizedMeta.height = maximizedMeta.width / itemRatio
          // check if height is within bounds
          if( maximizedMeta.height > availableSpace[1] ) {
            maximizedMeta.height = availableSpace[1];
            if( availableAspectRatio(availableSpace) < 1 ) {
              // tall screen
              maximizedMeta.width = maximizedMeta.height / itemRatio;
            } else {
              // wide screen
              maximizedMeta.width = maximizedMeta.height * itemRatio;
            }
          }
        }
      }


      function getItemMaximizedSquare( availableSpace ) {
        var maximizedMeta = {};
        setMaximizedSizeToMetaObjectFromItemRatio(
            maximizedMeta, 1, availableSpace );

        maximizedMeta.link = mediaItem.sourceUrl;
        maximizedMeta.title = mediaItem.creator
          + (mediaItem.caption ? " - "+mediaItem.caption : "");

        return maximizedMeta;
      }


      /// tumblr

      function getTumblrMaximizedImageUrl( mediaItem, availableSpace ) {
        var imageUrl;
        if( mediaItem.photos && mediaItem.photos[0].alt_sizes.length ) {
          for( var i=0; i < mediaItem.photos[0].alt_sizes.length; i++ ) {
            if( mediaItem.photos[0].alt_sizes[i].width <= availableSpace[0]
                && mediaItem.photos[0].alt_sizes[i].height <= availableSpace[1] ) {

              imageUrl = mediaItem.photos[0].alt_sizes[i].url;
              break;
            }
          }
        } else {
          imageUrl = mediaItem.mediaUrl;
        }
        return imageUrl;
      }
      function getTumblrItemMaximized( mediaItem, availableSpace ) {
        var maximizedMeta = {};
        var itemRatio;
        if( mediaItem.thumbnail_width && mediaItem.thumbnail_height ) {
          itemRatio = mediaItem.thumbnail_width / mediaItem.thumbnail_height;
        } else if( mediaItem.photos && mediaItem.photos[0].alt_sizes.length ) {
          itemRatio = mediaItem.photos[0].alt_sizes[0].width / mediaItem.photos[0].alt_sizes[0].height;
        } else {
          itemRatio = 1;
        }

        setMaximizedSizeToMetaObjectFromItemRatio(
            maximizedMeta, itemRatio, availableSpace );

        maximizedMeta.image =
          getTumblrMaximizedImageUrl( mediaItem, availableSpace );

        maximizedMeta.title = mediaItem.creator;
        if( mediaItem.caption ) {
          var $captionParsed = $( mediaItem.caption );
          if( $captionParsed.length ) {
            // we had some html, let's get it as plain text
            maximizedMeta.title += " - " + $captionParsed.text();
          } else {
            maximizedMeta.title += " - " + mediaItem.caption;
          }
          if( maximizedMeta.title.length > 60 ) {
            maximizedMeta.title = maximizedMeta.title.substring( 0, 60 ) + "...";
          }
        }

        maximizedMeta.link = mediaItem.sourceUrl;
        maximizedMeta.isPlayer = mediaItem.isPlayer;

        return maximizedMeta;
      }


      /// instagram

      function getInstagramMaximizedImageUrlForPhoto( mediaItem, availableSpace ) {
        var imageUrl;
        if( mediaItem.images ) {
          if( mediaItem.images.standard_resolution.width <= availableSpace[0]
              && mediaItem.images.standard_resolution.height <= availableSpace[1] ) {

            imageUrl = mediaItem.images.standard_resolution.url;

          } else if( mediaItem.images.low_resolution.width <= availableSpace[0]
              && mediaItem.images.low_resolution.height <= availableSpace[1] ) {

            imageUrl = mediaItem.images.low_resolution.url;

          } else {
            imageUrl = mediaItem.images.thumbnail.url;
          }
        } else {
          imageUrl = mediaItem.mediaUrl;
        }
        return imageUrl;
      }
      function getInstagramItemMaximized( mediaItem, availableSpace, callback ) {
        var maximizedMeta = {};
        var itemRatio;
        if( mediaItem.images && mediaItem.images.standard_resolution ) {
          // in case instagram will be anything but square
          itemRatio = mediaItem.images.standard_resolution.width /
            mediaItem.images.standard_resolution.height;
        } else {
          itemRatio = 1;
        }

        setMaximizedSizeToMetaObjectFromItemRatio(
            maximizedMeta, itemRatio, availableSpace );

        maximizedMeta.image =
          getInstagramMaximizedImageUrlForPhoto( mediaItem, availableSpace );

        maximizedMeta.title = mediaItem.creator
          + (mediaItem.caption ? " - "+mediaItem.caption : "" );
        maximizedMeta.link = mediaItem.sourceUrl;
        maximizedMeta.isPlayer = mediaItem.isPlayer;

        // now we have an opportunity to make the instagram embed api call
        // https://instagram.com/developer/embedding/
        // but not doing that for now and call immediately back:
        callback( maximizedMeta );
      }


      /// facebook

      function getFacebookMaximizedImageUrlForPhoto( mediaItem, availableSpace ) {
        var imageUrl;
        if( mediaItem.images && mediaItem.images.length ) {
          for( var i=0; i < mediaItem.images.length; i++ ) {
            if( mediaItem.images[i].width <= availableSpace[0]
                && mediaItem.images[i].height <= availableSpace[1] ) {

              imageUrl = mediaItem.images[i].source;
              break;
            }
          }
          if( ! imageUrl ) {
            // let's then use lowest resolution one of those available
            imageUrl = mediaItem.images[mediaItem.images.length-1].source;
          }
        } else {
          imageUrl = mediaItem.mediaUrl;
        }
        return imageUrl;
      }
      function setMaximizedPhotoAndEmbedToMetaObjectForVideo(
          maximizedMeta, mediaItem, availableSpace ) {

        var imageUrl, embed;

        if( mediaItem.format && mediaItem.format.length ) {
          for( var i=mediaItem.format.length-1; i >=0 ; i-- ) {
            if( mediaItem.format[i].width <= availableSpace[0]
                && mediaItem.format[i].height <= availableSpace[1] ) {

              embed = mediaItem.format[i].embed_html;
              imageUrl = mediaItem.format[i].picture;
              break;
            }
          }
          if( !imageUrl || !embed ) {
            embed= mediaItem.format[mediaItem.format.length-1].embed_html;
            imageUrl = mediaItem.format[mediaItem.format.length-1].picture;
          }
        } else {
          imageUrl = mediaItem.mediaUrl;
          embed = mediaItem.embed_html;
        }
        maximizedMeta.embed = embed;
        maximizedMeta.image = imageUrl;
      }
      function getFacebookItemTitle( mediaItem ) {
        return mediaItem.creator + " - " + mediaItem.provider;
      }

      function getFacebookVideoMaximized( mediaItem, availableSpace ) {
        var maximizedMeta = {};
        var itemRatio;
        if( mediaItem.format && mediaItem.format.length ) {
          itemRatio = mediaItem.format[0].width / mediaItem.format[0].height;
        } else {
          itemRatio = 1;
        }

        setMaximizedSizeToMetaObjectFromItemRatio(
          maximizedMeta, itemRatio, availableSpace );

        setMaximizedPhotoAndEmbedToMetaObjectForVideo(
          maximizedMeta, mediaItem, availableSpace );

        maximizedMeta.title = getFacebookItemTitle( mediaItem );
        maximizedMeta.link = "https://www.facebook.com/" + mediaItem.sourceUrl;
        maximizedMeta.isPlayer = mediaItem.isPlayer;
        // maximizedMeta.icon = mediaItem.icon;

        return maximizedMeta;
      }
      function getFacebookPhotoMaximized( mediaItem, availableSpace ) {
        var maximizedMeta = {};
        var itemRatio;
        if( mediaItem.width && mediaItem.height ) {
          itemRatio = mediaItem.width / mediaItem.height;
        } else {
          itemRatio = 1;
        }

        setMaximizedSizeToMetaObjectFromItemRatio(
          maximizedMeta, itemRatio, availableSpace );

        maximizedMeta.image =
          getFacebookMaximizedImageUrlForPhoto( mediaItem, availableSpace );

        maximizedMeta.title = getFacebookItemTitle( mediaItem );
        maximizedMeta.link = mediaItem.sourceUrl;
        maximizedMeta.isPlayer = mediaItem.isPlayer;

        return maximizedMeta;
      }
      function getFacebookItemMaximized( mediaItem, availableSpace ) {
        switch( mediaItem.type ) {
          case "photo":
            return getFacebookPhotoMaximized( mediaItem, availableSpace );
          case "video":
            return getFacebookVideoMaximized( mediaItem, availableSpace );
          default:
            return getItemMaximizedSquare( availableSpace );
        }
      }

      return {


        getMaximizedItemMeta: function( mediaItem, availableSpace, callback ) {
          switch( mediaItem.provider ) {
            case NETNAME.TUMBLR:
              callback( getTumblrItemMaximized( mediaItem, availableSpace ) );
              break;
            case NETNAME.INSTAGRAM:
              getInstagramItemMaximized( mediaItem, availableSpace, callback );
              break;
            case NETNAME.FACEBOOK:
              callback( getFacebookItemMaximized( mediaItem, availableSpace ) );
              break;
            default:
              callback( getItemMaximizedSquare( availableSpace ) );
          }
        },


        ////////////////////////////
        // iframely

        addEmbedDataToMediaItem: function( mediaItem, callback ) {

          // if( hasEmbedData( mediaItem ) ) {
          //   // we already have an object with embed data, so let's just call back
          //   callback();
          // } else {
          //
          //   // let's first try the iframely API:
          //   // $http.get('http://iframe.ly/api/iframely?url='
          //   // + mediaItem.sourceUrl
          //   // +'&api_key=4dad98eeb233482b1564af').success(
          //   //   function(data, status, headers, config) {
          //   //
          //   //     addEmbedDataToObject( mediaItem, data );
          //   //     callback();
          //   //   }
          //   // ).error(function(data, status, headers, config) {
          //
          //     // let's try the Open oEmbed API http://oembedapi.com/
          //     $http.get(
          //     '//open.iframe.ly/api/oembed?url='
          //     + mediaItem.sourceUrl
          //     +'&origin=gameoflikes').success(
          //       function(data, status, headers, config) {
          //
          //         addEmbedDataToObject( mediaItem, data );
          //         callback();
          //       }
          //     ).error(function(data, status, headers, config) {
          //       // out of luck, but let's call back
          //       callback();
          //     });
          //
          //   // });
          // }

        },

        // getThumbnailURL: function( mediaItem ) {
        //   var thumbURL;
        //   // get thumbnail URL form embed data, or return mediaUrl
        //   if( hasEmbedData( mediaItem )
        //       && mediaItem.iframely.links && mediaItem.iframely.links.thumbnail
        //       && mediaItem.iframely.links.thumbnail.length ) {
        //     thumbURL = mediaItem.iframely.links.thumbnail[0].href;
        //   } else {
        //     thumbURL = mediaItem.mediaUrl;
        //   }
        //   return thumbURL;
        // },
        //
        // isPlayer: function( mediaItem ) {
        //   var playerPresent = false;
        //   // check if mebed data has a "player" key
        //   if( hasEmbedData( mediaItem ) ) {
        //     if( mediaItem.iframely.links && mediaItem.iframely.links.player ) {
        //       playerPresent = true;
        //     }
        //   }
        //   return playerPresent;
        // },
        //
        // getFullScreenEmbedMarkup: function( mediaItem ) {
        //   var embedMarkup = "";
        //   if( hasEmbedData( mediaItem ) ) {
        //     // if( mediaItem.iframely.links && mediaItem.iframely.links.app
        //     //     && mediaItem.iframely.links.app.length ) {
        //     //
        //     //   embedMarkup = mediaItem.iframely.links.app[0].html;
        //     //
        //     // } else
        //     if( mediaItem.iframely.links && mediaItem.iframely.links.player
        //         && mediaItem.iframely.links.player.length ) {
        //
        //       embedMarkup = mediaItem.iframely.links.player[0].html;
        //
        //     } else if( mediaItem.iframely.html ) {
        //       embedMarkup = mediaItem.iframely.html;
        //     }
        //   }
        //   return embedMarkup;
        // },
        //
        // getEmbedSurfaceSize: function( mediaItem, availableSpace, useApp ) {
        //   var surfaceSize = [];
        //   var width, maxWidth, embedAspectRatio, availableAspectRatio;
        //   if( hasEmbedData( mediaItem ) ) {
        //     if( useApp ) {
        //       if( mediaItem.iframely.links && mediaItem.iframely.links.app
        //           && mediaItem.iframely.links.app.length ) {
        //
        //         if( mediaItem.iframely.links.app[0].media ) {
        //           width = mediaItem.iframely.links.app[0].media.width;
        //           maxWidth = mediaItem.iframely.links.app[0].media["max-width"];
        //         }
        //       }
        //     }
        //     if( mediaItem.iframely.links && mediaItem.iframely.links.player
        //         && mediaItem.iframely.links.player.length ) {
        //       if( mediaItem.iframely.links.player.media
        //           && mediaItem.iframely.links.player.media["aspect-ratio"] ) {
        //
        //         embedAspectRatio = mediaItem.iframely.links.player.media["aspect-ratio"];
        //       }
        //     } else {
        //       // we'll have to find the media aspect ratio by other means
        //       if( mediaItem.iframely.links && mediaItem.iframely.links.thumbnail
        //           && mediaItem.iframely.links.thumbnail.length
        //           && mediaItem.iframely.links.thumbnail[0].media ) {
        //
        //         embedAspectRatio =
        //         mediaItem.iframely.links.thumbnail[0].media.width /
        //         mediaItem.iframely.links.thumbnail[0].media.height;
        //       } else if( mediaItem.iframely.thumbnail_width && mediaItem.iframely.thumbnail_height ) {
        //         embedAspectRatio = mediaItem.iframely.thumbnail_width / mediaItem.iframely.thumbnail_height;
        //       }
        //     }
        //
        //     if( width ) {
        //       surfaceSize[0] = width < availableSpace[0] ? width : availableSpace[0];
        //
        //     } else if( maxWidth ) {
        //
        //       surfaceSize[0] = maxWidth < availableSpace[0] ? maxWidth : availableSpace[0];
        //     }
        //
        //     if( surfaceSize[0] ) {
        //       if( useApp ) {
        //         // useFullHeight when dealing with an "app" embed
        //         surfaceSize[1] = availableSpace[1];
        //       } else if( embedAspectRatio ) {
        //         surfaceSize[1] = surfaceSize[0] / embedAspectRatio;
        //       } else {
        //         // should not happen, but let's then just use the available height
        //         surfaceSize[1] = availableSpace[0];
        //       }
        //     }
        //
        //     if( ! surfaceSize[0] ) {
        //       // let's then fit the surface into the available space, in the same aspect ratio
        //       if( embedAspectRatio ) {
        //         availableAspectRatio = availableSpace[0] / availableSpace[1];
        //         if( embedAspectRatio < 1 ) {
        //           // we have a tall media item
        //           surfaceSize[1] = availableSpace[1];
        //           surfaceSize[0] = surfaceSize[1] * embedAspectRatio;
        //         } else {
        //           // wide media item
        //           surfaceSize[0] = availableSpace[0];
        //           surfaceSize[1] = surfaceSize[0] / embedAspectRatio;
        //           // sanity check:
        //           if( surfaceSize[1] > availableSpace[1] ) {
        //             if( availableAspectRatio < 1 ) {
        //               // tall screen
        //               surfaceSize[1] = availableSpace[1];
        //               surfaceSize[0] = surfaceSize[1] / embedAspectRatio;
        //             } else {
        //               // wide screen
        //               surfaceSize[1] = availableSpace[1];
        //               surfaceSize[0] = surfaceSize[1] * embedAspectRatio;
        //             }
        //           }
        //         }
        //       } else {
        //         // well then let's just fill the media item into the available space, regardless of aspect
        //         surfaceSize[0] = availableSpace[0];
        //         surfaceSize[1] = availableSpace[1];
        //       }
        //     }
        //
        //   } else {
        //     // again, let's just fill the media item into the available space, regardless of aspect
        //     surfaceSize[0] = availableSpace[0];
        //     surfaceSize[1] = availableSpace[1];
        //   }
        //   surfaceSize[0] = surfaceSize[0]*.92;
        //   surfaceSize[1] = surfaceSize[1]*.92;
        //   return surfaceSize;
        // }

      };
    }]);

 })();
