(function() {
   'use strict';

   /* localStorage Services */

   angular.module('likesplayApp.services.data.local', [])

     .factory('localStorageManager', [function(){
       var userHandlesStorageKey = "likeBreeder.userHandles";
       var mediaItemsStorageKey = "likeBreeder.mediaItems";
       var cubesStorageKey = "likeBreeder.cubes";
       var profileStorageKey = "likesplay.profile";
       var apiAccessTokenStorageKeyPrefix = 'likesplay.apitoken_';

       try {
         var isLocalStorage = ('localStorage' in window && window['localStorage'] !== null);
       } catch (e) {
         var isLocalStorage = false;
       }


       ////// handles

       function saveNetworkUserHandles( userHandles ) {
         if( isLocalStorage ) {
           localStorage[ userHandlesStorageKey ] = JSON.stringify( userHandles,
             function( key, val ) {
             // based on http://mutablethought.com/2013/04/25/angular-js-ng-repeat-no-longer-allowing-duplicates/
             if( key == '$$hashKey' ) {
               return undefined;
             }
             return val;
           } );
         }
       }

       function getSavedNeworkUserHandles() {
         if( isLocalStorage && localStorage[userHandlesStorageKey] ) {
           return JSON.parse( localStorage[userHandlesStorageKey] );
         } else {
           return null;
         }
       }

       function clearSavedNetworkUserHandles() {
         localStorage.removeItem( userHandlesStorageKey );
       }


       ////// media items

       function saveMediaItems( mediaItems, handle ) {
         if( isLocalStorage ) {
           var mediaStorageKeyForUser = mediaItemsStorageKey + handle.network + handle.user;
           localStorage[ mediaStorageKeyForUser ] = JSON.stringify(
             mediaItems, function( key, val ) {
             // based on http://mutablethought.com/2013/04/25/angular-js-ng-repeat-no-longer-allowing-duplicates/
             if( key == '$$hashKey' ) {
               return undefined;
             }
             return val;
           } );
           _allMediaItems = null;
         }
       }

       function getSavedMediaItems( handle ) {
         var mediaStorageKeyForUser = mediaItemsStorageKey + handle.network + handle.user;
         if( isLocalStorage && localStorage[mediaStorageKeyForUser] ) {
           return JSON.parse( localStorage[mediaStorageKeyForUser] );
         } else {
           return {};
         }
       }

       var _allMediaItems = null;
       function getAllMediaItems() {
         if( null === _allMediaItems ) {
           _allMediaItems = getAllSavedMediaItems();
         }
         return _allMediaItems;
       }

       function getAllSavedMediaItems() {
         var allMediaItems = {};
         if( getSavedNeworkUserHandles() ) {
           getSavedNeworkUserHandles().forEach(function(oneHandle){
             var mediaItemsForOneHandle = getSavedMediaItems( oneHandle );
             if( mediaItemsForOneHandle ) {
               Object.keys(mediaItemsForOneHandle).forEach(function(oneMediaKey){
                 allMediaItems[oneMediaKey] = mediaItemsForOneHandle[oneMediaKey];
               });
             }
           });
         }
         return allMediaItems;
       }

       function clearSavedMediaItems( network, user ) {
         localStorage.removeItem( mediaItemsStorageKey + network + user );
       }

       function clearAllSavedMediaItems() {
         var networkHandles = getSavedNeworkUserHandles();
         if( networkHandles ) {
           getSavedNeworkUserHandles().forEach(function(oneHandle){
             clearSavedMediaItems( oneHandle.network, oneHandle.user );
           });
         }
       }

       function saveOneCube( cube ) {
         if( isLocalStorage ) {

           var cubes = getSavedCubes();
           if( !cubes ) cubes = [];
           cubes.push( cube );

           localStorage[ cubesStorageKey ] = JSON.stringify( cubes );
         }
       }

       function saveCubeState( allCubes ) {
         if( isLocalStorage ) {
           localStorage[ cubesStorageKey ] = JSON.stringify( allCubes );
         }
       }

       // temporary migration to cubes as an array of object, instead of an array of arrays
       function migrateCubesToObject( cubes ) {
         var migratedCubes = [];
         cubes.forEach( function(oneCube) {
           if( Array.isArray( oneCube ) ) {
             migratedCubes.push( {mediaItems: oneCube} );
           } else {
             migratedCubes.push( oneCube );
           }
         });
         return migratedCubes;
       }
       function getSavedCubes() {
         if( isLocalStorage && localStorage[cubesStorageKey] ) {
           var cubes = JSON.parse( localStorage[cubesStorageKey] );
           return migrateCubesToObject( cubes );
         } else {
           return null;
         }
       }

       function getCubesNotSavedOnline() {
         var cubesYetToSaveOnline = [];
         var savedCubes = getSavedCubes();
         if( savedCubes ) {
           getSavedCubes().forEach( function(oneLocalCube) {
             if( ! oneLocalCube.savedOnline && oneLocalCube.mediaItems ) {
               cubesYetToSaveOnline.push( oneLocalCube );
             }
           });
         }
         return cubesYetToSaveOnline;
       }

       function clearSavedCubes() {
         localStorage.removeItem( cubesStorageKey );
       }


       function saveProfile( profileObject ) {
         if( isLocalStorage ) {
           localStorage[profileStorageKey] = JSON.stringify( profileObject );
         }
       }
       function getProfile() {
         if( isLocalStorage && localStorage[profileStorageKey] ) {
           return JSON.parse( localStorage[profileStorageKey] );
         } else {
           return null;
         }
       }

       function saveApiAccessToken( network, token ) {
         if( isLocalStorage ) {
           var apiTokenStorageKey = apiAccessTokenStorageKeyPrefix + network;
           localStorage[apiTokenStorageKey] = token;
         }
       }
       function getApiAccessToken( network ) {
         var apiTokenStorageKey = apiAccessTokenStorageKeyPrefix + network;
         if( isLocalStorage && localStorage[apiTokenStorageKey] ) {
           return localStorage[apiTokenStorageKey];
         } else {
           return null;
         }
       }


       return {
         saveNetworkUserHandles: saveNetworkUserHandles,
         getSavedNeworkUserHandles: getSavedNeworkUserHandles,
         clearSavedNetworkUserHandles: clearSavedNetworkUserHandles,
         saveMediaItems: saveMediaItems,
         getAllMediaItems: getAllMediaItems,
         getSavedMediaItems: getSavedMediaItems,
         getAllSavedMediaItems: getAllSavedMediaItems,
         clearSavedMediaItems: clearSavedMediaItems,
         clearAllSavedMediaItems: clearAllSavedMediaItems,
         saveOneCube: saveOneCube,
         saveCubeState: saveCubeState,
         getSavedCubes: getSavedCubes,
         getCubesNotSavedOnline: getCubesNotSavedOnline,
         clearSavedCubes: clearSavedCubes,
         saveProfile: saveProfile,
         getProfile: getProfile,
         saveApiAccessToken: saveApiAccessToken,
         getApiAccessToken: getApiAccessToken
       };
     }]);

})();
