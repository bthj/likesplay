'use strict';

// Declare app level module which depends on filters, and services
angular.module('likesplayApp.config', [])

  // from http://blog.gospodarets.com/track_javascript_angularjs_and_jquery_errors_with_google_analytics/
  .config(function ($provide) {
      $provide.decorator("$exceptionHandler", function ($delegate) {
          return function (exception, cause) {
              $delegate(exception, cause);
              console.log("SENDING ERROR");
              ga(
                  'send',
                  'event',
                  'AngularJS error',
                  exception.message,
                  exception.stack,
                  0,
                  true
              );
          };
      });
  })

  // version of this seed app is compatible with angularFire 1.0.0
  // see tags for other versions: https://github.com/firebase/angularFire-seed/tags
  .constant('version', '1.0.0')

  // where to redirect users if they need to authenticate (see security.js)
  .constant('loginRedirectPath', '/login')

  // your Firebase data URL goes here, no trailing slash
  .constant('FBURL', 'https://likesplay.firebaseio.com')

  // double check that the app has been configured before running it and blowing up space and time
  .run(['FBURL', '$timeout', function(FBURL, $timeout) {
    if( FBURL.match('//INSTANCE.firebaseio.com') ) {
      angular.element(document.body).html('<h1>Please configure app/config.js before running!</h1>');
      $timeout(function() {
        angular.element(document.body).removeClass('hide');
      }, 250);
    }


  }]);
