(function (angular) {
  "use strict";

  var app = angular.module('likesplayApp.breeding', [
    'ngRoute', 'famous.angular', 'firebase.utils', 'firebase']);

  app.controller('LikeBreeding',
      ['$scope',
      '$http',
      '$location',
      'networkUserHandles',
      'mediaItemHarvester',
      'localStorageManager',
      'dataService',
      'profileData',
      'geocoding',
      'similaritySearch',
      'cubification',
      'cubeRotation',
      'cubeInside',
      'colorFlow',
      'fading',
      'embed',
      '$sce',
      'randomness',
      'NETNAME',
      '$famous',
      '$analytics',
      function(
        $scope,
        $http,
        $location,
        networkUserHandles,
        mediaItemHarvester,
        localStorageManager,
        dataService,
        profileData,
        geocoding,
        similaritySearch,
        cubification,
        cubeRotation,
        cubeInside,
        colorFlow,
        fading,
        embed,
        $sce,
        randomness,
        NETNAME,
        $famous,
        $analytics ) {


    // let's begin with checking if there are any user handles on record
    // and if not, redirect to the social network registration:
    if( !networkUserHandles.getAllNetworkUserHandles().length ) {

      navigateToRoots();
    }


    $scope.totalVisibleItems = 6;


    ////////////////////////////////////
    // famo.us layout configuration

    var Timer = $famous['famous/utilities/Timer'];

    var Transitionable = $famous['famous/transitions/Transitionable'];

    var Transform  = $famous['famous/core/Transform'];
    var Quaternion = $famous['famous/math/Quaternion'];

    var Easing = $famous['famous/transitions/Easing'];

    var EventHandler = $famous['famous/core/EventHandler'];
    var GenericSync = $famous['famous/inputs/GenericSync'];
    var MouseSync = $famous["famous/inputs/MouseSync"];
    var TouchSync = $famous["famous/inputs/TouchSync"];
    var ScrollSync = $famous["famous/inputs/ScrollSync"];
    var RotateSync = $famous['famous/inputs/RotateSync'];
    var PinchSync = $famous['famous/inputs/PinchSync'];

    var TextareaSurface = $famous['famous/surfaces/TextareaSurface'];
    // var ImageSurface = $famous["famous/surfaces/ImageSurface"];


    // content options

    $scope.mediaItemsContainerOptions = {
      properties: {
        backgroundColor: '#000000'
      }
    };


    // header options
    $scope.headerOptions = {
      properties: {
        paddingTop: '8px',
        textAlign: 'center',
        fontSize: '34px',
        color: 'white'
      }
    };

    // footer options
    $scope.buttonGridLayoutOptions = {
      ratios: [1, true]
    };
    $scope.footerLeftOptions = {
      properties: {
        textAlign: 'center',
        fontSize:'2.2em',
        cursor: 'pointer'
      }
    };

    $scope.activeNetworks = {};
    function populateActiveNetworks() {
      networkUserHandles.getAllNetworkUserHandles().forEach(function(oneHandle){
        if( ! $scope.activeNetworks[oneHandle.network] ) {
          $scope.activeNetworks[oneHandle.network] = {};
          switch( oneHandle.network ) {
            case NETNAME.TUMBLR:
              $scope.activeNetworks[oneHandle.network].colour = "#32506d";
              $scope.activeNetworks[oneHandle.network].image = 'img/tumblr_white.svg';
              break;
            case NETNAME.INSTAGRAM:
              $scope.activeNetworks[oneHandle.network].colour = "#675144";
              $scope.activeNetworks[oneHandle.network].image = 'img/instagram_white.svg';
              break;
            case NETNAME.FACEBOOK:
              $scope.activeNetworks[oneHandle.network].colour = "#3b5998";
              $scope.activeNetworks[oneHandle.network].image = 'img/facebook_white.svg';
              break;
          }
        }
      });
    }
    populateActiveNetworks();


    // cube inside surfaces options
    $scope.tagFlexLayoutOptions = cubeInside.tagFlexLayoutOptions;


  /*
    // http://pbs.twimg.com/profile_images/568047917450031104/3DGl47jT.png
    $scope.imgTest = new BkImageSurface({
      // TODO: self-host the default image
      content: 'https://scontent-ams.xx.fbcdn.net/hphotos-xpf1/v/t1.0-9/q81/p180x540/10929556_10153244278521834_6765187175244298568_n.jpg?oh=f181c450f3bb11ed154fccd2465f3e0a&oe=55B188EB',
      sizeMode: BkImageSurface.SizeMode.ASPECTFILL
    });
    console.log( $scope.imgTest );
  */

    // media item surfaces

    // $scope.imageContainerSurfaces = [];
    // $scope.imageSurfaces = [];
    // function initializeImageSurfaces( numberOfItems ) {
    //
    //   for( var i=0; i < numberOfItems; i++ ) {
    //     /* // BkImageSurface ...
    //     $scope.imageSurfaces.push( new BkImageSurface({
    //       content: 'img/heart_background_square.png',
    //       sizeMode: BkImageSurface.SizeMode.ASPECTFILL
    //     }) );
    //     */
    //     // Ken Burns container:
    //     var oneKenBurnsContainer = new KenBurnsContainer();
    //     var oneImageSurface = new BkImageSurface({
    //       content: 'img/heart_background_square.png',
    //       sizeMode: BkImageSurface.SizeMode.ASPECTFILL
    //     });
    //     // oneImageSurface.on('click', function(event) {console.log(event);});
    //     oneKenBurnsContainer.add( oneImageSurface );
    //     $scope.imageContainerSurfaces.push( oneKenBurnsContainer );
    //     $scope.imageSurfaces.push( oneImageSurface );
    //   }
    // }
    // initializeImageSurfaces( $scope.totalVisibleItems );
    //
    // function setMediaSurfaceContent( itemIndex, imageUrl ) {
    //
    //   $scope.imageSurfaces[itemIndex].setContent( imageUrl );
    //
    //   //Ken Burns...
    //   $scope.imageContainerSurfaces[itemIndex].delay( randomness.getRandomInt(500, 3000) );
    //   $scope.imageContainerSurfaces[itemIndex].panAndZoom(
    //     [randomness.getRandomArbitrary(0.0, 1.0), randomness.getRandomArbitrary(0.0, 1.0)],
    //     randomness.getRandomArbitrary(1.1, 1.3)
    //   );
    //   $scope.imageContainerSurfaces[itemIndex].delay( randomness.getRandomArbitrary(500, 3000) );
    //   $scope.imageContainerSurfaces[itemIndex].panAndZoom(
    //     [randomness.getRandomArbitrary(0.0, 1.0), randomness.getRandomArbitrary(0.0, 1.0)],
    //     randomness.getRandomArbitrary(1.4, 1.7)
    //   );
    //   $scope.imageContainerSurfaces[itemIndex].delay( randomness.getRandomInt(500, 3000) );
    //   $scope.imageContainerSurfaces[itemIndex].panAndZoom(
    //     null, 1.0, randomness.getRandomInt(3000, 5000));
    // }



    // rotation, translation and opacity of media items

  //  $scope.itemTranslationTransitionable = new Transitionable(0);

    $scope.selectedMediaItems = [];

    $scope.mediaItemTranslators = [];
    $scope.mediaItemRotators = [];
    $scope.mediaItemOpacitators = [];

    $scope.mediaItemAligns = [];

    function initializeMediaItemTransitionables( numberOfItems ) {

      for( var i=0; i < numberOfItems; i++ ) {
        $scope.mediaItemTranslators.push( new Transitionable([0, 0, -20000]) );
        $scope.mediaItemRotators.push( new Transitionable([0, 0, 0]) );
        $scope.mediaItemOpacitators.push( new Transitionable(1) );

        $scope.mediaItemAligns.push( [.5, .5] );
      }


    }
    initializeMediaItemTransitionables( $scope.totalVisibleItems );

    $scope.gridRotator = new Transitionable( [0, 0, 0] );
    $scope.gridTranslator = new Transitionable( [0, 0, 2] );
    $scope.gridScale = new Transitionable( [1, 1, 1] );


    var _modPerspective = new Transitionable( 1000 );
    $scope.modPerspective = function(){
      return _modPerspective.get();
    }


    $scope.mediaItemSizes = [];
    var _mediaSurfaceSize = cubification.getMediaSurfaceSize( headerFooterHeight );
    function setAllMediaItemSizesToGlobalSurfaceSize( numberOfItems ) {

      for( var i=0; i < numberOfItems; i++ ) {
        $scope.mediaItemSizes.push( _mediaSurfaceSize );
      }
    }
    setAllMediaItemSizesToGlobalSurfaceSize( $scope.totalVisibleItems );



    var _isItemAtIndexAtGroundZero = [];

    function animateFromVoidToMiddleGround( itemIndex ) {
      // instantly set the item back into the void...
      var x = xyCenters[itemIndex][0];
      var y = xyCenters[itemIndex][1];
      $scope.mediaItemTranslators[itemIndex].set( [x, y, -20000] );
      // ...and as invisible
      $scope.mediaItemOpacitators[itemIndex].set(0);
      // and then animate into the middle ground
      var zDistance = -randomness.getRandomInt(500, 2500);
      var animDuration = randomness.getRandomInt(1500, 2500);
      var negOrPos = randomness.getNegativeOrPositiveMultiplier();

      $scope.mediaItemTranslators[itemIndex].set(
        [x, y, zDistance],
        {duration: animDuration, curve: 'easeOut'}
      );
      $scope.mediaItemRotators[itemIndex].set(
        [0, (Math.PI * 2 + Math.PI) * negOrPos, 0],
        {duration: animDuration}
      );
      $scope.mediaItemOpacitators[itemIndex].set(
        1,
        {duration: animDuration},
        //  after animation from the void, call shouldWaitForMoreMediaItemsToBeFetched
        // ...which then calls rotateOneCircleAndCallBack (rotate in place at the middle ground)
        // ...which then calls back shouldWaitForMoreMediaItemsToBeFetched...
        function() {

          // shouldWaitForMoreMediaItemsToBeFetched(
          //     itemIndex, animDuration, 0, 1, false, negOrPos );
          waitForSufficientMediaItemFetchBeforeAnimationToGroundZero(
            itemIndex, animDuration, negOrPos,
            ! haveFetchedMediaItemsReachedMinimumThreshold()
          );
        }
      );
    }

    function getSanitizationRemainder( itemIndex ) {
      var currentRotation = $scope.mediaItemRotators[itemIndex].get();
      var fullCircleRemainder = currentRotation[1] % Math.PI*2;
      // console.log( "fullCircleRemainder: " + fullCircleRemainder );
      return fullCircleRemainder;
    }

    function sanitizeMediaSurfaceRotation( itemIndex, sanitizationRemainder ) {
      // var currentRotation = $scope.mediaItemRotators[itemIndex].get();
      // var fullCircleRemainder = currentRotation[1] % Math.PI;
      //
      // if( Math.abs(fullCircleRemainder) > 0 ) {
      //   console.log( "sanitizeMediaSurfaceRotation at index: " + itemIndex );
      //   // $scope.$apply(function () {
          if( $scope.mediaItemRotators[itemIndex].isActive() ) {
            console.log( "halting" );
            $scope.mediaItemRotators[itemIndex].halt();
          }
          var newRotation;
          if( Math.abs(sanitizationRemainder) > Math.PI/2 ) {
            newRotation = [0,0,0];
          } else {
            var currentRotation = $scope.mediaItemRotators[itemIndex].get();
            newRotation = [
              currentRotation[0],
              currentRotation[1] + sanitizationRemainder,
              currentRotation[2]
            ];
          }
          $scope.mediaItemRotators[itemIndex].set(
            newRotation,
            {duration:500}, function() {

              autoRollAfterDelay( itemIndex );
            });
        // });
        // if(!$scope.$$phase) $scope.$apply();
        // console.log( "after sanitization at " + itemIndex );
        // console.log( $scope.mediaItemRotators[itemIndex].get() );
      // }
    }

    var _autoRollTimeoutIDs = [];

    function autoRollAfterDelay( itemIndex ) {

      // let's auto roll!
      _autoRollTimeoutIDs[itemIndex] = Timer.setTimeout( function() {

        _autoRollTimeoutIDs[itemIndex] = null;

        if( null === _currentMaximizedSurfaceIndex && ! inCubicState ) {

          var sanitizationRemainder = getSanitizationRemainder( itemIndex );
          if( Math.abs(sanitizationRemainder) > 0 ) {

            sanitizeMediaSurfaceRotation( itemIndex, sanitizationRemainder );
          } else {

            selectItem( itemIndex, false );
          }
        }
      },
      // randomness.getRandomInt(2500, 6000)
      $scope.range.flowSpeed * (1000 * randomness.getRandomArbitrary(.5, 1.5))
      );
    }

    function animateFromMiddleGroundToGroundZero(
        itemIndex, animDuration, negOrPos ) {

      negOrPos = negOrPos || randomness.getNegativeOrPositiveMultiplier();
      var x = xyCenters[itemIndex][0];
      var y = xyCenters[itemIndex][1];
      var lessDuration = animDuration || 0;
      var xyPosOffsets = getXYposOffsets( negOrPos );
      var zOffset = 0; // -randomness.getRandomInt(0, 100);
      var currentYRotation = $scope.mediaItemRotators[itemIndex].get()[1];

      $scope.mediaItemTranslators[itemIndex].set(
        [x + xyPosOffsets.x, y + xyPosOffsets.y, zOffset],

        {
          duration: lessDuration,
          curve: Easing.outBack
        }, function() {

          colorFlow.startWatchingColorFlowValues();

          _isItemAtIndexAtGroundZero[itemIndex] = true;
          cubifyStripIfItHasBeenRequested();

          autoRollAfterDelay( itemIndex );
        }
  /*
        {
          method: 'snap',
          period: lessDuration,
          dampingRatio: .6
        }
  */
      );
      $scope.mediaItemRotators[itemIndex].set(
        [0, currentYRotation + Math.PI * negOrPos, 0],
        {
          duration: lessDuration,
          curve: Easing.outBack
        }
  /*
        {
          method: 'spring',
          period: lessDuration,
          dampingRatio: .6
        }
  */
      );
    }

    function animateFromGroundZeroToMiddleGround( itemIndex, delay ) {
      var x = xyCenters[itemIndex][0];
      var y = xyCenters[itemIndex][1];
      var zDistance = -randomness.getRandomInt(500, 2500);
      var animDuration = randomness.getRandomInt(750, 1000);
      var negOrPos = randomness.getNegativeOrPositiveMultiplier();
      var currentYRotation = $scope.mediaItemRotators[itemIndex].get()[1];

      // Timer.setTimeout(function() {

        if( _currentMaximizedSurfaceIndex != itemIndex ) {

          _isItemAtIndexAtGroundZero[itemIndex] = false;

          $scope.mediaItemTranslators[itemIndex].set(
            [x, y, zDistance],
            {duration: animDuration}
          );
          $scope.mediaItemRotators[itemIndex].set(
            [0, currentYRotation + Math.PI * negOrPos, 0],
            {duration: animDuration, curve: 'easeOut'},
            // perform media selection for this index,
            // trusting that $scope.selectItems() has done the necessary preparation
            // and do some spinning in place at middle ground:
            function() {

              selectItemAndWaitForLoadBeforeAnimationToGroundZero(
                itemIndex, animDuration, negOrPos );
            }
          );

          colorFlow.stopWatchingColorFlowValues();

        }
      // }, delay ? randomness.getRandomInt(2500, 5000) : 0 );

    }



    function haveFetchedMediaItemsReachedMinimumThreshold() {
      // check if fetched media items have reached a minimum threshold
      return Object.keys(mediaItemHarvester.allItems()).length >= $scope.totalVisibleItems;
    }

    function waitForSufficientMediaItemFetchBeforeAnimationToGroundZero(
        itemIndex, animDuration, negOrPos, shouldWaitForMoreFetched ) {

      if( shouldWaitForMoreFetched ) {
        Timer.setTimeout(function() {
          waitForSufficientMediaItemFetchBeforeAnimationToGroundZero(
            itemIndex, animDuration, negOrPos,
            ! haveFetchedMediaItemsReachedMinimumThreshold()
          );
        }, 1000);
      } else {
        selectItemAndWaitForLoadBeforeAnimationToGroundZero(
            itemIndex, animDuration, negOrPos );
      }
    }

    function itemIsDuplicated( itemIndex, itemCandidate ) {
      var isDuplicated = false;
      if( itemCandidate ) {
        for( var i=0; i < $scope.selectedMediaItems.length; i++ ) {
          if( i != itemIndex &&
                $scope.selectedMediaItems[i].mediaUrl === itemCandidate.mediaUrl ) {
            isDuplicated = true;
            break;
          }
        }
      }
      return isDuplicated;
    }
    function selectItemAndWaitForLoadBeforeAnimationToGroundZero(
        itemIndex, animDuration, negOrPos ) {

      // selectOneItem(
      //   itemIndex, animDuration, negOrPos, fetchEmbedDataToMediaItem );
      var selectionAttemptCount = 0;
      var itemCandidate;
      do {
        itemCandidate = selectOneItem( itemIndex );
        selectionAttemptCount++;

      } while( itemIsDuplicated(itemIndex, itemCandidate)
        && selectionAttemptCount < 66
        && itemCandidate // if no candidate, assume item held
      );
      // console.log("selectionAttemptCount for: " + itemIndex);
      // console.log(selectionAttemptCount);

      if( itemCandidate ) {
        // let's loop through the keys in itemCandidate and set the corresponding
        // values in $scope.selectedMediaItems[itemIndex] - we do this, instead of
        // a full assignment, because we don't want to confuse the angular $$hashkey
        // $scope.selectedMediaItems[itemIndex] = itemCandidate;
        for( var key in itemCandidate ) {
          $scope.selectedMediaItems[itemIndex][key] = itemCandidate[key];
        }
      }

      waitForLoadBeforeAnimationToGroundZero( itemIndex, animDuration, negOrPos );


      $analytics.eventTrack('feed', {
        category: 'Breeding', label: 'selectMediaItem', noninteraction: true
      });
    }

    // function fetchEmbedDataToMediaItem( itemIndex, animDuration, negOrPos ) {
    //
    //   // TODO: might want to postpone the fetchin of embed data from an API,
    //   //  until it is chosen to see the item full screen,
    //   //  and manually choose a thumbnail url before that?
    //   //  will we then know there is a player?  ...tumblr at least gives a media type...
    //
    //   // embed.addEmbedDataToMediaItem(
    //   //   $scope.selectedMediaItems[itemIndex],
    //   //   // callback with bound parameters, so the caller can just say callback():
    //   //   waitForLoadBeforeAnimationToGroundZero.bind(null, itemIndex, animDuration, negOrPos)
    //   // );
    //
    //   waitForLoadBeforeAnimationToGroundZero( itemIndex, animDuration, negOrPos );
    // }

    function waitForLoadBeforeAnimationToGroundZero( itemIndex, animDuration, negOrPos ) {

      var mediaItem = $scope.selectedMediaItems[itemIndex];
      // var imageUrl = embed.getThumbnailURL( mediaItem );
      var imageUrl = mediaItem.mediaUrl;

      // add information on whether the media item contains timebased media,
      // such as video or sound, which has a player available
      // (displayed in full screen when item clicked)

      // mediaItem.isPlayer = embed.isPlayer( mediaItem );


      // setMediaSurfaceContent( itemIndex, imageUrl );

      // loading callback based on
      // http://stackoverflow.com/a/3281711/169858 && http://jsfiddle.net/7bmhf/
      $('<img>').attr('src',function(){

          return imageUrl;
      }).load(function(){

        Timer.setTimeout(function(){

          animateFromMiddleGroundToGroundZero( itemIndex, animDuration, negOrPos );

        }, randomness.getRandomInt(50, 2000) );
      });
    }



    ////////////////////////////////////
    // detail surface in full screen size


    $scope.scrollDetailsEventHandler = new EventHandler();

    var _currentMaximizedSurfaceIndex = null;

    $scope.showFullScreenControls = false;
    $scope.detailSurfaceOpacitator = new Transitionable( 0 );
    $scope.detailSurfaceEmbed = [];
    $scope.detailSurfaceSize = new Transitionable( _mediaSurfaceSize );
    $scope.detailPadlockSize;
    $scope.detailPadlockTranslation = [0, 0, 0];
    $scope.detailCollapseButtonTranslation = [0, 0, 0];

    var _detailSurfaceBackgroundImage = "img/heart_background_square.png";
    $scope.detailSurfaceBackgroundImage = _detailSurfaceBackgroundImage;

    // $scope.detailSurfaceSize = _mediaSurfaceSize;

    // $scope.backsideContainerSurfaceOptions = {
    //   properties: {
    //     zIndex: '2',
    //     overflow:'hidden'
    //   }
    // }
    $scope.backsideSurfaceZindex = '2';

    var _beforeMaximizeTransforms = [];

    function saveBeforeMaximizeTransforms( index ) {
      var currentSurfaceTranslation = $scope.mediaItemTranslators[index].get();
      var currentSurfaceRotation =  $scope.mediaItemRotators[index].get();
      _beforeMaximizeTransforms[index] = {};
      _beforeMaximizeTransforms[index].translation = currentSurfaceTranslation;
      _beforeMaximizeTransforms[index].rotation = currentSurfaceRotation;
    }


    function fetchMaximizedMetaAndCallBack( itemIndex ) {
      embed.getMaximizedItemMeta(
        $scope.selectedMediaItems[itemIndex],
        [window.innerWidth, window.innerHeight - headerFooterHeight],
        maximizeItem.bind(null, itemIndex)
      );
    }

    function maximizeItem( itemIndex, maximizedMeta ) {
      if( maximizedMeta && maximizedMeta.width && maximizedMeta.height ) {

        var maximizedSize = [ maximizedMeta.width, maximizedMeta.height  ];
        // $scope.detailSurfaceSize = maximizedSize;

        var embedMarkup;
        // TODO: let's not use embedded iframe players for now
        // as fitting them can be clumbsy, at least from facebook...
        // if( maximizedMeta.embed ) {
        //   embedMarkup = maximizedMeta.embed;
        // } else {
          $scope.detailSurfaceBackgroundImage = maximizedMeta.image;
          embedMarkup =
            // '<img src="'+maximizedMeta.icon+'">&nbsp;'
            '<a href="'
            + maximizedMeta.link + '" target="_blank">'
            + (maximizedMeta.isPlayer ? '&#9658; ' : '')
            + maximizedMeta.title + '</a>';
        // }

        $scope.detailSurfaceEmbed[itemIndex] = $sce.trustAsHtml( embedMarkup );

        $scope.mediaItemSizes[itemIndex] = $scope.detailSurfaceSize;
        // so now it is a Transitionable, instead of an array as initially
        $scope.detailSurfaceSize.set(
          maximizedSize, {duration: 1000, curve: 'easeOut'}, function() {


            var currentSize = $scope.detailSurfaceSize.get();
            $scope.detailPadlockSize = currentSize[0] / 6;
            $scope.detailPadlockTranslation = [
              currentSize[0]/2 - $scope.detailPadlockSize*.5,
              currentSize[1]/2 - $scope.detailPadlockSize*.7,
              2
            ];
            $scope.detailCollapseButtonTranslation = [
              - currentSize[0]/2 + $scope.detailPadlockSize*.6,
              currentSize[1]/2 - $scope.detailPadlockSize*.6,
              2
            ];
            $scope.showFullScreenControls = true;
          }
        );
      }
    }

/*
    function fetchEmbedDataToMediaItemAndPlaceOnDetailSurface( itemIndex ) {
      embed.addEmbedDataToMediaItem(
        $scope.selectedMediaItems[itemIndex],
        // callback with bound parameters, so the caller can just say callback():
        // populateDetailSurfaceEmbed.bind(null, itemIndex)
        findMaximizedSurfaceSizeAndAnimate.bind(null, itemIndex)
      );
    }
*/
    // function populateDetailSurfaceEmbed( index ) {
    //
    //   $scope.detailSurfaceEmbed = $sce.trustAsHtml(
    //     embed.getFullScreenEmbedMarkup( $scope.selectedMediaItems[index] ) );
    //
    //   findMaximizedSurfaceSizeAndAnimate( index );
    // }
/*
    function findMaximizedSurfaceSizeAndAnimate( index ) {

      var maximizedSize = embed.getEmbedSurfaceSize(
        $scope.selectedMediaItems[index],
        [window.innerWidth, window.innerHeight - headerFooterHeight] );

      $scope.detailSurfaceSize = [
        maximizedSize[0], maximizedSize[1]  ];


      $scope.mediaItemSizes[index] = $scope.detailSurfaceSize;
      // so now it is a Transitionable, instead of an array as initially
      $scope.detailSurfaceSize.set(
        maximizedSize, {duration: 1000, curve: 'easeOut'}, function() {

          var embedMarkup = embed.getFullScreenEmbedMarkup(
            $scope.selectedMediaItems[index] );
          // console.log( "embedMarkup" );
          // console.log( embedMarkup );
          $scope.detailSurfaceEmbed = $sce.trustAsHtml( embedMarkup );

          $scope.detailPadlockSize = $scope.detailSurfaceSize.get()[0] / 6;
          var currentSize = $scope.detailSurfaceSize.get();
          $scope.detailPadlockTranslation = [
            currentSize[0]/2 - $scope.detailPadlockSize*.5,
            currentSize[1]/2 - $scope.detailPadlockSize*.7,
            2
          ];
          $scope.detailCollapseButtonTranslation = [
            - currentSize[0]/2 + $scope.detailPadlockSize*.6,
            currentSize[1]/2 - $scope.detailPadlockSize*.6,
            2
          ];
          $scope.showFullScreenControls = true;
        }
      );
    }
*/


    $scope.maximizeMediaSurface = function( index ) {

      if( _isItemAtIndexAtGroundZero[index] ) {

        if( null === _currentMaximizedSurfaceIndex && ! inCubicState ) {
          _currentMaximizedSurfaceIndex = index;

          saveBeforeMaximizeTransforms( index );

          // $scope.backsideContainerSurfaceOptions.properties.zIndex = '7';
          $scope.backsideSurfaceZindex = '7';

          $scope.detailSurfaceEmbed[index] = $sce.trustAsHtml("<p>Loading...</p>");


          var maximizedTransform = [
            window.innerWidth/2, window.innerHeight/2 - headerFooterHeight/2, 0];

          var currentRotation = $scope.mediaItemRotators[index].get();
          var maximizedRotation = [
            currentRotation[0],
            currentRotation[1]+Math.PI,
            currentRotation[2] ];

          $scope.mediaItemRotators[index].set(
            maximizedRotation, {duration: 1200, curve: 'easeOut'}
          );

          $scope.mediaItemTranslators[index].set(
            maximizedTransform, {duration: 1200, curve: 'easeOut'}
          );


          // results in a callback to findMaximizedSurfaceSizeAndAnimate(...):
          // fetchEmbedDataToMediaItemAndPlaceOnDetailSurface( index );
          fetchMaximizedMetaAndCallBack( index );

          $scope.showPadlocks = false;

          $analytics.eventTrack('click', {
            category: 'UI Interaction', label: 'maximizeMediaSurface'
          });
        }

      }
    }

    $scope.demaximizeMediaSurface = function( index ) {

      if( null !== _currentMaximizedSurfaceIndex && ! inCubicState ) {

        $scope.showFullScreenControls = false;

        $scope.detailSurfaceSize.set(
          _mediaSurfaceSize, {duration: 1000, curve: 'easeOut'}, function() {

            $scope.mediaItemSizes[index] = _mediaSurfaceSize;
          }
        );

        $scope.mediaItemRotators[index].set(
          _beforeMaximizeTransforms[index].rotation,
          {duration: 1200, curve: 'easeOut'}
        );

        $scope.mediaItemTranslators[index].set(
          _beforeMaximizeTransforms[index].translation,
          {duration: 1200, curve: 'easeOut'},
          function() {

            $scope.showPadlocks = true;

            $scope.detailSurfaceEmbed[index] = "";

            // $scope.detailSurfaceSize = _mediaSurfaceSize;

            $scope.detailSurfaceBackgroundImage = _detailSurfaceBackgroundImage;

            _currentMaximizedSurfaceIndex = null;

            // $scope.backsideContainerSurfaceOptions.properties.zIndex = '2';
            $scope.backsideSurfaceZindex = '2';


            if( _hasCubificationBeenRequested ) {
              cubifyStripIfItHasBeenRequested();
            } else {
              // for( var i=0; i < $scope.totalVisibleItems; i++ ) {
              //   if( _isItemAtIndexAtGroundZero[i] ) {
              //     selectItem( i, false, true );
              //   }
              // }
              for( var i=0; i < $scope.totalVisibleItems; i++ ) {
                if( !_autoRollTimeoutIDs[ i ] ) {
                  autoRollAfterDelay( i );
                }
              }
            }

          }
        );

        $analytics.eventTrack('click', {
          category: 'UI Interaction', label: 'demaximizeMediaSurface'
        });
      }
    }



    ////////////////////////////////////
    // size of media item surfaces, and their position offsets:

    // TODO: instead of relying on headerFooterHeight,
    //  might get the available height form the content modifier
    //  ...but had some trouble with that, try agin:
    //  $famous.find('#media-items-grid')[0].renderNode ...getHeight()... something...
    var headerFooterHeight = 50;

    // get available margin space to move surfaces randomly within:
    function getGridCellMargins() {
      var margins = {x:0, y:0};

      var mediaSurfaceEdgeLength = _mediaSurfaceSize[0];

      if( window.innerWidth < window.innerHeight ) {
        // we have two media surfaces along the X axis
        if( 2 * mediaSurfaceEdgeLength < window.innerWidth ) {
          // whatever margin space we'll have will be along the X axis
          margins.x = (window.innerWidth - 2 * mediaSurfaceEdgeLength) / 4;
        } else {
          // whatever margin space we'll have will be along the Y axis
          margins.y =
            (window.innerHeight - headerFooterHeight - 3 * mediaSurfaceEdgeLength) / 6;
        }
      } else {
        // we have three media surfaces along the X axis
        if( 3 * mediaSurfaceEdgeLength < window.innerWidth ) {
          // whatever margin space we'll have will be along the X axis
          margins.x = (window.innerWidth - 3 * mediaSurfaceEdgeLength) / 6;
        } else {
          // whatever margin space we'll have will be along the Y axis
          margins.y =
            (window.innerHeight - headerFooterHeight - 2 * mediaSurfaceEdgeLength) / 4;
        }
      }
      return margins;
    }

    // replaces the use of fa-grid-layout
    function getGridCellCenters() {
      var xyCenters = [];
      if( window.innerWidth < window.innerHeight ) {
        // going to lay things out along [2, 3]
        var horizontalPartSize = window.innerWidth / 2;
        var horizontalPartHalf = horizontalPartSize / 2;
        var verticalPartSize = (window.innerHeight - headerFooterHeight) / 3;
        var verticalPartHalf = verticalPartSize / 2;
        for( var rowCount = 0; rowCount < 3; rowCount++ ) {
          for( var colCount = 0; colCount < 2; colCount++ ) {
            xyCenters.push( [
              colCount * horizontalPartSize + horizontalPartHalf,
              rowCount * verticalPartSize + verticalPartHalf
            ]);
          }
        }
      } else {
        // going to lay things out along [3, 2]
        var horizontalPartSize = window.innerWidth / 3;
        var horizontalPartHalf = horizontalPartSize / 2;
        var verticalPartSize = (window.innerHeight - headerFooterHeight) / 2;
        var verticalPartHalf = verticalPartSize / 2;
        for( var rowCount = 0; rowCount < 2; rowCount++ ) {
          for( var colCount = 0; colCount < 3; colCount++ ) {
            xyCenters.push( [
              colCount * horizontalPartSize + horizontalPartHalf,
              rowCount * verticalPartSize + verticalPartHalf
            ]);
          }
        }
      }
      return xyCenters;

    }
    var xyCenters = getGridCellCenters();

    function getXYposOffsets( negOrPos ) {
      var xOffset = 0;
      var yOffset = 0;
      var margins = getGridCellMargins();
      if( margins.x != 0 || margins.y != 0 ) {
        if( margins.x < margins.y || (margins.x == margins.y && randomness.halfChance()) ) {
          yOffset = randomness.getRandomInt(0, margins.y) * negOrPos;
        } else {
          xOffset = randomness.getRandomInt(0, margins.x) * negOrPos;
        }
      }
      return {
        x: xOffset, y: yOffset
      }
    }



    $scope.resizeEventHandler = new EventHandler();

    $scope.resizeEventHandler.on('resize', function(){
      if( ! inCubicState ) {

        _mediaSurfaceSize = cubification.getMediaSurfaceSize( headerFooterHeight );

        xyCenters = getGridCellCenters();
        $scope.mediaItemTranslators.forEach(function( oneTranslator, itemIndex, array) {
          var x = xyCenters[itemIndex][0];
          var y = xyCenters[itemIndex][1];
          var zOffset = oneTranslator.get()[2];
          oneTranslator.set( [x, y, zOffset] );
        });

      } else {
        // handle surface resizing in cubic state
        _mediaSurfaceSize = cubification.getMediaSurfaceSize( headerFooterHeight );
        var newCubeCenter = cubification.getBoxCenterPoint( headerFooterHeight );

        $scope.mediaItemTranslators.forEach(function( oneTranslator, itemIndex, array) {

          var edgeHalfLength = _mediaSurfaceSize[0] / 2;
          var transformForMediaItemAsSideOnCube = cubification.getBoxSideTransform(
            itemIndex, newCubeCenter, edgeHalfLength,
            $scope.mediaItemRotators[itemIndex].get()[1] );
          $scope.mediaItemTranslators[itemIndex].set(
            transformForMediaItemAsSideOnCube.translate );
        });
        if( $scope.insideSurfacesShown ) {

          cubeInside.calculateGripSizes(
            $scope.gripSize, $scope.gripIconSize, _mediaSurfaceSize[0] );
          cubeInside.calculateGripTranslations(
            $scope.gripSize, $scope.gripTranslations, $scope.gripIconTranslations,
            $scope.totalVisibleItems, _mediaSurfaceSize[0] );

          updateGrabIconFontSize();
        }
      }
      setAllMediaItemSizesToGlobalSurfaceSize( $scope.totalVisibleItems );
    });



    ////////////////////////////////////
    // surface locking

    $scope.showPadlocks = true;

    $scope.padlockOpacity = new Transitionable( .85 );

    function cubifyIfAllItemsAreHeld() {
      var allAreHeld = true;
      for( var i=0; i < $scope.selectedMediaItems.length; i++ ) {
        if( ! $scope.selectedMediaItems[i].held ) {
          allAreHeld = false;
          break;
        }
      }
      if( allAreHeld ) {
        cubifyStrip();

        $analytics.eventTrack('click', {
          category: 'UI Interaction', label: 'cubifyStripWhenAllHeld'
        });
      }
    }

    $scope.toggleMediaSurfaceHold = function( itemIndex ) {

      console.log( "toggleMediaSurfaceHold" );

      $scope.selectedMediaItems[itemIndex].held = ! $scope.selectedMediaItems[itemIndex].held;

      if( $scope.selectedMediaItems[itemIndex].held ) {
        cubifyIfAllItemsAreHeld();
      } else {
        selectItem( itemIndex, false );
      }

      $analytics.eventTrack('click', {
        category: 'UI Interaction', label: 'toggleMediaSurfaceHold',
        value: $scope.selectedMediaItems[itemIndex].held
      });
    }

    function hidePadlocks() {

      $scope.padlockOpacity.set( 0, {duration: 1000}, function() {
        $scope.$apply(function () {
          $scope.showPadlocks = false;
        });
      });
    }



    ////////////////////////////////////
    // boxing

    $scope.showKeyhole = false;

    var inCubicState = false;

    var cubeArrangementDefaultBracingDuration0 = 1000;
    var cubeArrangementDefaultBracingDuration1 = 1200;
    var cubeArrangementDefaultBracingDuration2 = 500;
    var cubeArrangementBracingDuration0;
    var cubeArrangementBracingDuration1;
    var cubeArrangementBracingDuration2;
    function arrangeMediaSurfacesIntoACube(
        itemIndex, cubeCenter, edgeHalfLength, bracePosition ) {

      var edgeLength;
      var animDuration;
      var animCurve;
      switch( bracePosition ) {
        case 0:
          edgeLength = edgeHalfLength * 1.5;
          animDuration = cubeArrangementBracingDuration0;
          animCurve = Easing.outExpo;
          break;
        case 1:
          edgeLength = edgeHalfLength * 2;
          animDuration = cubeArrangementBracingDuration1;
          animCurve = Easing.outCirc;
          break;
        case 2:
          edgeLength = edgeHalfLength;
          animDuration = cubeArrangementBracingDuration2;
          animCurve = Easing.outBounce;
          break;
        default:
          break;
      }

      var transformForMediaItemAsSideOnCube = cubification.getBoxSideTransform(
        itemIndex, cubeCenter, edgeLength,
        $scope.mediaItemRotators[itemIndex].get()[1] );

      $scope.mediaItemTranslators[itemIndex].set(
        transformForMediaItemAsSideOnCube.translate,
        {
          duration: animDuration,
          curve: animCurve
        }, function() {

          if( bracePosition < 2 ) {
            arrangeMediaSurfacesIntoACube(
              itemIndex, cubeCenter, edgeHalfLength, ++bracePosition );
          } else {
            if( 0 == itemIndex ) {

              $scope.$apply(function () {
                $scope.showKeyhole = true;
              });

              fading.transitionableFadeIn( $scope.keyholeOpacity );
            }
          }
        }
      );
      if( bracePosition == 0 ) {
        $scope.mediaItemRotators[itemIndex].set(
          transformForMediaItemAsSideOnCube.rotate,
          {duration: animDuration}
        );
      }
    }

    function animateMediaSurfacesIntoACube( numberOfItems ) {

      var cubeCenter = cubification.getBoxCenterPoint( headerFooterHeight );
      var edgeHalfLength = _mediaSurfaceSize[0] / 2;

      cubeArrangementBracingDuration0 = randomness.getRandomInt(
        cubeArrangementDefaultBracingDuration0 * .8,
        cubeArrangementDefaultBracingDuration0 * 1.2 );
      cubeArrangementBracingDuration1 = randomness.getRandomInt(
        cubeArrangementDefaultBracingDuration1 * .8,
        cubeArrangementDefaultBracingDuration1 * 1.2 );
      cubeArrangementBracingDuration2 = randomness.getRandomInt(
        cubeArrangementDefaultBracingDuration2 * .8,
        cubeArrangementDefaultBracingDuration2 * 1.2 );

      for( var i=0; i < numberOfItems; i++ ) {

        arrangeMediaSurfacesIntoACube( i, cubeCenter, edgeHalfLength, 0 );
      }

      inCubicState = true;

  /*
      var cubeRotationSpeed = 10;
      Timer.every(function(){
        var adjustedSpeed = parseFloat(cubeRotationSpeed) / 1200;
        $scope.gridRotator.set($scope.gridRotator.get() + adjustedSpeed);
      }, 1);
  */
      var currentYRotation = $scope.gridRotator.get()[1];
      $scope.gridRotator.set(
        [randomness.getRandomArbitrary(-Math.PI / 4, Math.PI / 4),
          (currentYRotation + Math.PI * 2
            + randomness.getRandomArbitrary(Math.PI / 6, Math.PI / 2)
          ) * randomness.getNegativeOrPositiveMultiplier(),
          0],
        {duration: 4000, curve: Easing.outSine}
      );
    }


    // keyhole transitionable methods

    $scope.keyholeOpacity = new Transitionable( 0 );



    ////////////////////////////////////
    // colour flow

    $scope.flowRGB = [0, 0, 0];

    function updateFlowRGB( newRGB ) {
      $scope.$apply(function () {
        $scope.flowRGB = newRGB;
      });
    }

    colorFlow.registerColorUpdateCallback( updateFlowRGB );



    ////////////////////////////////////
    // cube drag event handling

    var didPinch = false;  // to prevent jumps in rotation after a pinch gesture.

    // generic touch

    GenericSync.register({
      "mouse": MouseSync,
      "touch": TouchSync,
      "scroll": ScrollSync
    });

    $scope.gripTouchHandler = new EventHandler();
    var cubeInsideMovementSync = new GenericSync(["mouse", "touch"]);
    $scope.gripTouchHandler.pipe( cubeInsideMovementSync );

    $scope.cubeTouchHandler = new EventHandler();
    var inputMovementSync = new GenericSync(["mouse", "touch"]);
    $scope.cubeTouchHandler.pipe( inputMovementSync );

    // var INPUT_ROTATION_SENSITIVITY = 120;
    // var lastMax5RotationVelocity = [];
    // var lastMax5Delta = [];
    inputMovementSync.on( 'update', function(data) {

      if( inCubicState ) {
        if( didPinch ) {
          didPinch = false;
        } else {
          cubeRotation.updateRotation( $scope.gridRotator, data );
        }
      }
    });

    inputMovementSync.on( 'end', function() {
      if( didPinch ) {
        didPinch = false;
      } else {
        cubeRotation.animateRotatorWithFauxDrag( $scope.gridRotator );

        $analytics.eventTrack('click', {
          category: 'UI Interaction', label: 'cubeRotatedOutsideBreeding'
        });
      }
    });


    // pinch!
  /*
    var pinchEventHandler = new EventHandler();
    $scope.cubeTouchHandler.pipe( pinchEventHandler );
  */

    var pinchSync = new PinchSync();
    $scope.cubeTouchHandler.pipe( pinchSync );

    var lastPinchVelocity;

  /*
    pinchSync.on( 'start', function() {
      rotationBeforPinch = $scope.gridRotator.get();
    });
  */

    pinchSync.on( 'update', function(data){

      var currentZ = $scope.gridTranslator.get()[2];
      var newZ = currentZ + data.delta;
      $scope.$apply(function () {
        $scope.gridTranslator.set( [0, 0, newZ] );
  //      console.log( "newZ: " + newZ );
      });
      lastPinchVelocity = data.velocity;

  /*
      var newPerspective = 1000 - newZ;
      console.log( "newPerspective: " + newPerspective );
      _modPerspective.set( newPerspective );
  */
    });

    pinchSync.on( 'end', function() {
      didPinch = true;

      var currentZ = $scope.gridTranslator.get()[2];
      if( isInsideCube ) {

        if( currentZ > 700 ) {
          $scope.gridTranslator.set( [0, 0, 700], {
            method: 'snap',
            period: 150,
            damping: .1
          });
        } else if( currentZ < 550 ) {
          $scope.gridTranslator.set( [0, 0, 550], {
            method: 'snap',
            period: 150,
            damping: .1
          });
        }
      } else {

        if( currentZ > 500 ) {
          $scope.gridTranslator.set( [0, 0, 500], {
            method: 'snap',
            period: 150,
            damping: .1
          });
        } else if( currentZ < -1000 ) {
          $scope.gridTranslator.set( [0, 0, -1000], {
            method: 'snap',
            period: 150,
            damping: .1
          });
        }
      }
    });



    ////////////////////////////////////
    // button event methods

    function navigateToRoots() {
      $location.path("/roots");
    }

    $scope.navigateToRoots = function() {

      navigateToRoots();

      $analytics.eventTrack('click', {
        category: 'Navigation', label: 'navigateToRootsFromBreeding'
      });
    }

    $scope.rightFooterButton1 = {};
    $scope.rightFooterButton1.action = selectItems;
    $scope.rightFooterButton1.title = "Roll";
    $scope.rightFooterButton1.visible = true;
    $scope.rightFooterButton1.opacity = new Transitionable( 1 );
    $scope.rightFooterButton1.options = {
      properties: {
        fontSize: '18px',
        paddingTop: '12px',
        color: 'white',
        textAlign: 'center',
        cursor: 'pointer'
      }
    };

    $scope.rightFooterButton2 = {};
    $scope.rightFooterButton2.action = cubifyStrip;
    $scope.rightFooterButton2.title = "Create";
    $scope.rightFooterButton2.visible = true;
    $scope.rightFooterButton2.opacity = new Transitionable( 1 );
    $scope.rightFooterButton2.rotator = new Transitionable( 0 );
    $scope.rightFooterButton2.options = {
      properties: {
        fontSize: '18px',
        paddingTop: '12px',
        color: 'white',
        textAlign: 'center',
        cursor: 'pointer'
      }
    };

    function hideButton( buttonObject ) {

      buttonObject.opacity.set( 0, {duration: 1000} );
      buttonObject.options.properties.cursor = 'default';
      buttonObject.action = undefined;
    }
    function hideButton1() {

      hideButton( $scope.rightFooterButton1 );
    }
    function hideButton2() {

      hideButton( $scope.rightFooterButton2 );
    }


    function getChosenLocation() {
      var location = null;
      if( $scope.geolocationAddresses.length && $scope.geoStatus.selectedAddressIndex >= 0 ) {
        location = {
          "address": $scope.geolocationAddresses[$scope.geoStatus.selectedAddressIndex].formatted_address,
          "latLng": $scope.geolocationAddresses[$scope.geoStatus.selectedAddressIndex].latLng
        };
      }
      return location;
    }

    function getInsideInformation() {
      return {
        "bottleMessage": $scope.message.inBottle, // $scope.bottleMessage.getValue(),
        "location": getChosenLocation()
      };
    }
    function saveStripToLocalStorage() {

      localStorageManager.saveOneCube( {
        "mediaItems": $scope.selectedMediaItems,
        "insideInformation": getInsideInformation(),
        "tags": $scope.cubeTags,
        "savedOnline": false
      } );

      $scope.cubeTags = null;
    }


    var _hasCubificationBeenRequested = false;

    function cubifyStripIfItHasBeenRequested() {
      if( _hasCubificationBeenRequested ) {
        cubifyStrip();
      }
    }
    function areAllItemsAtGroundZero() {
      var allAreAtGroundZero = true;
      for( var i=0; i < $scope.totalVisibleItems; i++ ) {
        if( ! _isItemAtIndexAtGroundZero[i] ) {
          allAreAtGroundZero = false;
          break;
        }
      }
      return allAreAtGroundZero;
    }
    function cubifyStrip() {

      if( areAllItemsAtGroundZero() && null === _currentMaximizedSurfaceIndex) {

        animateMediaSurfacesIntoACube( $scope.totalVisibleItems );

        hideButton1();
        hideButton2();
        hidePadlocks();

        $analytics.eventTrack('click', {
          category: 'UI Interaction', label: 'cubifyStrip'
        });

      } else {
        _hasCubificationBeenRequested = true;

        spinCubificationButtonUntilInCubicState();
      }
    }

    function spinCubificationButtonUntilInCubicState() {
      if( ! inCubicState ) {
        var currentRotation = $scope.rightFooterButton2.rotator.get();
        $scope.rightFooterButton2.rotator.set(
          currentRotation + Math.PI * 2,
          {duration: 2000},
          spinCubificationButtonUntilInCubicState );
      }
    }


    $scope.range = {
      flowSpeed: 3
    };
    $scope.mediaItemsFlowspeedChanged = function() {
      // console.log( $scope.range.flowSpeed );

      $analytics.eventTrack('change', {
        category: 'UI Interaction', label: 'mediaItemsFlowspeedChanged',
        value: $scope.range.flowSpeed
      });
    }



    ////////////////////////////////////
    // cube insides

    var firstItemBottomAligned = false;

    var isInsideCube = false;

    $scope.mediaSurfacesShown = true;

    $scope.insideSurfacesShown = false;

    $scope.hatchInsideShown = true;

    $scope.cubeTags = null;


    $scope.goDeepInside = function() {

      if( $scope.cubeTags === null ) {
        $scope.cubeTags = dataService.getTagsObjectFromMediaItems(
          $scope.selectedMediaItems );
      }

      $scope.insideSurfacesShown = true;

      openCube();
    }


    function openCube() {

      // stop listening to outside surface input events to move cube
      // ...we're on auto pilot now, then we'll later hook up input handling
      // for the inside surfaces...
      $scope.cubeTouchHandler.unpipe( inputMovementSync );

      // we don't want the use fiddling with the grips while entering:
      $scope.gripTouchHandler.unpipe( cubeInsideMovementSync );


      if( ! firstItemBottomAligned ) {

        $scope.mediaItemAligns[0] = cubeInside.getHatchSurfaceOriginForOpening();

        cubeInside.translateHatchSurfaceForOpening(
          $scope.mediaItemTranslators[0], _mediaSurfaceSize[0] );

        firstItemBottomAligned = true;
      }

      // and then rotate it as a door:

      cubeInside.openAndEnterCube(
        $scope.mediaItemRotators[0], postOpenCallback,
        $scope.gridRotator, $scope.gridTranslator,
        cubeEntryDuringCallback, cubeEntryDoneCallback
      );

    }

    function postOpenCallback() {

      colorFlow.stopWatchingColorFlowValues();
      fading.stopTransitionableFading( $scope.keyholeOpacity );
      $scope.keyholeOpacity.set(0);
      $scope.showKeyhole = false;

      populateProfileArrays();  // profile data population from db
      populateOptionArrays();
    }

    function cubeEntryDuringCallback() {

      showBottleMessageTextArea();

      geocoding.getAddressesCloseToUserLocation(
        applyGeocodingResults, handleGeolocationDenial );

      $scope.gripTouchHandler.pipe( cubeInsideMovementSync );
    }

    function cubeEntryDoneCallback() {

      $scope.$apply(function () {
        $scope.mediaSurfacesShown = false;
        $scope.hatchInsideShown = false;
      });

      isInsideCube = true;

      $analytics.eventTrack('click', {
        category: 'UI Interaction', label: 'cubeEntryBreeding'
      });
    }



    ////////////////////////////////////
    // inside grip sizes and positions

    $scope.gripSize = {};
    $scope.gripIconSize = {};
    $scope.gripTranslations = [];
    $scope.gripIconTranslations = [];

    cubeInside.calculateGripSizes(
      $scope.gripSize, $scope.gripIconSize, _mediaSurfaceSize[0] );
    cubeInside.calculateGripTranslations(
      $scope.gripSize, $scope.gripTranslations, $scope.gripIconTranslations,
      $scope.totalVisibleItems, _mediaSurfaceSize[0] );



    function startContinuousCubeRotation() {
      console.log( "startContinuousCubeRotation" );
      var cubeRotationSpeed = 10;
      Timer.every(function(){
        var adjustedSpeed = parseFloat(cubeRotationSpeed) / 1200;
        var currentZ = $scope.gridRotator.get()[1];
        $scope.gridRotator.set([0, currentZ + adjustedSpeed, 0]);
      }, 1);
    }



    ////////////////////////////////////
    // cube inside surfaces configuration - input surfaces

    $scope.showBottleMessage = false;
    $scope.bottleMessageOpacitator = new Transitionable( 0 );
    $scope.metaInfoEntryOpacitator = new Transitionable( 0 );

    function showBottleMessageTextArea() {

      // fade in text area for message entry
      $scope.showBottleMessage = true;
      $scope.bottleMessageOpacitator.set(
        1, {duration: 2000, curve: Easing.inBounce}, function() {

          // $("#bottlemessage").on('touchstart', _onTextareaTouchstart.bind(this));
          // $("#bottlemessage").on('touchmove', _onTextareaTextmove.bind(this));
          // $("#bottlemessage").on('touchend', _onTextareaTouchend.bind(this));
          // $("#bottlemessage").keyup(_onTextareaKeyup.bind(this));
        }
      );
    }


    // $scope.bottleMessage = new TextareaSurface({
    //   placeholder: 'Message in a bottle:',
    //   rows: '30',
    //   cols: '30'
    // });

    // $scope.bottleMessage.on('touchstart', _onTextareaTouchstart.bind(this));
    // $scope.bottleMessage.on('touchmove', _onTextareaTextmove.bind(this));
    // $scope.bottleMessage.on('touchend', _onTextareaTouchend.bind(this));
    // $scope.bottleMessage.on('keyup', _onTextareaKeyup.bind(this));

    $scope.message = {
      inBottle: "",
      charactersAvailable: 151
    }
    // $scope.bottleMessageCharactersAvailable = 151; // is a prime number
    // $scope.bottlemessage = "";
    $scope.updateBottleMessage = function() {
      // console.log("$scope.updateBottleMessage");
      var messageLength = $scope.message.inBottle.length; // $scope.bottleMessage.getValue().length
      var charsLeft = 151 - messageLength;
      if( charsLeft < 0 ) {
        $scope.message.inBottle = $scope.message.inBottle.substring(0, 151);
        charsLeft = 0;
      }
      // $scope.$apply(function () {
        $scope.message.charactersAvailable = charsLeft;
      // });

      setMetaSurfacesOpacityRelativeToBottleMessageLength( messageLength );
    }


    $scope.grabIconFontSize = $scope.gripIconSize.x / 2;
    function updateGrabIconFontSize() {
      $scope.$apply(function () {
        $scope.grabIconFontSize = $scope.gripIconSize.x / 2;
      });
    }

    // var bottleMessageLastMoveY = undefined;
    //
    // function _onTextareaTouchstart(event) {
    //
    //   // the following event prevention because
    //   // the textarea was sometimes loosing focus in Chrome on iOS
    //   // $scope.bottleMessage.focus();
    //
    // }
    // function _onTextareaTouchend(event) {
    //
    //   bottleMessageLastMoveY = undefined;
    //
    //   //event.preventDefault();
    //   // event.stopPropagation();
    //
    //   //$scope.bottleMessage.focus();
    // }
    // function _onTextareaTextmove(event) {
    //
    //   if( bottleMessageLastMoveY ) {
    //     event.target.scrollTop = Math.max(
    //       0, event.target.scrollTop + bottleMessageLastMoveY - event.touches[0].clientY );
    //   }
    //   bottleMessageLastMoveY = event.touches[0].clientY;
    // }
    //
    // $scope.bottleMessageCharactersAvailable = 151; // is a prime number
    // function _onTextareaKeyup(event) {
    //   var messageLength = getBottleMessage().length; // $scope.bottleMessage.getValue().length
    //   var charsLeft = 151 - messageLength;
    //   if( charsLeft < 0 ) {
    //     $scope.bottleMessage.setValue(
    //       // $scope.bottleMessage.getValue().substring(0, 151)
    //       getBottleMessage().substring(0, 151)
    //     );
    //     charsLeft = 0;
    //   }
    //   $scope.$apply(function () {
    //     $scope.bottleMessageCharactersAvailable = charsLeft;
    //   });
    //
    //   setMetaSurfacesOpacityRelativeToBottleMessageLength( messageLength );
    // }


    function setMetaSurfacesOpacityRelativeToBottleMessageLength( messageLength ) {
      var fullOpacityLength = 11;  // is a prime numer
      var opacityPercentage = Math.min( messageLength, fullOpacityLength ) / fullOpacityLength;
      $scope.metaInfoEntryOpacitator.set( opacityPercentage, {duration: 50} );
    }


    ////////////////////////////////////
    // cube inside data entry handling

    $scope.scrollTagsEventHandler = new EventHandler();
    $scope.scrollProfileEventHandler = new EventHandler();
    $scope.scrollLocationEventHandler = new EventHandler();

    $scope.scrollContainerOptions = {
      properties: {
        overflow: 'hidden',
        zIndex: '3'
      }
    };

    // note to self:  not doing 3-way data binding here to Firebase
    // as a user may or may not be logged in when entering profile information
    // ...later when sending the cube, will be required to log in if is not.


    $scope.insideFormData = {
      newTag: "",
      gender: "",
      customGender: "",
      lookingFor: "",
      customLookingFor: "",
      relationship: "",
      customRelationship: "",
      orientation: "",
      customOrientation: ""
    };
    $scope.profileGender = [null];
    $scope.profileLookingFor = [null];
    $scope.profileRelationships = [null];
    $scope.profileOrientation = [null];

    $scope.onlineGenders = ["Male", "Female", "Transgender"];  // TODO: read from db
    $scope.onlineLookingFor = ["Friendship", "Events", "Relationship"];  // TODO: read from db
    $scope.onlineRelationships = ["Single", "Dating", "In A Relationship", "Friend With Benefits", "Married"];  // TODO: read from db
    $scope.onlineOrientation = ["Straight", "Heteroflexible", "Bisexual", "Homoflexible", "Gay", "Lesbian", "Queer", "Pansexual", "Fluctuating/Evolving", "Asexual", "Unsure"];  // TODO: read from db

    function populateProfileArrays() {

      $scope.profileGender = profileData.getArrayWithGender();
      $scope.profileLookingFor = profileData.getArrayWithSeeking();
      $scope.profileRelationships = profileData.getArrayWithRelationship();
      $scope.profileOrientation = profileData.getArrayWithOrientation();
    }

    function populateOptionArrays() {
      $scope.onlineGenders = profileData.getOptionArrayGender();
      // console.log( "$scope.onlineGenders:" );
      // console.log( $scope.onlineGenders );
      $scope.onlineLookingFor = profileData.getOptionArrayLookingFor();
      $scope.onlineRelationships = profileData.getOptionArrayRelationship();
      $scope.onlineOrientation = profileData.getOptionArrayOrientation();
    }

    function saveProfileObjectToLocalStorage() {
      var profileObject ={
        "gender": $scope.profileGender,
        "seeking": $scope.profileLookingFor,
        "relationship": $scope.profileRelationships,
        "orientation": $scope.profileOrientation
      };
      localStorageManager.saveProfile( profileObject );
    }


    ///// tags
    $scope.removeTag = function( tagToRemove ) {
      var scrollView = $famous.find('#tagEditScroll')[0].renderNode;
      var scrollPos = scrollView.getAbsolutePosition();

      delete $scope.cubeTags[tagToRemove];

      scrollView.setPosition( scrollPos );

      $analytics.eventTrack('entry', {
        category: 'UI Interaction', label: 'removeTag'
      });
    }

    $scope.addTag = function() {
      var scrollView = $famous.find('#tagEditScroll')[0].renderNode;
      var scrollPos = scrollView.getAbsolutePosition();

      $scope.cubeTags[$scope.insideFormData.newTag] = 1;
      $scope.insideFormData.newTag = "";

      scrollView.setPosition( scrollPos );

      $analytics.eventTrack('entry', {
        category: 'UI Interaction', label: 'addTag'
      });
    }

    function addToItemList( itemList, firstValue, secondValue, bucketKey ) {
      // temporary hack to get the scrollview not to jump when items are added or removed
      // added a fix to .getAbsolutePosition() from https://github.com/Famous/famous/commit/3f6c2316c7d6a982e5b93fde88d6cf4031778cb4
      // - via https://github.com/Famous/famous-angular/issues/256
      // might also want to consider https://github.com/Famous/famous-angular/pull/223
      //  (links to: https://github.com/Famous/famous/issues/122 )
      // or even try out: https://github.com/IjzerenHein/famous-flex/blob/master/tutorials/FlexScrollView.md
      var scrollView = $famous.find('#profileEditScroll')[0].renderNode;
      var scrollPos = Math.max( 0, Math.round( scrollView.getAbsolutePosition() ) );

      var whatToAdd;
      if( firstValue ) {
        whatToAdd = firstValue;
      } else if( secondValue ) {
        whatToAdd = secondValue;
      }
      if( whatToAdd && itemList.indexOf(whatToAdd) < 0 ) {
        // if( itemList.indexOf(whatToAdd) < 0 ) {
        //
        //   if( onlyOneEntry || itemList.length === 1 && itemList[0] === null ) {
        //     itemList[0] = whatToAdd;
        //   } else {
        //     itemList.push( whatToAdd );
        //   }
        // }

        var addedToQueue = profileData.addValueToBucketssOrSaveQueue( whatToAdd, bucketKey );
        if( addedToQueue ) {
          // we can't then rely on an $firebaseArray object automatically updating
          // and we'll push to the local array we got handed from profileData.getArrayWith
          itemList.push( whatToAdd );
        }

        $analytics.eventTrack('entry', {
          category: 'UI Interaction', label: 'addItemToList', value: bucketKey
        });
      }

      scrollView.setPosition( scrollPos );
    }

    function removeItemFromList( itemList, index, bucketKey ) {
      var scrollView = $famous.find('#profileEditScroll')[0].renderNode;
      var scrollPos = Math.max( 0, Math.round( scrollView.getAbsolutePosition() ) );

      // if( onlyOneEntry || itemList.length === 1 ) {
      //   itemList[0] = null;
      // } else {
      //   itemList.splice(index, 1);
      // }

      var valueToRemove = itemList[index];
      if( valueToRemove.$id ) {
        valueToRemove = valueToRemove.$id;
      }
      var removedFromQueue =
        profileData.removeValueFromBucketssOrSaveQueue( valueToRemove, bucketKey );
      if( removedFromQueue ) {
        // we can't then rely on an $firebaseArray object automatically updating
        // and we'll remove from the local array we got handed from profileData.getArrayWith
        itemList.splice(index, 1);
      }

      scrollView.setPosition( scrollPos );

      $analytics.eventTrack('entry', {
        category: 'UI Interaction', label: 'removeItemFromList', value: bucketKey
      });
    }


    ///// profile
    // - gender
    $scope.addGender = function() {

      addToItemList(
        $scope.profileGender,
        $scope.insideFormData.customGender,
        $scope.insideFormData.gender,
        "gender");
      $scope.insideFormData.gender = "";
      $scope.insideFormData.customGender = "";
    }
    $scope.removeGender = function( index ) {
      removeItemFromList( $scope.profileGender, index, "gender" );
    }

    // - looking for
    $scope.addLookingFor = function() {

      addToItemList(
        $scope.profileLookingFor,
        $scope.insideFormData.customLookingFor,
        $scope.insideFormData.lookingFor,
        "seeking" );
      $scope.insideFormData.lookingFor = "";
      $scope.insideFormData.customLookingFor = "";
    }
    $scope.removeLookingFor = function( index ) {
      removeItemFromList( $scope.profileLookingFor, index, "seeking" );
    }

    // - relationship status
    $scope.addRelationship = function() {

      addToItemList(
        $scope.profileRelationships,
        $scope.insideFormData.customRelationship,
        $scope.insideFormData.relationship,
        "relationship" );
      $scope.insideFormData.relationship = "";
      $scope.insideFormData.customRelationship = "";
    }
    $scope.removeRelationship = function( index ) {
      removeItemFromList( $scope.profileRelationships, index, "relationship" );
    }

    // - sexual orientation
    $scope.addOrientation = function() {

      addToItemList(
        $scope.profileOrientation,
        $scope.insideFormData.customOrientation,
        $scope.insideFormData.orientation,
        "orientation" );
      $scope.insideFormData.orientation = "";
      $scope.insideFormData.customOrientation = "";
    }
    $scope.removeOrientation = function( index ) {
      removeItemFromList( $scope.profileOrientation, index, "orientation" );
    }


    ///// location

    $scope.geolocationAddresses = [];
    $scope.geoStatus = {
      selectedAddressIndex : -1,
      geolocationNotAvailable : false,
      geolocationDenied : false
    };

    function applyGeocodingResults( geocodingResults ) {
      if( null == geocodingResults ) {
        $scope.geoStatus.geolocationNotAvailable = true;
      } else {
        $scope.geolocationAddresses = geocodingResults;
        if( geocodingResults.length > 1 ) {
          $scope.geoStatus.selectedAddressIndex = 1;
        } else if( geocodingResults.length ) {
          $scope.geoStatus.selectedAddressIndex = 0;
        }
      }
    }
    function handleGeolocationDenial() {
      $scope.geoStatus.geolocationDenied = true;
    }

    // $scope.requestCloseAddresses = function() {
    //   geocoding.getAddressesCloseToUserLocation(
    //     applyGeocodingResults, handleGeolocationDenial );
    // }

    $scope.setAddressIndex = function( addressIndex ) {
      $scope.geoStatus.selectedAddressIndex = addressIndex;

      $analytics.eventTrack('click', {
        category: 'UI Interaction', label: 'setAddressIndex', value: addressIndex
      });
    }



    ////////////////////////////////////
    // cube inside drag event handling

    cubeInsideMovementSync.on( 'start', function(data) {

      cubeInside.startCubeInsideRotation( $scope.gridRotator );
    });

    cubeInsideMovementSync.on( 'update', function(data) {
      if( didPinch ) {
        didPinch  = false;
      } else {

        cubeInside.updateCubeInsideRotation( $scope.gridRotator, data );
      }
    });

    cubeInsideMovementSync.on( 'end', function(data) {
      if( didPinch ) {
        didPinch = false;
      } else {

        cubeInside.snapCubeRotationToQuartCircles(
          data,
          $scope.gridRotator, $scope.gridTranslator, $scope.mediaItemRotators[0],
          cubeExitDuringCallback, cubeExitDoneCallback, postCloseCallback );


        $analytics.eventTrack('click', {
          category: 'UI Interaction', label: 'cubeRotatedInsideBreeding'
        });
      }
    });

    function cubeExitDuringCallback() {
      $scope.$apply(function () {
        $scope.mediaSurfacesShown = true;
        $scope.hatchInsideShown = true;
      });
    }

    function cubeExitDoneCallback() {

      $scope.cubeTouchHandler.pipe( inputMovementSync );

      isInsideCube = false;

      $analytics.eventTrack('click', {
        category: 'UI Interaction', label: 'cubeExitBreeding'
      });
    }

    function postCloseCallback() {

      $scope.$apply(function () {
        $scope.showKeyhole = true;
        fading.transitionableFadeIn( $scope.keyholeOpacity );
        colorFlow.startWatchingColorFlowValues();
      });

      checkIfCubeIsReadyForSpace();
    }



    ////////////////////////////////////
    // cubes (pigs) in space functionality

    $scope.showRocket = false;
    $scope.rocketOpacity = new Transitionable( 0 );

    function checkIfCubeIsReadyForSpace() {

      // TODO: check if all cube internals have been sufficiently filled out
      //    ...then offer the rocket icon on all sides but the one having the keyhole
      //    ...which will send the cube into space by pressing it...
      //    ...triggering a login action if required.

      // if( $scope.bottleMessage.getValue().length ) {
      if( $scope.message.inBottle.length ) {
        $scope.$apply(function () {
          $scope.showRocket = true;
        });
        fading.transitionableFadeIn( $scope.rocketOpacity );
      } else {
        $scope.$apply(function () {
          $scope.showRocket = false;
        });
        fading.stopTransitionableFading( $scope.rocketOpacity );
      }
    }

    $scope.sendCubeIntoSpace = function() {
      var spaceTravelDuration = 1300;
      var currentCubeTranslation = $scope.gridTranslator.get();
      var currentCubeRotation = $scope.gridRotator.get();

      var currentRotation = $scope.gridRotator.get();
      $scope.gridRotator.set(
        [
          currentRotation[0] - currentRotation[0] % Math.PI,
          (currentRotation[1] - currentRotation[1] % Math.PI ),
          currentRotation[2]
        ], {duration: spaceTravelDuration}
      );
      $scope.gridTranslator.set(
        [currentCubeTranslation[0], currentCubeTranslation[1], -15000], {
          duration: spaceTravelDuration
        }
      );

      // TV off-ish effect:
      Timer.setTimeout( function() {
        $scope.gridScale.set( [.2, 6, 1], {duration: 250, curve:'easeOut'}, function() {

          Timer.setTimeout( function(){

            $scope.mediaItemOpacitators.forEach( function( oneOpacitator) {
              oneOpacitator.set( 0, {duration:250, curve:'easeOut'});
            });
            $scope.gridScale.set( [40, .02, 1], {duration:150, curve:'easeOut'}, function() {
              Timer.setTimeout( function() {

                navigateToLoginOrCorral();

              }, 1000);
            });

          }, 75);
        });
      }, spaceTravelDuration * .8 );

      $analytics.eventTrack('click', {
        category: 'UI Interaction', label: 'sendCubeIntoSpace'
      });
    }

    function navigateToLoginOrCorral() {

      saveStripToLocalStorage();
      saveProfileObjectToLocalStorage();

      if( $scope.loggedIn ) {
        $location.path( "/corral" );
      } else {
        // send to login page
        $location.path( "/howdy" );
      }
    }



    ////////////////////////////////////
    // media items selection

    // mediaItemHarvester.allItems() = Object.keys(localStorageManager.getAllSavedMediaItems()).length > 0 ?
    //                         localStorageManager.getAllSavedMediaItems() : {};


    $scope.rollDisabled = true;
    $scope.saveDisabled = true;



    var usedIndexes = [];
    // var heldIndexes = [];

    // or AKA:  Roll items
    function selectItems( numberOfItems, isSelectingForInitialPopulation, delay ) {

      for( var i=0; i < numberOfItems; i++ ) {
        selectItem( i, isSelectingForInitialPopulation, delay );
      }

  //     // var combinedTags = {};
  //     // TODO: DELETE $scope.selectedMediaItems = [];
  //     // console.log("Object.keys(mediaItemHarvester.allItems()).length: " + Object.keys(mediaItemHarvester.allItems()).length);
  //
  //     // let's set indexes (into allMediaItems) to -1 for all items that are not held
  //     for( var i=0; i < numberOfItems; i++ ) {
  //       if( usedIndexes[i] !== undefined ) {
  //         if( $scope.selectedMediaItems[i] &&
  //           ( !$scope.selectedMediaItems[i].held && !$scope.selectedMediaItems[i].similar) ) {
  //             usedIndexes[i] = -1;
  //         }
  //       } else {
  //         usedIndexes.push( -1 );
  //       }
  //
  //
  //
  //     }
  //
  //     // collect those indexes (into allMediaItems) that are held, for use in Explore / Exploit
  //     heldIndexes = [];
  //     usedIndexes.forEach( function(idxValue){
  //       if( idxValue > -1 ) heldIndexes.push( idxValue );
  //     });
  //     console.log( "heldIndexes" );
  //     console.log( heldIndexes );
  //
  // /*
  //     $scope.selectedMediaItems = [];
  // */
  //
  //     if( ! isSelectingForInitialPopulation ) {
  //
  //       for( var i=0; i < numberOfItems; i++ ) {
  //
  //         if( !$scope.selectedMediaItems[i].held ) {
  //
  //           animateFromGroundZeroToMiddleGround( i );
  //           // animation callback will call selectOneItem( ... )
  //         }
  //       }
  //     }

    }

    function isItemAnimating( itemIndex ) {

      return $scope.mediaItemTranslators[itemIndex].isActive()
        || $scope.mediaItemRotators[itemIndex].isActive();
    }



    function selectItem( itemIndex, isSelectingForInitialPopulation, delay ) {

      if( itemIndex !== _currentMaximizedSurfaceIndex
          && !_hasCubificationBeenRequested
          && ! isItemAnimating( itemIndex ) ) {

        // let's set indexes (into allMediaItems) to -1 for all items that are not held
        if( usedIndexes[itemIndex] !== undefined ) {
          if( $scope.selectedMediaItems[itemIndex] &&
            ( !$scope.selectedMediaItems[itemIndex].held && !$scope.selectedMediaItems[itemIndex].similar) ) {
              usedIndexes[itemIndex] = -1;
          }
        } else {
          usedIndexes.push( -1 );
        }

        if( ! isSelectingForInitialPopulation ) {

          if( !$scope.selectedMediaItems[itemIndex].held ) {

            animateFromGroundZeroToMiddleGround( itemIndex, delay );
            // animation callback will call selectOneItem( ... )
          }
        }
      }
    }



    // let's start with a default set of media items, to use as loading spinners
  //  var populationIteration = 1;
    function populateSelectedMediaItemsWithPlaceholders( numberOfItems ) {

      for( var i=0;  i < numberOfItems; i++ ) {
        $scope.selectedMediaItems.push({
          mediaUrl: "img/heart_background_square.png"
        });


        animateFromVoidToMiddleGround( i );

  //  // TODO: temporary remove:
  //       if( i == numberOfItems-1 ) {
  //         $scope.insideSurfacesShown = true;
  // //        $scope.cubeTouchHandler.unpipe( inputMovementSync );
  //         cubifyStrip();
  //       }

      }

      selectItems( numberOfItems, true );

  /* failed attempt at setting a delay between loop executions:
      Timer.setTimeout(function(){

        $scope.selectedMediaItems.push({
          mediaUrl: "http://pbs.twimg.com/profile_images/568047917450031104/3DGl47jT.png"
        });

        animateFromVoidToMiddleGround( populationIteration-1 );

        populationIteration++;

        if( populationIteration < $scope.totalVisibleItems ) {

          populateSelectedMediaItemsWithPlaceholders();
        } else {

          $scope.selectItems( $scope.totalVisibleItems, true );
        }
      }, 1000);
  */
    }

    populateSelectedMediaItemsWithPlaceholders( $scope.totalVisibleItems );


  //  populateSelectedMediaItemsWithPlaceholders( 0 );

    function selectOneItem( i /* , animDuration, negOrPos, callback */ ) {

      // TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO
      // handle the fact that $scope.selectedMediaItems is never emptied
      // -> affects comparison of what elements are already there ???
      //    ...or is there no further action required -> analyze!
      // TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO

      var itemCandidate;

      // collect those indexes (into allMediaItems) that are held, for use in Explore / Exploit
      var heldIndexes = [];
      usedIndexes.forEach( function(idxValue){
        if( idxValue > -1 ) heldIndexes.push( idxValue );
      });


  //     if( usedIndexes[i] > -1 ) {
  // /*
  //       $scope.selectedMediaItems.push(
  //         mediaItemHarvester.allItems()[ Object.keys(mediaItemHarvester.allItems())[usedIndexes[i]] ] );
  // */
  //       $scope.selectedMediaItems[i] =
  //         mediaItemHarvester.allItems()[ Object.keys(mediaItemHarvester.allItems())[usedIndexes[i]] ];
  //
  //
  //       if( $scope.selectedMediaItems[i].similar ) {
  //         // so, we want to replace the semi-held item we have, with something similar:
  //         var bestSimilarMatches = similaritySearch.getClosestMediaItemToOneMediaItem(
  //           $scope.selectedMediaItems[i],
  //           mediaItemHarvester.allItems(),
  //           {},
  //           5
  //         );
  //
  //         console.log( bestSimilarMatches );
  //
  //         delete $scope.selectedMediaItems[i].similar;
  //
  //         var bestSimilarIndex;
  //         var oneSimilarItem = undefined;
  //         // check for duplicates
  //         do {
  //           bestSimilarIndex = randomness.getRandomInt(0, bestSimilarMatches.length);
  //           if( bestSimilarMatches[bestSimilarIndex] ) {
  //             // for some (unknown) reason, there have been occasions of
  //             // bestSimilarMatches[bestSimilarIndex] being undefined, thus this if check...
  //             oneSimilarItem = bestSimilarMatches[bestSimilarIndex].item;
  //           }
  //         } while( $scope.selectedMediaItems.indexOf(oneSimilarItem) > -1 || !oneSimilarItem );
  //
  //         // let's do the deed of replacing the semi-held item, with one (hopefully) similar
  //         itemCandidate = oneSimilarItem;
  //         usedIndexes[i] = Object.keys(mediaItemHarvester.allItems()).indexOf(oneSimilarItem.sourceUrl);
  //       }
  //
  //     } else if( usedIndexes[i] == -1 ) {
  //
  //       // Explore / Exploit
  //
  //       /*
  //       50% chance of doing random (if nothing is held, 100% chance)
  //       50% chance of:
  //         similaritySearch.getClosestMediaItemToOneMediaItem
  //           for each held image, get an array of similar images,
  //             combine those arrays and choose the closest (highest) Jaccard index
  //             or select semi-randomly (weighted) from the elements in the arrays.
  //       */
  //
  //       if( heldIndexes.length === 0 || Math.random() > 0.5 ) {
  //       console.log( "DOING RANDOM THINGS" );

          // let's pick something randomly
          var oneItemIndex;
          do {
            oneItemIndex = randomness.getRandomInt( 0, Object.keys(mediaItemHarvester.allItems()).length );
          } while( usedIndexes.indexOf(oneItemIndex) > -1);

          usedIndexes[i] = oneItemIndex;

          var oneItem = mediaItemHarvester.allItems()[Object.keys(mediaItemHarvester.allItems())[oneItemIndex]];

  //        $scope.selectedMediaItems.push( oneItem );
            itemCandidate = oneItem;
            // TODO: why not set the item like above? ^^ ...forgot :(
            // ...it was because angular $$hashkey property getting confused,
            // -> see handling for that in selectItemAndWaitForLoadBeforeAnimationToGroundZero(...)
            // $scope.selectedMediaItems[i].type = oneItem.type;
            // $scope.selectedMediaItems[i].mediaUrl = oneItem.mediaUrl;
            // $scope.selectedMediaItems[i].tags = oneItem.tags;
            // $scope.selectedMediaItems[i].sourceUrl = oneItem.sourceUrl;
            // $scope.selectedMediaItems[i].isPlayer = oneItem.isPlayer;


  //       } else {
  //         // let's get closest media items to those held, and pick one of those
  //         console.log( "DOING A SIMILARITY SEARCH" );
  //
  //         var bestSimilarMatchesCollection = [];
  //         heldIndexes.forEach(function( oneHeldIndex ) {
  //           var oneHeldItem = mediaItemHarvester.allItems()[ Object.keys(mediaItemHarvester.allItems())[oneHeldIndex] ];
  //
  //           // let's exclude other held items from coming in as similar matches
  //           var otherHeldItems = {};
  //           heldIndexes.forEach(function( oneOtherHeldIndex ) {
  //             if( oneHeldIndex !== oneOtherHeldIndex ) {
  //               var oneOtherHeldItemKey = Object.keys(mediaItemHarvester.allItems())[oneOtherHeldIndex];
  //               var oneOtherHeldItem = mediaItemHarvester.allItems()[ oneOtherHeldItemKey ];
  //               otherHeldItems[ oneOtherHeldItemKey ] = oneOtherHeldItem;
  //             }
  //           });
  //           // and now get similar matches for the held item considered in this iteration
  //           var bestSimilarMatchesForOneItem = similaritySearch.getClosestMediaItemToOneMediaItem(
  //             oneHeldItem,
  //             mediaItemHarvester.allItems(),
  //             otherHeldItems,
  //             5
  //           );
  //           bestSimilarMatchesCollection.push( bestSimilarMatchesForOneItem );
  //         });
  //
  //
  //         // assemble all similar items found for all held items
  //         var bestSimilarMatches;
  //         if( bestSimilarMatchesCollection.length > 1 ) {
  //
  //           bestSimilarMatches = [].concat.apply([], bestSimilarMatchesCollection);
  //
  //         } else { // we should have exactly one array item (which is an array btw)
  //
  //           bestSimilarMatches = bestSimilarMatchesCollection[0];
  //         }
  //         bestSimilarMatches.sort(function(a,b){
  //           return a.jccrdIdx - b.jccrdIdx;
  //         });
  //
  //
  //         // let's just select the closest match for now...
  //         // ...that is, the closest match that is not already in $scope.selectedMediaItems
  //         // TODO: we might want to do something more stochastic,
  //         //  maybe weighted towards the closer matches.
  //
  //         // console.log( "bestSimilarMatchesCollection: " );
  //         // console.log( bestSimilarMatchesCollection );
  //
  //         var bestSimilarIndex = bestSimilarMatches.length;
  //         var oneSimilarItem;
  //         // check for duplicates
  //         do {
  //           bestSimilarIndex -= 1;
  //           oneSimilarItem = bestSimilarMatches[bestSimilarIndex].item;
  //         } while( $scope.selectedMediaItems.indexOf(oneSimilarItem) > -1 && bestSimilarIndex > 0 );
  //
  //         usedIndexes[i] = Object.keys(mediaItemHarvester.allItems()).indexOf(oneSimilarItem.sourceUrl);
  //
  // //        $scope.selectedMediaItems.push( oneSimilarItem );
  //         itemCandidate = oneSimilarItem;
  //        }
  //
  //       if( $scope.selectedMediaItems.length == $scope.totalVisibleItems ) {
  //         $scope.rollDisabled = false;
  //         $scope.saveDisabled = false;
  //       }
  //
  //     }


      if(!$scope.$$phase) $scope.$apply();

  //      doMediaItemLoadingAnimation( i );


      // callback( i, animDuration, negOrPos );

      return itemCandidate;
    }



    // networkUserHandles.getAllNetworkUserHandles().forEach(function(handle, index, array){
    //   // console.log( handle.network + ': ' + handle.user );
    //
    //   if( handle.network == NETNAME.TUMBLR ) {
    //
    //     mediaItemHarvester.getMediaItemsFromTumblrAccount(handle, mediaItemHarvester.allItems());
    //   }
    // });

    // when items come in from an api call, we want to make selection to display
  //   $scope.$watchCollection( "allMediaItems", function( newValue, oldValue ) {
  //     var newValuePropCount = Object.keys(newValue).length;
  //
  //     if( newValuePropCount > $scope.totalVisibleItems &&
  //           $scope.selectedMediaItems.length < $scope.totalVisibleItems ) {
  //       // from this $watchCollection thing, we'll only *once* call selectItems
  //       // when the above critera is met.
  // /*
  //       $scope.selectItems(
  //         newValuePropCount >= $scope.totalVisibleItems ? $scope.totalVisibleItems : newValuePropCount );
  // */
  //     }
  //   });


  }]);



  app.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/breeding', {
      templateUrl: 'breeding/likebreeding.html',
      controller: 'LikeBreeding'
    });
  }]);


})(angular);
