(function (angular) {
  "use strict";

  var app = angular.module('likesplayApp.cubes', ['ngRoute']);


  app.controller("Cubes", ['$scope', 'localStorageManager', function($scope, localStorageManager) {

    $scope.cubes = localStorageManager.getSavedCubes();

    console.log( $scope.cubes );

    $scope.removeCube = function( index ) {
      $scope.cubes.splice( index, 1 );
      localStorageManager.saveCubeState( $scope.cubes );
    }
  }]);



  app.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/cubes', {
      templateUrl: 'savedcubes/cubes.html',
      controller: 'Cubes'
    });
  }]);


})(angular);
