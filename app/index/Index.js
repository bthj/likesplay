(function (angular) {
  "use strict";

  var app = angular.module('likesplayApp.index', ['ngRoute']);

  app.controller( 'Index', [
    '$scope',
    '$routeParams',
    '$location',
    'colorFlow',
    '$famous',
    '$timeline',
    '$analytics',
    function(
      $scope,
      $routeParams,
      $location,
      colorFlow,
      $famous,
      $timeline,
      $analytics ) {


    var EventHandler = $famous['famous/core/EventHandler'];
    var Transitionable = $famous['famous/transitions/Transitionable'];
    var Easing = $famous['famous/transitions/Easing'];

    console.log( "$location.path()" );
    console.log( $location.path() );

    var _relocationPath;
    var _navigationPath = $routeParams.navigationPath;
    if( _navigationPath ) {
      // we rely on other paths having been defined before
      // with $routeProvider.when() calls before those for this controller
      _relocationPath = '/corral/' + _navigationPath;
    } else if( $location.path() !== '/index' ) {
      // requested the app root, so send you to the Corral
      _relocationPath = '/corral';
    }
    $location.path( _relocationPath );



    ////////////////////////////////////
    // resize event handling

    $scope.videoXProportions = .8;
    function calculateVideoProportions() {
      var referenceWidth = 220;
      var witdhRatioFromReference = referenceWidth / (window.innerWidth / 2)
      + window.innerWidth * .00008 // trial and error!
      ;

      $scope.videoXProportions = .65 * witdhRatioFromReference;
    }
    calculateVideoProportions();

    $scope.resizeEventHandler = new EventHandler();
    $scope.resizeEventHandler.on('resize', function(){

      calculateVideoProportions();
    });



    ////////////////////////////////////
    // resize event handling

    $scope.animationTransition = new Transitionable( 0 );

    $scope.animationTimelines = {
      diceZRotation: function( $transition ) {
        return $timeline([
          [.5, 0, Easing.outQuart],
          [1, Math.PI*4]

        ])( $transition.get() );
      },
      diceTranslation: function( $transition ) {
        return $timeline([
          [0, [0,0,0]],
          [.5, [0,0,100], Easing.inQuad],
          [1, [0, -2600, -20000]]
        ])( $transition.get() );
      },
      diceOpacity: function( $transition ) {
        return $timeline([
          [.6, 1, Easing.inCirc],
          [1, 0]
        ])( $transition.get() );
      },
      headerTranslation: function( $transition ) {
        return $timeline([
          [0, [0,0,0], Easing.outQuart],
          [1, [0,-window.innerHeight/3,0]]
        ])( $transition.get() );
      },
      footerXRotation: function( $transition ) {
        return $timeline([
          [0, 0, Easing.outQuart],
          [1, Math.PI]
        ])( $transition.get() );
      }
    };


    $scope.playExitAnimationAndNavigateToRoots = function() {

      $scope.animationTransition.set( 1, {duration:3000}, function() {

        $location.path( "/roots" );
      });


      $analytics.eventTrack('click', {
        category: 'Navigation', label: 'EnterRootsFromFrontpage'
      });
    }



    ////////////////////////////////////
    // colour flow

    $scope.flowRGB = [0, 0, 0];

    function updateFlowRGB( newRGB ) {
      $scope.$apply(function () {
        $scope.flowRGB = newRGB;
      });
    }
    colorFlow.registerColorUpdateCallback( updateFlowRGB );
    colorFlow.startWatchingColorFlowValues();

  }]);

  app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    .when('/', {
      templateUrl: 'index/index.html',
      controller: 'Index'
    })
    .when('/index', {
      templateUrl: 'index/index.html',
      controller: 'Index'
    })
    .when('/:navigationPath', {
      templateUrl: 'index/index.html',
      controller: 'Index'
    });
  }]);

})(angular);
