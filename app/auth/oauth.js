(function(angular) {
  "use strict";

  var app = angular.module('likesplayApp.oauth', [
    'firebase.auth', 'firebase.utils', 'famous.angular', 'ngRoute']);

  app.controller('OAuthCtrl', [
    '$scope', '$window', '$location', '$famous', 'Auth', 'authService',
      function($scope, $window, $location, $famous, Auth, authService) {

    var EventHandler = $famous['famous/core/EventHandler'];
    $scope.scrollViewEventHandler = new EventHandler();

    $scope.scrollSectionHeight = $window.innerHeight / 4;
    // $scope.innerWidth = $window.innerWidth;

    $scope.auth = Auth;

    $scope.auth.$onAuth( function(authData) {

      $scope.authData = authData;
    });


    $scope.authenticateWithFacebook = function() {
      authService.authenticateWithFacebook();
    }
    $scope.authenticateWithGoogle = function() {
      authService.authenticateWithGoogle();
    }
    $scope.authenticateWithTwitter = function() {
      authService.authenticateWithTwitter();
    }


    $scope.logout = function() {
      Auth.$unauth();
    }


    $scope.navigateToCorral = function() {
      $location.path('/corral');
    }

    $scope.navigateToBreeding = function() {
      $location.path('/breeding');
    }

    $scope.navigateToRoots = function() {
      $location.path('/roots');
    }

  }]);



  app.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/howdy', {
      controller: 'OAuthCtrl',
      templateUrl: 'auth/oauth.html'
    });
  }]);

})(angular);
